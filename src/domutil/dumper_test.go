package domutil

import (
	"testing"
	//"domutil"
)

func TestFontExtract1(t *testing.T) {
	fi,err := ExtractFont("@font-face { font-family: 'Bitstream Vera Serif Bold'; src: url(https://mdn.mozillademos.org/files/2468/VeraSeBd.ttf); }")
	if err != nil {
		t.Errorf("ERROR %v",err)
	}
	if fi.Url != "https://mdn.mozillademos.org/files/2468/VeraSeBd.ttf" {
		t.Errorf("fi.url %s",fi.url)
	}
	if fi.fontFamily != "Bitstream Vera Serif Bold" {
		t.Errorf("fi.fontFamily %s",fi.fontFamily)
	}
}


func TestFontExtract2(t *testing.T) {
	fi,err := ExtractFont("@font-face { font-family: 'PT Sans'; "+
			"src: url(http://icdn.lenta.ru/assets/pt_sans-regular-07e5bb65752f3fab570cf6192b64e088.eot?#iefix) format(embedded-opentype), "+
			"url(http://icdn.lenta.ru/assets/pt_sans-regular-c52bda140fe4363d7a09dafce6ab906e.woff) format(woff), "+
			"url(http://icdn.lenta.ru/assets/pt_sans-regular-f3d329cf9e89884aa8e4345b5261ff15.svg) format(svg); "+
			"font-weight: normal; font-style: normal; }")
	if err != nil {
		t.Errorf("ERROR %v",err)
	}
	if fi.fontFamily != "PT Sans" {
		t.Errorf("fi.fontFamily %s",fi.fontFamily)
	}
	if fi.Url != "http://icdn.lenta.ru/assets/pt_sans-regular-c52bda140fe4363d7a09dafce6ab906e.woff" {
		t.Errorf("fi.fontFamily %s",fi.url)
	}

}

// @font-face { font-family: 'Glyphicons Halflings'; src: url(http://127.0.0.1:9000/assets/third/bootstrap/fonts/glyphicons-halflings-regular.eot?#iefix) format(embedded-opentype), url(http://127.0.0.1:9000/assets/third/bootstrap/fonts/glyphicons-halflings-regular.woff) format(woff), url(http://127.0.0.1:9000/assets/third/bootstrap/fonts/glyphicons-halflings-regular.ttf) format(truetype), url(http://127.0.0.1:9000/assets/third/bootstrap/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format(svg); }

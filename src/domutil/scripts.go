package domutil

import (
	"io/ioutil"
	"log"
)

const TestJs =`function(a) {
console.log("AAAA:"+a);
return {"X":a};
}`

func readOrPanic(fname string) string {
	data,err := ioutil.ReadFile(fname)
	if err != nil {
		log.Panic(err)
	}
	return string(data)
}

var ClientDumpJs = readOrPanic("js/tweets2.js")

var BackgoundJs = readOrPanic("js/background.js")
var ManifestJson = readOrPanic("js/manifest.json")
var BackgroundCSS = readOrPanic("js/background.css")



func PrepareChromeExtensions(extensionsDir string) (err error) {
	err = ioutil.WriteFile(extensionsDir + "/background.js",[]byte(BackgoundJs),0644)

	if err != nil {
		log.Println("Prepare background.js: " + err.Error())
		return
	}

	err = ioutil.WriteFile(extensionsDir + "/background.css",[]byte(BackgroundCSS),0644)

	if err != nil {
		log.Println("Prepare background.css: " + err.Error())
		return
	}


	err = ioutil.WriteFile(extensionsDir + "/manifest.json",[]byte(ManifestJson),0644)

	if err != nil {
		log.Println("Prepare manifest.json: " + err.Error())
		return
	}

	return
}

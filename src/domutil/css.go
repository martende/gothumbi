package domutil

import (
	"regexp"
	"strings"
	"fmt"
	"sort"
	"bytes"
)


type WalkerCSS struct {
	Key string
	Value string
}

func compactStyle(css []CSSProperty,ws*WebSocket,tmpDir *string,bm *BoxModel) string {
	var cssMap = make(map[string]string)
	var importants = make(map[string]bool)
	var s CSSProperty

	for _,s = range css {
		if _,ok:=importants[s.Name];!s.Disabled && (! ok || s.Important) {
			cssMap[s.Name]=s.Value
			if s.Important {
				importants[s.Name]=true
			}
		}
	}

	re1, _ := regexp.Compile("url\\(([^)]+)\\)")

	for _,stl := range []string{"background-image","list-style-image","cursor","-webkit-border-image","list-style-image"} {
		if bgimage,exists:=cssMap[stl];exists {
			if rx := re1.FindAllStringSubmatch(bgimage, -1); len(rx) > 0 {
				cssMap[stl] = "url("+ventiurl("src",rx[0][1],ws,tmpDir)+")"
			}
		}
	}

	if v,exists := cssMap["position"];exists && v == "fixed" {
		cssMap["position"] = "absolute"
	}

	if v,exists := cssMap["height"];exists && strings.HasSuffix(v,"%") {
		cssMap["height"] = fmt.Sprintf("%dpx",bm.Height)
	}

	var buffer bytes.Buffer

	var cssKeys = make([]string,len(cssMap))
	for k := range cssMap {
		cssKeys = append(cssKeys,k)
	}
	sort.Strings(cssKeys)

	for _,k:= range cssKeys {
		v := cssMap[k]
		if v != "" {
			buffer.WriteString(k + ":" + v + ";")
		}
	}

	return buffer.String();
}


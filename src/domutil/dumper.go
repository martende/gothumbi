package domutil

import (
	"log"
	"io/ioutil"
	"fmt"
	"errors"
	"bytes"
	"strings"
	"html"
	"regexp"
	"net/url"
	"strconv"
	"encoding/base64"
)

var SKIP_NODES = map[string] int{"#comment":1,"SCRIPT":1,"NOSCRIPT":1,"NOFRAMES":1,"AUDIOTITLE":1,"STYLE":1,"LINK":1,"PARAM":1,"META":1}
var TRANSPARENT1x1GIF = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="


var resourcesPool map[string] HttpRequestInfo = make(map[string] HttpRequestInfo)

func prepareResourcesPool(ws *WebSocket) {
	log.Println("-----------RESOURCES POOL---------" )

	for _,v := range ws.SentHttpRequests {
		if old,ok:=resourcesPool[v.Url];ok {
			if v.FinishTimestamp > old.FinishTimestamp {
				resourcesPool[v.Url]=v
			}
		} else {
			resourcesPool[v.Url]=v
		}
		//log.Println(v)
	}

}
func toAbsUrl(baseurlStr string, weburlStr string) string {
	baseurl, err := url.Parse(baseurlStr)
	if err != nil {
		return ""
	}
	relurl, err := url.Parse(weburlStr)
	if err != nil {
		return ""
	}
	absurl := baseurl.ResolveReference(relurl)
	return absurl.String()
}

func cssUnescape(s string) string {
	if (strings.HasPrefix(s,"'") && strings.HasSuffix(s,"'")) || (strings.HasPrefix(s,"\"") && strings.HasSuffix(s,"\"")) {
		s = s[1 : len(s)-1]
	}
	i := 0
	l := len(s)
	var s2 = ""
	for i < l {
		if s[i] == '\\' {
			us := ""
			j := i + 1

			if  j < l && s[j] >= '0' && s[j] <= '9' {
				for  j < l && s[j] >= '0' && s[j] <= '9' {
					us += string(s[j])
					j += 1
				}
				if us2, err := strconv.Unquote("'\\u" + us + "'"); err == nil {
					us = us2
				}
			} else {
				us = string(s[j])
			}
			i = j
			s2 += us

		} else {
			s2 += string(s[i])
			i += 1
		}
	}

	return s2
}

func constructOnePseudo(n *nodeInfo) {
	content := nodeInfo{NodeName:"#text",NodeValue:""}
	for _,r := range n.MatchedCSSRules {
		for _,s := range r.Rule.Style.CssProperties {
			if s.Name == "content" {
				content.NodeValue = cssUnescape(s.Value)
				log.Printf("content.NodeValue (%s) (%s)",content.NodeValue,s.Value)
			}
		}
	}
	n.NodeName = "span"
	n.Children = make([]nodeInfo,1)
	n.Children[0] = content
}

func processPseudoElements(node *nodeInfo) []nodeInfo {

	nodeChildren := make([]nodeInfo,0,len(node.PseudoElements) + len(node.Children))

	for _,n:= range node.PseudoElements {
		if (n.NodeName=="<pseudo:before>") {
			constructOnePseudo(&n)
			nodeChildren = append(nodeChildren,n)
		}
	}

	nodeChildren = append(nodeChildren,node.Children...)

	for _,n:= range node.PseudoElements {
		if (n.NodeName=="<pseudo:after>") {
			constructOnePseudo(&n)
			nodeChildren = append(nodeChildren,n)
		}
	}

	return nodeChildren
}

func walkNodes(node*nodeInfo,buffer *bytes.Buffer,docInfo *DocInfo,ws*WebSocket,tmpDir *string) {

	nodeChildren := node.Children

	walkDeeper := func(docInfo *DocInfo) {
		for i:=0;i<len(nodeChildren);i++ {
			walkNodes(&nodeChildren[i],buffer,docInfo,ws,tmpDir)
		}
	}

	continueWalkDeeper := func() {
		walkDeeper(docInfo)
	}

	if  _,ok:=SKIP_NODES[node.NodeName];ok {
		return
	}

	var css1 []CSSProperty = make([]CSSProperty,0,0)

	affectedStyles:= make(map[string] bool)

	for _,r:=range node.MatchedCSSRules {
		for _,p:= range r.Rule.Style.CssProperties {
			affectedStyles[p.Name]=true
		}
	}

	for _,p:= range node.AttributesStyle.CssProperties {
		affectedStyles[p.Name]=true
	}

	for _,p:= range node.InlineStyle.CssProperties {
		affectedStyles[p.Name]=true
	}

	// POST COMPRESS affected Styles
	// merge background-repeat-x -> background-repeat (as stored in webkit properties hash)
	affectedStyles2 := make(map[string] bool)

	for n := range affectedStyles {
		switch n {
			case "background": // skip as splited to many
			case "background-position-x": affectedStyles2["background-position"] = true
			case "background-position-y": affectedStyles2["background-position"] = true
			case "background-repeat-x": affectedStyles2["background-repeat"] = true
			case "background-repeat-y": affectedStyles2["background-repeat"] = true
			default: affectedStyles2[n] = true
		}
	}


	for n := range affectedStyles2 {
		css1 = append(css1,CSSProperty{Name:n,Value:node.GetPropertyValue(n)})
	}

	var attrs = make(map[string] string)

	allowAttrs := func(args ...string) {
		for _,key := range args  {
			if val,exists:=node.GetAttribute(key);exists {
				attrs[key] = val
			}
		}
	}

	var tag string = node.NodeName
	var needCloseTag = true

	if ( len(node.PseudoElements) > 0 ) {
		needCloseTag = true
		nodeChildren = processPseudoElements(node)
	}

	switch node.NodeName {
	case "html":
		docInfo.DocType = DocType{"html",node.PublicId,node.SystemId}
	case "#comment": // do Nothing
	case "#document":
		var bodyEl *nodeInfo = node.GetElementByTagName("BODY")
		var htmlEl *nodeInfo = node.GetElementByTagName("HTML")

		docInfo.usedBodyColor = htmlEl != nil && bodyEl != nil && htmlEl.GetPropertyValue("background-color") == "rgba(0, 0, 0, 0)"
		docInfo.usedBodyImage = docInfo.usedBodyColor && htmlEl.GetPropertyValue("background-image") == "none"
		docInfo.bodyEl = bodyEl
		docInfo.htmlEl = htmlEl

		docInfo.overflowX = "auto"
		docInfo.overflowY = "auto"
		docInfo.innerWidth = "1024px"
		docInfo.baseUrl = node.BaseUrl

		if bodyEl != nil {
			if bodyEl.GetPropertyValue("overflow-x") == "hidden" {
				docInfo.overflowX = "hidden"
			}
			if bodyEl.GetPropertyValue("overflow-y") == "hidden" {
				docInfo.overflowY = "hidden"
			}
			docInfo.innerWidth =  fmt.Sprintf("%dpx",bodyEl.BoxModel.Width)
		}
		continueWalkDeeper()
		return

	case "HTML":
		if ! docInfo.InIframe {
			css1 = append(css1, []CSSProperty{
				CSSProperty{Name:"overflow-x", Value:"visible", Important:true},
				CSSProperty{Name:"overflow-y", Value:"visible", Important:true},
				CSSProperty{Name:"height", Value:"", Important:true},
				CSSProperty{Name:"background-color", Value:"", Important:true},
				CSSProperty{Name:"background-size", Value:"", Important:true},
				CSSProperty{Name:"background-position", Value:"", Important:true},
				CSSProperty{Name:"background-image", Value:"", Important:true},
				CSSProperty{Name:"background-repeat", Value:"", Important:true},
			}...)
		}
		attrs["class"] = "html"
		tag = "DIV"
		/*buffer.WriteString(fmt.Sprintf("<DIV class=\"html\" style=\"%s\">\n",compactStyle(css1)))
		continueWalkDeeper()
		buffer.WriteString(fmt.Sprintf("</DIV>\n"))
		*/
	case "BODY":
		if ! docInfo.InIframe {
			css1 = append([]CSSProperty{
					CSSProperty{Name:"vertical-align", Value:"bottom"},
					CSSProperty{Name:"min-height", Value:node.GetPropertyValue("height")  },
					CSSProperty{Name:"color", Value:node.GetPropertyValue("color")        },
					CSSProperty{Name:"margin-left", Value:node.GetPropertyValue("margin-left")},
					CSSProperty{Name:"margin-top", Value:node.GetPropertyValue("margin-top")},
					CSSProperty{Name:"margin-right", Value:node.GetPropertyValue("margin-right")},
					CSSProperty{Name:"margin-bottom", Value:node.GetPropertyValue("margin-bottom")},
				}, css1...)

			css1 = append(css1, []CSSProperty{
					CSSProperty{Name:"overflow-x", Value:"visible", Important:true},
					CSSProperty{Name:"overflow-y", Value:"visible", Important:true},
					CSSProperty{Name:"height", Value:"", Important:true},
				}...)

			if docInfo.usedBodyColor {
				css1 = append(css1, CSSProperty{Name:"background-color", Value:"", Important:true})
			}
			if docInfo.usedBodyImage {
				css1 = append(css1, []CSSProperty{
						CSSProperty{Name:"background-size", Value:"", Important:true},
						CSSProperty{Name:"background-position", Value:"", Important:true},
						CSSProperty{Name:"background-image", Value:"", Important:true},
						CSSProperty{Name:"background-repeat", Value:"", Important:true},
					}...)
			}
		}
		attrs["class"] = "body"
		tag = "DIV"
		/*
		buffer.WriteString(fmt.Sprintf("<DIV class=\"body\" style=\"%s\">\n",compactStyle(css1)))
		continueWalkDeeper()
		buffer.WriteString(fmt.Sprintf("</DIV>\n"))
		*/
	case "HEAD": return // TODO: HEAD
	case "FRAMESET":
		var borderStr string
		if v,exists:=node.GetAttribute("border");exists {
			borderStr = v
		} else if v,exists:=node.GetAttribute("framespacing");exists {
			borderStr = v
		} else {
			borderStr = "6"
		}
		var border int
		var err error
		if border,err = strconv.Atoi(borderStr);err!=nil {
			border = 0
		}

		buffer.WriteString("<table class=\"frameset\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">")

		var rows bool
		if v,exists:=node.GetAttribute("rows");exists && strings.Index(v,",")> 0 {
			rows = true
		}
		if ! rows {
			buffer.WriteString("<tr>")
		}
		var prevch *nodeInfo= nil
		for _,ch := range node.Children {
			if ( ch.NodeName == "FRAMESET" || ch.NodeName=="FRAME") {
				if ( prevch != nil ) {
					var border2 = border
					var fb1 = prevch.NodeName != "FRAME" || prevch.GetAttributeValOnly("frameborder","0") == "0"
					var fb2 = ch.NodeName != "FRAME" || ch.GetAttributeValOnly("frameborder","0") == "0"
					if ( fb1 && fb2 ) {
						border2 = 0
					}
					if(border2!=0) {
						if rows {
							buffer.WriteString("<tr>")
						}
						buffer.WriteString("<td style=\"background-color: silver;\">")
						buffer.WriteString(fmt.Sprintf("<div style=\"width:%dpx;height:%dpx\"></div>",border2,border2))
						buffer.WriteString("</td>")
						if rows {
							buffer.WriteString("</tr>")
						}
					}
				}
				if rows {
					buffer.WriteString("<tr>")
				}
				buffer.WriteString("<td style=\"vertical-align:top\">")
				walkNodes(&ch,buffer,docInfo,ws,tmpDir)
				buffer.WriteString("</td>")
				prevch = &ch
			}
		}
		return
	case "FRAME" , "IFRAME":
		tag = "DIV"
		attrs["class"] = "frame"

		buffer.WriteString("<div class=\"iframe\" style=\""+compactStyle(css1,ws,tmpDir,&node.BoxModel) + "\">" )

		var docInfo2 = DocInfo{InIframe:true}

		walkNodes(node.ContentDocument,buffer,&docInfo2,ws,tmpDir)

		buffer.WriteString("</div>")
		return
	case "TEXTAREA":allowAttrs("cols","rows","placeholder","name","disabled","readonly")
	case "LABEL":allowAttrs("for")

	case "OBJECT":
		tag = "DIV"
	case "EMBED","VIDEO","APPLET":
		tag = "IMG"
		needCloseTag = false
		var err error
		var c string
		if c,err = getElementContent(ws,node.NodeId,tmpDir);err==nil {
			attrs["src"] = ventiurl("src",c,ws,tmpDir)
		} else {
			log.Printf("WARN: getElementContent error:%#v",err)
			attrs["src"] = TRANSPARENT1x1GIF
		}

	case "CANVAS":
		needCloseTag = false
		tag = "IMG"
		var err error
		var c string
		if c,err = getCanvasContent(ws,node.NodeId);err==nil {
			attrs["src"] = ventiurl("src",c,ws,tmpDir)
		} else {
			log.Printf("WARN: getCanvasContent error:%#v",err)
			attrs["src"] = TRANSPARENT1x1GIF
		}
	case "FORM":
		allowAttrs("acceptCharset","method","target","action","enctype")
	case "FIELDSET":
		allowAttrs("disabled")
	case "DEL","INS":
		allowAttrs("cite","datetime")
	case "BLOCKQUOTE","Q":
		allowAttrs("cite")
	case "BUTTON":
		if val,exists:=node.GetAttribute("type");exists && strings.ToLower(val) != "submit" {
			attrs["type"] = val
		}
		allowAttrs("name","value","disabled")
	case "INPUT":
		needCloseTag = false
		if val,exists:=node.GetAttribute("type");exists && strings.ToLower(val) != "text" {
			attrs["type"] = val
		}
		for _,key := range []string{"src"} {
			if val,exists:=node.GetAttribute(key);exists {
				attrs[key] = ventiurl("src",toAbsUrl(docInfo.baseUrl,val),ws,tmpDir)
			}
		}
		allowAttrs("accept","placeholder","autosave","results","maxlength","name","size","checked","disabled","readonly")
	case "IMG":
		needCloseTag = false
		for _,key := range []string{"alt","ismap","longdesc","name","usemap"} {
			if val,exists:=node.GetAttribute(key);exists {
				attrs[key] = val
			}
		}
		for _,key := range []string{"lowsrc","src"} {
			if val,exists:=node.GetAttribute(key);exists {
				attrs[key] = ventiurl("src",toAbsUrl(docInfo.baseUrl,val),ws,tmpDir)
			}
		}
	case "HR" , "BR":
		needCloseTag = false
	case "FONT" , "BASEFONT": 		allowAttrs("size","color","face")
	case "MARQUEE":
	case "LI":						allowAttrs("value")
	case "OL":						allowAttrs("reversed","start")
	case "UL":
	case "SELECT":					allowAttrs("disabled","multiple","size")
	case "OPTION":					allowAttrs("disabled","selected","label","value")
	case "A":						allowAttrs("hreflang","name","target","type","href")
	case "AREA":					allowAttrs("alt","coords","target","shape","href")
	case "OPTGROUP":				allowAttrs("disabled","label")
	case "MENU":					allowAttrs("type","label")
	case "TABLE":					allowAttrs("frame","rules","caption")
	case "COL","COLGROUP":			allowAttrs("char","charoff","span")
	case "TBODY","TFOOT" ,"THEAD","TR": allowAttrs("char","charoff")
	case "TD","TH":					allowAttrs("abbr","axis","char","charoff","headers","colspan","rowspan")
	case "#text":
		buffer.WriteString(node.NodeValue)
		return
	default:
		//buffer.WriteString(fmt.Sprintf("<%s>\n",node.NodeName))
		//continueWalkDeeper()
		//buffer.WriteString(fmt.Sprintf("</%s>\n",node.NodeName))
	}

	buffer.WriteString(fmt.Sprintf("<%s",tag))

	attrs["style"] = compactStyle(css1,ws,tmpDir,&node.BoxModel)

	for k,v := range attrs {
		switch k {
		case "ismap","disabled","reversed","multiple","selected","readonly","checked": buffer.WriteString(fmt.Sprintf(" %s",k))
		case "href","action","cite":
			if strings.HasPrefix(v,"javascript:") {
				v = "#"
			} else {
				v = toAbsUrl(docInfo.baseUrl,v)
			}
			buffer.WriteString(fmt.Sprintf(" %s=\"%s\"",k,html.EscapeString(v)))
		default: buffer.WriteString(fmt.Sprintf(" %s=\"%s\"",k,html.EscapeString(v)))
		}
	}

	buffer.WriteString(">\n")

	if needCloseTag {
		continueWalkDeeper()
		buffer.WriteString(fmt.Sprintf("</%s>\n",tag))
	}
}

func getElementContent(ws *WebSocket,nodeId int,tmpDir *string)  (ret string,err error) {
	var screenShotRet2 InMessage

	if screenShotRet2,err =ws.Cmd(&DOMCaptureScreenshotElementMessage{OutMessage{Method:"DOM.captureScreenshotElement"},DOMCaptureScreenshotElementParams{NodeId:nodeId}}); err != nil {
		log.Printf("Page.captureScreenshot error %v",err)
		return
	}

/*
	var screenshotData2 []byte

	if screenshotData2, err = base64.StdEncoding.DecodeString(screenShotRet2.Result.Data); err != nil {
		log.Printf("ERROR: screenshot encoding ERROR %v",err)
		return
	}
*/

	ret = "data:image/png;base64," + string(screenShotRet2.Result.Data)

	return
}


func getCanvasContent(ws *WebSocket,nodeId int) (ret string,err error) {
	var im InMessage

	if im,err=ws.Cmd(&DOMResolveNodeMessage{OutMessage{Method:"DOM.resolveNode"},DOMResolveNodeParams{NodeId:nodeId} }); err != nil {
		log.Printf("DOM.resolveNode error %v",err)
		return
	}

	var objectId = im.Result.Object.ObjectId

	var dataUrlResult InMessage

	if dataUrlResult,err =ws.Cmd(&RuntimeCallFunctionOnMessage{OutMessage{Method:"Runtime.callFunctionOn"},RuntimeCallFunctionOnParams{ObjectId:objectId,FunctionDeclaration:"function() { return this.toDataURL(); }"} }); err != nil {
		log.Printf("Runtime.getProperties error %v",err)
		return
	}

	var ok bool
	if ret,ok = dataUrlResult.Result.Result.Value.(string);!ok {
		err = errors.New("getCanvasContent: dataUrlResult.Result.Result.Value is not string")
		return
	}

	return
}

func walkCSS(root *nodeInfo,buffer *bytes.Buffer,docInfo *DocInfo,ws *WebSocket,tmpDir *string) {
	//ws.Debug =true
	var ret InMessage
	var err error
	if ret,err = ws.Cmd(&OutMessage{Method:"CSS.getAllStyleSheets"}) ; err != nil{
		log.Printf("ERROR: CSS.getAllStyleSheets %#v",err)
		return
	}

	// @font-face { font-family: 'Bitstream Vera Serif Bold'; src: url(https://mdn.mozillademos.org/files/2468/VeraSeBd.ttf); }
	// @font-face { font-family: Roboto; font-style: normal; font-weight: 400; src: url(https://fonts.gstatic.com/s/roboto/v15/ek4gzZ-GeXAPcSbHtCeQI_esZW2xOQ-xsNqO47m55DA.woff2) format(woff2); unicode-range: U+460-52F, U+20B4, U+2DE0-2DFF, U+A640-A69F; }


	for _,fontFace := range ret.Result.FontStyles {
		log.Println("fontFace",fontFace)
		if fi,err := ExtractFont(fontFace);err == nil {
			if fi.Url != "" {
				var fontUrl = fi.Url
				fi.Url = ventiurl("font",fontUrl,ws,tmpDir)
				if fi.Url == "" {
					log.Printf("WARN: FontUrl '%s' convert to venti failed. Skip it\n",fontUrl)
					continue
				}
			}
			res := fi.String()
			log.Print("Font: " + res)
			buffer.WriteString(res)
		} else {
			log.Println("WARN: Cant parse fontFace error: %v font: %s ",err,fontFace)
		}
	}
	//ws.Debug =false
}

type DumpedDocument struct {
	html,css,docType string
}

func processDocument(root *nodeInfo,htmlstyle string,tmpDir string,ws*WebSocket) (ret DumpedDocument,err error) {
	var buffer bytes.Buffer
	var cssBuffer bytes.Buffer
	var docinfo DocInfo


	walkNodes(root,&buffer,&docinfo,ws,&tmpDir)

	walkCSS(root,&cssBuffer,&docinfo,ws,&tmpDir)

	if docinfo.htmlEl == nil || docinfo.bodyEl == nil {
		err = errors.New("No body or html tag found")
		return
	}

	var html = "<div class=\"html1\""
	html += " style=\"width: " + docinfo.innerWidth + ";";
	html += "text-align: left;"
	html += "overflow-x: " + docinfo.overflowX+ ";"
	html += "overflow-y: " + docinfo.overflowY+ ";"

	var bgcolor string

	if  docinfo.usedBodyColor  {
		bgcolor = docinfo.bodyEl.GetPropertyValue("background-color")
	} else {
		bgcolor = docinfo.htmlEl.GetPropertyValue("background-color")
	}

	html += "background-color: " + bgcolor + ";"


	var bgimageEl *nodeInfo

	if  docinfo.usedBodyImage  {
		bgimageEl = docinfo.bodyEl
	} else {
		bgimageEl = docinfo.htmlEl
	}

	var bgimage string = bgimageEl.GetPropertyValue("background-image")

	if (bgimage != "none"/* && bgimage != ""*/) {
		re1, _ := regexp.Compile("url\\(([^)]+)\\)")
		if rx := re1.FindAllStringSubmatch(bgimage, -1); len(rx) > 0 {
			bgimage = rx[0][1]
		} else {
			err = errors.New(fmt.Sprintf("ERROR: Document bad formatbgimage bgimage=(%s) docinfo.usedBodyImage=%t",bgimage,docinfo.usedBodyImage))
			return
		}

		var ventiimage string
		if ventiimage=ventiurl("bgimage",bgimage,ws,&tmpDir); ventiimage == "" {
			log.Printf("WARN: ventiurl error: url:%s",bgimage)
			bgimage = TRANSPARENT1x1GIF
		} else {
			bgimage = "url("+ventiimage+")"
		}



		html += "background-image: "    + bgimage + ";"

	}

	html += "position: relative;"
	if ( htmlstyle != "") {
		html += htmlstyle;
	}

	// z-index must be after `+=htmlstyle` as it may have "z-index: -1", tc:www.davidrumsey.com, http://archive.is/OMCwI
	html += "; z-index: 0"
	html += "\">"

	html += buffer.String() + "</div>"

	var cssText = cssBuffer.String()


	if ( cssText != "") {
		html = "<style>\n"+cssText+"</style>\n" + html
	}



	return DumpedDocument{html,cssText,docinfo.DocType.String()},nil
}


func MakeDump(ws *WebSocket,tmpDir string) (err error) {

	log.Println("---------MakeDump-----------" )

	ws.Debug = true
	documentResult,err := ws.Cmd(&OutMessage{Method :"DOM.getFullDocument"})
	if err != nil {
		log.Printf("DOM.getFullDocument",err)
		return
	}

	//_,_ = ws.Cmd(&CSSGetMatchedStylesForNodeMessage{OutMessage{Method :"CSS.getMatchedStylesForNode"},CSSGetMatchedStylesForNodeParams{10,false,false}})

	//_,_ = ws.Cmd(&CSSGetMatchedStylesForNodeMessage{OutMessage{Method :"CSS.getMatchedStylesForNode"},CSSGetMatchedStylesForNodeParams{9,false,false}})

	ws.Debug = false

	prepareResourcesPool(ws)

	var screenShotRet InMessage

	if screenShotRet,err =ws.Cmd(&OutMessage{Method:"Page.captureScreenshot"}); err != nil {
		log.Printf("Page.captureScreenshot error %v",err)
		return
	}

	var screenshotData []byte

	if screenshotData, err = base64.StdEncoding.DecodeString(screenShotRet.Result.Data); err != nil {
		log.Printf("ERROR: screenshot encoding ERROR %v",err)
		return
	}

	err = ioutil.WriteFile(tmpDir + "/index.png",[]byte(screenshotData),0644)

	if err != nil {
		log.Println("ERROR: screenshotData save failed:" + err.Error())
		return
	}

	dumpRet,err := processDocument(&documentResult.Result.Root,"",tmpDir,ws)

	if err != nil {
		log.Printf("ERROR: processDocument %#v",err)
		return
	}


	log.Println("dumpRet.docType",dumpRet.docType)

	data:= dumpRet.docType + "\n" +
			"<head>" +
			`<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>` +
			dumpRet.css +
			"</head>" +
	dumpRet.html

	err = ioutil.WriteFile(tmpDir + "/index.html",[]byte(data),0644)

	if err != nil {
		log.Println("ERROR: Upload Index failed:" + err.Error())
		return
	}

	return

}

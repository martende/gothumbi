package domutil

import (
	"code.google.com/p/go.net/websocket"
	"log"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
	"errors"
	"strings"
)


// Out Messages

type IOutMessage interface {
	SetId(id int)
	GetMethod() string
}

type OutMessage struct {
	IOutMessage		`json:"-"`
	Id 	int 		`json:"id"`
	Method string	`json:"method"`
}

func (self *OutMessage) SetId(id int) {
	self.Id = id
}
func (self *OutMessage) GetMethod() string {
	return self.Method
}

type PageNavigateMessage struct {
	OutMessage
	Params PageNavigateMessageParams `json:"params"`
}

type CSSGetMatchedStylesForNodeMessage struct {
	OutMessage
	Params CSSGetMatchedStylesForNodeParams `json:"params"`
}

type RuntimeEvaluateMessage struct {
	OutMessage
	Params RuntimeEvaluateMessageParams `json:"params"`
}

type AddCookieMessage struct {
	OutMessage
	Params AddCookieMessageParams `json:"params"`
}

type SetUserAgentOverrideMessage struct {
	OutMessage
	Params SetUserAgentOverrideMessageParams `json:"params"`
}

type RequestChildNodesMessage struct {
	OutMessage
	Params RequestChildNodesMessageParams `json:"params"`
}

type PageHandleJavascriptDialogMessage struct {
	OutMessage
	Params PageHandleJavascriptDialogMessageParams `json:"params"`
}

type PageGetResourceContentMessage struct {
	OutMessage
	Params PageGetResourceContentMessageParams `json:"params"`
}

type NetworkGetResponseBody struct {
	OutMessage
	Params NetworkGetResponseBodyParams `json:"params"`
}


type RuntimeGetPropertiesMessage struct {
	OutMessage
	Params RuntimeGetPropertiesParams `json:"params"`
}

type DOMResolveNodeMessage struct {
	OutMessage
	Params DOMResolveNodeParams `json:"params"`
}

type RuntimeCallFunctionOnMessage struct {
	OutMessage
	Params RuntimeCallFunctionOnParams `json:"params"`
}
type DOMCaptureScreenshotElementMessage struct {
	OutMessage
	Params DOMCaptureScreenshotElementParams `json:"params"`
}


type DOMCaptureScreenshotElementParams struct {
	NodeId	int `json:"nodeId"`
}

type RuntimeCallFunctionOnParams  struct {
	ObjectId string `json:"objectId"`
	FunctionDeclaration string `json:"functionDeclaration"`
}

type DOMResolveNodeParams struct {
	NodeId	int `json:"nodeId"`
}


type RuntimeGetPropertiesParams struct {
	ObjectId string `json:"objectId"`
}

type CSSGetMatchedStylesForNodeParams struct {
	NodeId 			 int 	`json:"nodeId"`
	ExcludePseudo 	 bool 	`json:"excludePseudo"`
	ExcludeInherited bool 	`json:"excludeInherited"`
}

type NetworkGetResponseBodyParams struct {
	RequestId string `json:"requestId"`
}

type PageGetResourceContentMessageParams struct {
	FrameId string	`json:"frameId"`
	Url string		`json:"url"`
}

type PageHandleJavascriptDialogMessageParams struct {
	Accept bool `json:"accept"`
	PromptText string `json:"promptText"`
}

type RequestChildNodesMessageParams struct {
	NodeId int `json:"nodeId"`
	Depth int `json:"depth"`
}

type SetUserAgentOverrideMessageParams struct {
	UserAgent string `json:"userAgent"`
}

type AddCookieMessageParams struct {
	Name 		string  `json:"name"`
	Value		string `json:"value"`
	Domain		string `json:"domain"`
	Path		string `json:"path"`
	Expires		int64  `json:"expires"`
	HttpOnly 	bool	`json:"httpOnly"`
	Secure		bool	`json:"secure"`
	Session 	bool	`json:"session"`
}

type RuntimeEvaluateMessageParams struct {
	Expression string	`json:"expression"`
	ObjectGroup string	`json:"objectGroup"`
	ContextId int		`json:"-"`
	ReturnByValue bool	`json:"returnByValue"`
}

type PageNavigateMessageParams struct {
	Url string `json:"url"`
}


type IInMessage interface {
//	GetId() int
}

// In Messages

type InBaseMessage struct {
	IInMessage
	Id 	int
	Method		string
	Error       *errorInfo
}
/*
func (self *InBaseMessage) GetId() int {
	return self.Id
}
*/
type InMessage struct {
	*InBaseMessage
	Result		*resultInfo
	Params 		*paramsInfo
}

type In_Runtime_getProperties struct {
	InBaseMessage
	Result		struct {
		Result	[]struct {
			Name	string
			/*Value	struct {
				Type	string
				Value	string
			}*/
			Writable bool
			Enumerable bool
			Configurable bool
			IsOwn	bool
		}
	}
}

type errorInfo struct {
	Code 	int				`json:"code"`
	Message string			`json:"message"`
}

type resultInfo struct {
	FrameId		string			`json:"frameId"`
	Result		evalResultInfo
	WasThrown	bool
	ExceptionDetails exceptionDetailsInfo
	Data		string
	Root		nodeInfo
	Body		string
	Base64Encoded bool
	Object		*objectInfo
	FontStyles  []string
}

type objectInfo struct {
	Type string
	ObjectId string
	Subtype string
	ClassName string
	Description string
}
/*
type objectIdInfo struct {
	InjectedScriptId int
	Id int
}
*/

type nodeInfo struct {
	NodeId int
	NodeType int
	NodeName string
	PublicId,SystemId string
	LocalName string
	NodeValue string
	ChildNodeCount int
	Children []nodeInfo
	DocumentUrl string
	BaseUrl string
	XMLVersion string
	Attributes []string
	PseudoElements []nodeInfo
	MatchedCSSRules []ruleMatch
	ComputedStyle 	[]CSSComputedStyleProperty
	InlineStyle   	CSSStyle
	AttributesStyle CSSStyle
	BoxModel	BoxModel
	ContentDocument *nodeInfo
	computedStyleMap map[string] string `json:"-"`
	computedAttributeMap map[string] string `json:"-"`
}
/*
func (this *nodeInfo) GetNodeStyles() map[string] string {
	ret:= make(map[string] string)
	log.Println(this.NodeStyles )
	var css ruleMatch
	for css = range this.NodeStyles {
		var style CSSStyle
		for style = range css.Rule.Style {

		}
	}
	return ret
}
*/
func (this *nodeInfo) GetAttribute(key string) (val string,exists bool) {
	if (this.computedAttributeMap == nil ) {
		this.computedAttributeMap = make(map[string] string)
		for i:= 0; i < len(this.Attributes) ; i+=2 {
			this.computedAttributeMap[this.Attributes[i]]=this.Attributes[i+1]
		}
	}
	val,exists = this.computedAttributeMap[key]
	return
}

func (this *nodeInfo)GetAttributeValOnly(key string,defaultVal string) string {
	if val,exists:=this.GetAttribute(key);exists {
		return val
	}
	return defaultVal
}

func (this *nodeInfo) GetPropertyValue(key string) string {
	if ( this.computedStyleMap == nil ) {
		this.computedStyleMap = make(map[string] string)
		for i:= 0; i < len(this.ComputedStyle) ; i++ {
			this.computedStyleMap[this.ComputedStyle[i].Name]=this.ComputedStyle[i].Value
		}
	}
	if v,ok:=this.computedStyleMap[key];ok {
		return v
	}
	return ""
}

func (this *nodeInfo) GetElementByTagName(tag string) *nodeInfo {
	if this.NodeName==tag {
		return this
	} else {
		for _,v:= range this.Children {
			if ret:=v.GetElementByTagName(tag);ret!=nil {
				return ret
			}
		}
	}
	return nil
}

type CSSComputedStyleProperty struct {
	Name string
	Value string
}

type ruleMatch struct {
	Rule				CSSRule
	MatchingSelectors	[]int
}

type CSSRule struct {
	StyleSheetId	string
	SelectorList	selectorList
	Origin			string
	Style			CSSStyle
	Media			[]CSSMedia
}

type CSSStyle struct {
	StyleSheetId string
	CssProperties	[]CSSProperty
	ShorthandEntries []shorthandEntry
	CssText string
	Range	sourceRange
}

type CSSMedia struct {
	Name string
	Source		string
	SourceURL	string
	Range				sourceRange
	ParentStyleSheetId string
	MediaList	[]mediaQuery
}

type BoxModel struct {
	Content		[]int
	Padding		[]int
	Border		[]int
	Margin		[]int
	Width		int
	Height		int
	//ShapeOutside	ShapeOutsideInfo
}

type ShapeOutsideInfo struct {
	Bounds 		[]int
	// Shape 		[]interface{}
	//MarginShape		[]interface{}
}
type mediaQuery struct {
	Expressions []mediaQueryExpression
	Active bool
}

type mediaQueryExpression struct {
	Value int
	Unit  string
	Feature string
	ValueRange sourceRange
	ComputedLength int
}

type shorthandEntry struct {
	Name string
	Value string
}

type CSSProperty struct {
	Name string
	Value string
	Important	bool
	Implicit bool
	Text string
	ParsedOk bool
	Disabled bool
	Range sourceRange
}
type selectorList struct {
	Selectors	[]selector
	Text		string
}

type selector struct {
	Value	string
	Range	sourceRange
}

type sourceRange struct {
	StartLine int
	StartColumn int
	EndLine int
	EndColumn int
}

type evalResultInfo struct {
	Type		string
	ObjectId	string
	ClassName	string
	Description string
	Value 		interface {}
}

type exceptionDetailsInfo struct {
	Text string
	Url  string
	Line int
	Column int
	StackTrace []stackTraceInfo
}

type stackTraceInfo struct {
	FunctionName string
	ScriptId 	 string
	Url 		 string
	LineNumber	 int
	ColumnNumber int
}

type paramsInfo struct {
	FrameId		string		 `json:"frameId"`
	RequestId 	string		 `json:"requestId"`
	Request 	requestInfo  `json:"request"`
	Response 	responseInfo `json:"response"`
	Context 	contextInfo
	DocumentURL string
	Type		string
	LoaderId	string
	Timestamp   float64
	EncodedDataLength int
	DataLength	int
	Message 	messageInfo
	MessageStr 	string
}

type contextInfo struct {
	Id int
	FrameId string
	Name string
	Origin string
	IsPageContext bool
}

type messageInfo struct {
	Text string
	Source string
	Level string
	Timestamp float64
	Type string
	Line int
	Column int
	Url string
	NetworkRequestId string
}

type requestInfo struct {
	Url	string
	Method string
	Headers map[string]string
}

type responseInfo struct {
	Url	string					`json:"url"`
	Status int					`json:"status"`
	StatusText string			`json:"statusText"`
	MimeType string				`json:"mimeType"`
	Headers map[string]string
}

type TesterFunction func(x IInMessage) bool

type WebSocket struct {
	ws *websocket.Conn
	Debug bool
	incomingMessages chan IInMessage
	outgoingMessages chan IOutMessage
	FatalError chan error
	msgId int

	ReaderStatus chan error
	openRequests map[int] *OpenRequestInfo
//	openRequestsIdsChan chan int
	waitWorFunction TesterFunction
	waiterMessage chan IInMessage

	openRequestsInfoChan  chan int
	openRequestsInfoOutChan chan *OpenRequestInfo


	SentHttpRequests  map[string] HttpRequestInfo
	ActiveFrames	  map[string] int
	LastRequestTimeSeq int64

	JSCallback chan string
}

type OpenRequestInfo struct {
	InMessage chan IInMessage
	Method	  string
}

type HttpRequestInfo struct {
	RequestId  string
	State int
	Url string
	UrlChain []string
	Method string
	StartTimestamp float64
	FinishTimestamp float64
	DataLength int
	EncodedDataLength int
	ResponseUrl string
	ResponseContentType string
	ResponseContentLength int
	StatusText string
	HttpStatus int
	DurationSec float64
}


const (
	STARTED int = 0
	HEADERS int = 1
	PROGRESS int = 2
	FINISHED int = 3
)

func (self HttpRequestInfo) String() string {
	return fmt.Sprintf("%s\t%d",self.Url,self.HttpStatus)
}


func (self InMessage) String() string {
	switch self.Method {
	case "Inspector.detached": return "["+self.Method+"]"

	case "Network.loadingFinished":
		// {RequestId:19345.1,Timestamp:1422749461.159660,EncodedDataLength:495}
		return fmt.Sprintf("[%s] Params{RequestId:%s,Timestamp:%f,EncodedDataLength:%d}",self.Method,self.Params.RequestId,self.Params.Timestamp,self.Params.EncodedDataLength)
	case "Network.dataReceived":
		//{"method":"Network.dataReceived","params":{"requestId":"19251.1","timestamp":1422749288.6438,"dataLength":279,"encodedDataLength":495}}
		return fmt.Sprintf("[%s] Params{RequestId:%s,Timestamp:%f,DataLength:%d,EncodedDataLength:%d}",self.Method,self.Params.RequestId,self.Params.Timestamp,
			self.Params.DataLength,self.Params.EncodedDataLength)
	case "Network.requestWillBeSent":
		// {"method":"Network.responseReceived","params":{"requestId":"19912.1","frameId":"19912.1","loaderId":"19912.2","timestamp":1422750091.336,
		// "type":"Document","response":{"url":"http://127.0.0.1/404.php","status":404,"statusText":"Not Found","headers":{"Date":"Sun, 01 Feb 2015 00:21:31 GMT","Server":"Apache/2.4.7 (Ubuntu)","Connection":"Keep-Alive","Keep-Alive":"timeout=5, max=100","Content-Length":"279","Content-Type":"text/html; charset=iso-8859-1"},"mimeType":"text/html","connectionReused":true,"connectionId":452,"encodedDataLength":-1,"fromDiskCache":false,"fromServiceWorker":false,"timing":{"requestTime":1422750091.3308,"proxyStart":-1,"proxyEnd":-1,"dnsStart":-1,"dnsEnd":-1,"connectStart":-1,"connectEnd":-1,"sslStart":-1,"sslEnd":-1,"serviceWorkerFetchStart":-1,"serviceWorkerFetchReady":-1,"serviceWorkerFetchEnd":-1,"sendStart":2.34399997862056,"sendEnd":2.55199999082834,"receiveHeadersEnd":3.23199998820201},"headersText":"HTTP/1.1 404 Not Found\r\nDate: Sun, 01 Feb 2015 00:21:31 GMT\r\nServer: Apache/2.4.7 (Ubuntu)\r\nContent-Length: 279\r\nKeep-Alive: timeout=5, max=100\r\nConnection: Keep-Alive\r\nContent-Type: text/html; charset=iso-8859-1\r\n\r\n","requestHeaders":{"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8","Connection":"keep-alive","Accept-Encoding":"gzip, deflate, sdch","Host":"127.0.0.1","Accept-Language":"en-US,en;q=0.8","User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2289.0 Safari/537.36"},"requestHeadersText":"GET /404.php HTTP/1.1\r\nHost: 127.0.0.1\r\nConnection: keep-alive\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2289.0 Safari/537.36\r\nAccept-Encoding: gzip, deflate, sdch\r\nAccept-Language: en-US,en;q=0.8\r\n\r\n","remoteIPAddress":"127.0.0.1","remotePort":80,"protocol":"http/1.1"}}}<---
		return fmt.Sprintf("[%s] Params{RequestId:%s,Timestamp:%f,FrameId:%s,LoaderId:%s,DocumentURL:%s,Type:%s} Params.Request{Url:%s,Method:%s,Headers:%v}",
			self.Method,self.Params.RequestId,self.Params.Timestamp,self.Params.FrameId,self.Params.LoaderId,self.Params.DocumentURL,
			self.Params.Type,
			self.Params.Request.Url,self.Params.Request.Method,
			self.Params.Request.Headers,
		)
	case "Network.responseReceived":
		return fmt.Sprintf("[%s] Params{RequestId:%s,Timestamp:%f,FrameId:%s,LoaderId:%s,Type:%s} Params.Response{Url:%s,Status:%d,StatusText:%s,Headers:%v}",
			self.Method,self.Params.RequestId,self.Params.Timestamp,self.Params.FrameId,self.Params.LoaderId,
			self.Params.Type,
			self.Params.Response.Url,self.Params.Response.Status,self.Params.Response.StatusText,
			self.Params.Response.Headers,
		)
	case "Page.javascriptDialogOpening":
		return fmt.Sprintf("[%s] Params{MessageStr:%s}",self.Method,self.Params.MessageStr)
	case "Page.javascriptDialogClosed":
		return fmt.Sprintf("[%s]",self.Method)
	case "Page.domContentEventFired":
		// {"method":"Page.domContentEventFired","params":{"timestamp":1422748847.72038}}
		return fmt.Sprintf("[%s] Params{Timestamp:%f}",self.Method,self.Params.Timestamp)
	case "Page.frameStoppedLoading":
		// {"method":"Page.domContentEventFired","params":{"timestamp":1422748847.72038}}
		return fmt.Sprintf("[%s] Params{FrameId:%s}",self.Method,self.Params.FrameId)
	case "Page.frameResized": return "["+self.Method+"]"
	case "Page.frameNavigated":
		//{"method":"Page.frameNavigated","params":{"frame":{"id":"19180.1","loaderId":"19180.2","url":"http://127.0.0.1/404.php","mimeType":"text/html","securityOrigin":"http://127.0.0.1"}}}
		return fmt.Sprintf("[%s] Params{...}",self.Method)
	case "Page.loadEventFired":
		// {"method":"Page.loadEventFired","params":{"timestamp":1422749093.63065}}
		return fmt.Sprintf("[%s] Params{Timestamp:%f}",self.Method,self.Params.Timestamp)
	case "Console.messageAdded":
		//{"method":"Console.messageAdded","params":{"message":{"source":"network","level":"error","text":"Failed to load resource: the server responded with a status of 404 (Not Found)","timestamp":1422749664.94974,"type":"log","line":0,"column":0,"url":"http://127.0.0.1/404.php","networkRequestId":"19449.1"}}}
		return fmt.Sprintf("[%s]:%f \"%s\"",self.Method,self.Params.Message.Timestamp,self.Params.Message.Text)
	case "Console.messagesCleared": return "["+self.Method+"]"
	case "Runtime.executionContextCreated":
		// {"method":"Runtime.executionContextCreated","params":{"context":{"id":1,"isPageContext":true,"name":"","origin":"","frameId":"31489.1"}}}
		return fmt.Sprintf("[%s] Params.Context{Id:%d,FrameId:%s,Name:%s,Origin:%s,IsPageContext:%t}",self.Method,
			self.Params.Context.Id,self.Params.Context.FrameId,self.Params.Context.Name,self.Params.Context.Origin,self.Params.Context.IsPageContext)
	case "":
		ret := fmt.Sprintf("[Return:%v]",self.Id)
		if self.Error != nil {
			ret += fmt.Sprintf(" Error=%#v",self.Error)
		}
		if self.Result!= nil {
			ret += fmt.Sprintf(" Result=%#v",self.Result)
		}
		return "UNK:"+ret
	default: return fmt.Sprintf("UNK:%#v",self)
	}
}

func WebSocketConnect(wsUrl string) (*WebSocket,error) {
	var err error
	self := WebSocket{}

	self.ws, err = websocket.Dial(wsUrl, "", wsUrl)

	if err != nil {
		return nil,err
	}

	self.incomingMessages = make(chan IInMessage)
	self.outgoingMessages = make(chan IOutMessage)
	self.FatalError = make(chan error)
	self.ReaderStatus = make(chan error)
	self.msgId = 0

	//self.openRequestsIdsChan = make(chan int)
	self.openRequestsInfoChan = make(chan int)
	self.openRequestsInfoOutChan = make(chan *OpenRequestInfo)

	self.openRequests = make(map[int] *OpenRequestInfo)

	self.waiterMessage = make(chan IInMessage)
	self.SentHttpRequests  = make(map[string] HttpRequestInfo)
	self.ActiveFrames      = make(map[string] int)
	self.LastRequestTimeSeq = time.Now().Unix()
	self.JSCallback = make(chan string)
	return &self,nil
}

func constructPageJavascriptDialogOpening(data map[string]interface{}) (jsonMsg InMessage,err error) {
	jsonMsg.InBaseMessage = &InBaseMessage{}
	jsonMsg.Params = &paramsInfo{}

	jsonMsg.Method = "Page.javascriptDialogOpening"
	if params,ok:= data["params"];ok {
		if paramsMap,ok:=params.(map[string]interface{});ok {
			if message,ok:=paramsMap["message"];ok {
				if messageStr,ok:=message.(string);ok {
					jsonMsg.Params.MessageStr = messageStr
				} else {
					err =  errors.New("message not string")
					return
				}
			} else {
				jsonMsg.Params.MessageStr = ""
			}
		} else {
			err = errors.New("no paramsMap")
			return
		}
	} else {
		err = errors.New("no params")
		return
	}

	return
}

func (self *WebSocket) readClientMessages() {
	var err error
	for {
		var jsonBaseMsg InBaseMessage
		var jsonMsgIface IInMessage
		var plainData []byte

		plainData,err = websocket.JSON.Receive(self.ws,&jsonBaseMsg)
		var method = jsonBaseMsg.Method

		if err != nil {
			log.Printf("JSON Processing error: %v,PlainData: %v",err,string(plainData))
			break
		}

		switch method {
		case "Page.javascriptDialogOpening":
			var data map[string]interface{}
			err = json.Unmarshal(plainData,&data)
			if err != nil {
				log.Printf("JSON Processing error: %v,PlainData: %v",err,string(plainData))
				break
			}

			if jsonConstructMsg,err2:=constructPageJavascriptDialogOpening(data);err != nil {
				log.Printf("JSON Processing error: %v constructError:%v PlainData: %v",err2,err,string(plainData))
				err = err2
				break
			} else {
				jsonMsgIface = jsonConstructMsg
			}

		case "":

			var constructDefault = true
			if jsonBaseMsg.Id != 0 {
				self.openRequestsInfoChan <- jsonBaseMsg.Id
				chanInfo:= <- self.openRequestsInfoOutChan
				if ( chanInfo == nil ) {
					log.Println("WARN: readClientMessages - chanInfo=nil")
					break
				} else {
					switch chanInfo.InMessage {

					default:
					}
				}
			}
			if constructDefault {
				var jsonConstructMsg InMessage
				err = json.Unmarshal(plainData, &jsonConstructMsg)
				if err != nil {
					log.Printf("JSON Processing error: %v,PlainData: %v", err, string(plainData))
					break
				}
				jsonMsgIface = jsonConstructMsg
			}
		default:
			var jsonConstructMsg InMessage
			err = json.Unmarshal(plainData,&jsonConstructMsg)
			if err != nil {
				log.Printf("JSON Processing error: %v,PlainData: %v",err,string(plainData))
				break
			}
			jsonMsgIface = jsonConstructMsg
		}


		switch method {
		case "Network.requestWillBeSent":
			var jsonMsg = jsonMsgIface.(InMessage)
			if ( jsonMsg.Params.RequestId == "") {
				log.Printf("ERROR Network.requestWillBeSent Params.RequestId - is empty")
			} else if v,ok := self.SentHttpRequests[jsonMsg.Params.RequestId];ok {
				self.ActiveFrames[jsonMsg.Params.FrameId]=1
				if ( v.State == STARTED ) {
					// 302 chain
					v.UrlChain = append(v.UrlChain,jsonMsg.Params.Request.Url)
					self.SentHttpRequests[jsonMsg.Params.RequestId] = v
				} else {
					log.Printf("ERROR Network.requestWillBeSent Params.RequestId - already exists")
				}
			} else {
				self.LastRequestTimeSeq = time.Now().Unix()
				self.ActiveFrames[jsonMsg.Params.FrameId]=1
				self.SentHttpRequests[jsonMsg.Params.RequestId] = HttpRequestInfo{
					RequestId: jsonMsg.Params.RequestId,
					Url:jsonMsg.Params.Request.Url,
					Method:jsonMsg.Params.Request.Method,
					StartTimestamp:jsonMsg.Params.Timestamp,
					State: STARTED,
					UrlChain: make([]string,0),
				}
			}
		case "Network.responseReceived":
			var jsonMsg = jsonMsgIface.(InMessage)
			if v,ok := self.SentHttpRequests[jsonMsg.Params.RequestId];ok {
				if ( v.State != STARTED) {
					log.Printf("ERROR %s Params.RequestId - %s is in Status %d",jsonMsg.Method,jsonMsg.Params.RequestId,v.State)
				} else {
					v.FinishTimestamp = jsonMsg.Params.Timestamp
					v.HttpStatus 	 =  jsonMsg.Params.Response.Status
					v.StatusText =  jsonMsg.Params.Response.StatusText
					v.ResponseUrl =  jsonMsg.Params.Response.Url
					if vv,ok:=jsonMsg.Params.Response.Headers["Content-Length"];ok {
						if vvv,err0 := strconv.Atoi(vv);err0 == nil  {
							v.ResponseContentLength = vvv
						} else {
							log.Printf("ERROR %s Content-Length - %s is not Int - IGNORE IT",jsonMsg.Method,vv)
						}
					}
					if vv,ok:=jsonMsg.Params.Response.Headers["Content-Type"];ok {
						v.ResponseContentType = vv
					}


					v.DurationSec  = v.FinishTimestamp - v.StartTimestamp
					v.State = HEADERS

					self.SentHttpRequests[jsonMsg.Params.RequestId] = v

				}
			} else {
				log.Printf("ERROR %s Params.RequestId - %s Not found",jsonMsg.Method,jsonMsg.Params.RequestId)
			}
		case "Network.dataReceived":
			var jsonMsg = jsonMsgIface.(InMessage)

			if v,ok := self.SentHttpRequests[jsonMsg.Params.RequestId];ok {
				if ( v.State != PROGRESS && v.State != HEADERS ) {
					log.Printf("ERROR %s Params.RequestId - %s is in Status %d",jsonMsg.Method,jsonMsg.Params.RequestId,v.State)
				} else {
					v.FinishTimestamp = jsonMsg.Params.Timestamp
					v.EncodedDataLength = jsonMsg.Params.EncodedDataLength
					v.DataLength = jsonMsg.Params.DataLength
					v.DurationSec  = v.FinishTimestamp - v.StartTimestamp
					v.State = PROGRESS
					self.SentHttpRequests[jsonMsg.Params.RequestId] = v
				}
			} else {
				log.Printf("ERROR %s Params.RequestId - %s Not found",jsonMsg.Method,jsonMsg.Params.RequestId)
			}

		case "Network.loadingFinished":
			var jsonMsg = jsonMsgIface.(InMessage)

			if v,ok := self.SentHttpRequests[jsonMsg.Params.RequestId];ok {
				if ( v.State != PROGRESS) {
					log.Printf("ERROR %s Params.RequestId - %s is in Status %d",jsonMsg.Method,jsonMsg.Params.RequestId,v.State)
				} else {
					self.LastRequestTimeSeq = time.Now().Unix()
					v.EncodedDataLength = jsonMsg.Params.EncodedDataLength
					v.FinishTimestamp = jsonMsg.Params.Timestamp
					v.DurationSec  = v.FinishTimestamp - v.StartTimestamp
					v.State = FINISHED
					self.SentHttpRequests[jsonMsg.Params.RequestId] = v
				}
			} else {
				log.Printf("ERROR %s Params.RequestId - %s Not found",jsonMsg.Method,jsonMsg.Params.RequestId)
			}
		case "Page.javascriptDialogOpening":
			var jsonMsg = jsonMsgIface.(InMessage)
			self.JSCallback <- jsonMsg.Params.MessageStr
		default:
			self.incomingMessages <- jsonMsgIface
		}

		if ( self.Debug ) {
			jsonMsgStr := fmt.Sprintf("%v",jsonMsgIface)
			if ( strings.HasPrefix(jsonMsgStr,"UNK:")) {
				//log.Printf("WS received Obj : -->%s<--  JSON -->%s<---",jsonMsgStr[4:],string(plainData))
				log.Printf("WS received JSON: %s",string(plainData))
			} else {
				log.Printf("WS received: %s",jsonMsgStr)
			}
		}

	}

	log.Printf("readClientMessages fatalError:%v",err)
	self.FatalError <- err
}


func (self *WebSocket) sendMessage(msg IOutMessage) (err error) {
	msg.SetId(self.msgId)
	if self.Debug {
		log.Printf("sendMessage: %#v",msg)
	}

	msgJson,_ := json.Marshal(msg)

	if self.Debug {
		log.Printf("sendMessage(Json): %#v",string(msgJson))
	}

	_,err = self.ws.Write(msgJson)

	return
}

func (self *WebSocket) Cmd(msg IOutMessage) (ret InMessage,err error) {

	self.outgoingMessages <- msg
	chanInfo:= <- self.openRequestsInfoOutChan

	ret0:= <- chanInfo.InMessage

	ret = ret0.(InMessage)

	if ret.Error != nil {
		return ret,errors.New(fmt.Sprintf("WS.CMD error: %#v",ret.Error))
	}

	return
}


func (self *WebSocket) WaitFor(tester TesterFunction ) (ret IInMessage,err error) {
	if self.waitWorFunction!=nil {
		log.Panic("WaitFor called with other active self.waitWorFunction")
	}
	self.waitWorFunction = tester
	ret = <-self.waiterMessage
	return ret,nil
}

func (self *WebSocket) WsReader() {
	go self.readClientMessages()

	for {
		select {
		case requestId:= <- self.openRequestsInfoChan:
			if r,ok :=self.openRequests[requestId];ok {
				self.openRequestsInfoOutChan <- r
			} else {
				log.Printf("WARN: openRequestsInfoChan - request method on unknown requestId:%d",requestId)
				self.openRequestsInfoOutChan <- nil
			}
		case inMsg:= <-self.incomingMessages:
			msgId := inMsg.(InMessage).Id
			if r,ok :=self.openRequests[msgId];ok {
				r.InMessage <- inMsg
				delete(self.openRequests,msgId)
			}
			if self.waitWorFunction != nil {
				if self.waitWorFunction(inMsg) {
					self.waiterMessage <- inMsg
					self.waitWorFunction = nil
				}
			}
		case err:= <-self.FatalError:
			self.ReaderStatus <- err
			break
		case outMsg:= <- self.outgoingMessages:
			self.msgId++
			v := &OpenRequestInfo{make(chan IInMessage),outMsg.GetMethod()}
			self.openRequests[self.msgId] = v
			self.openRequestsInfoOutChan <- v

			if err:= self.sendMessage(outMsg);err!=nil {
				self.ReaderStatus <- err
				break
			}
		}
	}
}

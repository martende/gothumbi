package domutil

import (
	"regexp"
	"fmt"
	"errors"
)


type FontInfo struct {
	Url,fontFamily,fontFormat,fontStyle,fontWeight,unicodeRange string
}

func (self FontInfo) String() string {
	result := fmt.Sprintf("@font-face { font-family: '%s'; src: url(%s)", self.fontFamily, self.Url);
	if ( self.fontFormat != "") {
		result+= " format("+self.fontFormat+");"
	} else {
		result+= ";"
	}
	if ( self.fontStyle != "") {
		result+= " font-style: "+self.fontStyle+";"
	}
	if ( self.fontWeight != "") {
		result+= " font-weight: "+self.fontWeight+";"
	}
	if ( self.unicodeRange != "") {
		result+= " unicode-range: "+self.unicodeRange+";"
	}
	result+= " }\n"
	return result
}

var reFF = regexp.MustCompile("font-family: (?:'(.*?)'|(.*?));")
//var reSRC = regexp.MustCompile("src: url\\((.*?)\\)(?: format\\((.*?)\\))?;")
var reSRC = regexp.MustCompile("src: (.*?);")
var reSRC2 = regexp.MustCompile("url\\((.*?)\\)(?: format\\((.*?)\\))?,")
var re3 = regexp.MustCompile("font-style: (.*?);")
var re4 = regexp.MustCompile("font-weight: (.*?);")
var re5 = regexp.MustCompile("unicode-range: (.*?);")

func ExtractFont(fontFace string) (fi FontInfo,err error){

	if rx := reFF.FindAllStringSubmatch(fontFace, -1); len(rx) > 0 {
		fontFamily := rx[0][1]
		if fontFamily == "" {
			fontFamily = rx[0][2]
		}

		var fontUrl, fontStyle, fontWeight, unicodeRange, fontFormat string

		if rx := reSRC.FindAllStringSubmatch(fontFace, -1); len(rx) > 0 {
			var t = rx[0][1] + ","
			if t2:=reSRC2.FindAllStringSubmatch(t,-1);len(t2) > 0 {
				if len(t2) == 1 {
					fontUrl = t2[0][1]
					if (len(rx[0]) > 2) {
						fontFormat = t2[0][2]
					}
				} else {
					var formats = make(map[string] int)
					var availFormats = []string{"woff","svg","embedded-opentype",""}

					for i,v := range t2 {
						formats[v[2]] = i
					}
					var found = false
					for _,k := range availFormats {
						if v,ok:=formats[k]; ok {
							fontUrl = t2[v][1]
							fontFormat = k
							found = true
							break
						}
					}
					if ! found {
						fontUrl = t2[0][1]
						fontFormat = t2[0][2]
					}
				}
			} else {
				err = errors.New("ExtractFont reSRC2 failed")
				return
			}
		}
		if rx := re3.FindAllStringSubmatch(fontFace, -1); len(rx) > 0 {
			fontStyle = rx[0][1]
		}
		if rx := re4.FindAllStringSubmatch(fontFace, -1); len(rx) > 0 {
			fontWeight = rx[0][1]
		}
		if rx := re5.FindAllStringSubmatch(fontFace, -1); len(rx) > 0 {
			unicodeRange = rx[0][1]
		}
		fi.Url = fontUrl
		fi.fontStyle = fontStyle
		fi.fontWeight = fontWeight
		fi.unicodeRange = unicodeRange
		fi.fontFormat = fontFormat
		fi.fontFamily = fontFamily
		//log.Print("Font: " + result)
	} else {
		err = errors.New("ExtractFont reFF failed")
	}

	return
}

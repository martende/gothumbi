package domutil

import (
	"crypto/sha1"
	"fmt"
	"io/ioutil"
	"log"
	"errors"
	"strings"
	"regexp"
	"encoding/base64"
)


var resourcesCache = make(map[string]resourceInfo)

type resourceInfo struct {
	Ctype 	 string
	Ext 	 string
	FileName string
	Err		 error
}


func ventiurl(tp string,url string,ws*WebSocket,tmpDir *string) string {
	var resource resourceInfo

	retVal := func(v *resourceInfo) string {
		if v.Err != nil {
			switch tp {
			case "src": return TRANSPARENT1x1GIF
			case "font": return ""
			default:return "about:blank"
			}
		}
		return v.FileName
	}

	getCache := func() (string,bool) {
		if v,ok:=resourcesCache[url];ok {
			return retVal(&v),true
		}
		return "",false
	}

	updateCache := func() string {
		resourcesCache[url] = resource
		if resource.Err != nil {
			log.Printf("WARN: ventiurl error: (%v) url: (%s) ",resource.Err,url)
		}
		log.Printf("resource %#v",resource)
		return retVal(&resource)
	}

	if v,ok:=getCache();ok {
		return v
	}

	var data []byte = nil
	var err error

	re1, _ := regexp.Compile("^data:([a-z0-9/+-]+)(;[a-z0-9]+=[^;]*)*;base64,\\s*([0-9A-Za-z/+]+=*)$")

	if rx := re1.FindAllStringSubmatch(url, -1); len(rx) > 0 {
		resource.Ctype = rx[0][1]
		if data, err = base64.StdEncoding.DecodeString(rx[0][3]); err != nil {
			resource.Err = err
			return updateCache()
		}
	} else if ok,_:=regexp.MatchString("^data:", url) ;ok {
		resource.Err = errors.New("Strange dataurl")
		return updateCache()
	} else if ok,_:=regexp.MatchString("^about:", url);ok {
		resource.FileName = "about:blank"
		return updateCache()
	} else if ok,_:=regexp.MatchString("^(ftp|http|https|file)://", url);!ok {
		resource.Err  = errors.New("Strange url")
		return updateCache()
	}

	if data == nil {
		if r,ok:=resourcesPool[url];ok {
			ret,err:=ws.Cmd(&NetworkGetResponseBody{OutMessage{Method :"Network.getResponseBody"},NetworkGetResponseBodyParams{RequestId:r.RequestId}})
			if err != nil {
				resource.Err = errors.New(fmt.Sprintf("Resource ERROR: %v",err))
				return updateCache()
			}
			if ret.Result.Base64Encoded {
				if data, err = base64.StdEncoding.DecodeString(ret.Result.Body); err != nil {
					resource.Err = errors.New(fmt.Sprintf("Resource ERROR: %v",err))
					return updateCache()
				}
			} else {
				data = []byte(ret.Result.Body)
			}
		} else {
			resource.Err = errors.New(fmt.Sprintf("Resource ERROR: NOTFOUND"))
			return updateCache()
		}
	}

	h := sha1.New()
	h.Write(data)

	switch resource.Ctype {
	case "image/png": resource.Ext = ".png"
	case "image/jpeg": resource.Ext = ".jpg"
	default:
	}

	if resource.Ext == "" {
		re1, _ := regexp.Compile("\\.([a-z0-9]+)(?:#|$)")
		if rx := re1.FindAllStringSubmatch(url, -1); len(rx) > 0 {
			resource.Ext = "." + rx[0][1]
		}
	}

	resource.FileName = fmt.Sprintf("%x", h.Sum(nil)) + resource.Ext
	var localFname = *tmpDir + "/" + resource.FileName

	if strings.HasPrefix(url,"data:") {
		log.Printf("ventiurl: Save file URL:(data:...) to %s",localFname)
	} else {
		log.Printf("ventiurl: Save file URL:(%s) to %s",url,localFname)
	}

	err = ioutil.WriteFile(localFname,data,0644)
	if err != nil {
		resource.Err = errors.New(fmt.Sprintf("Resource ERROR %v",err))
		return updateCache()
	}


	return updateCache()


}

func ventiUploadData(ventiData []byte, ext string, ctype string,tmpDir string) (string,error) {
	h := sha1.New()
	h.Write(ventiData)
	fileName := fmt.Sprintf("%x", h.Sum(nil)) + ext

	err := ioutil.WriteFile(tmpDir + "/" + fileName,ventiData,0644)

	if err != nil {
		log.Println("Venti upload data err:" + err.Error())
		return "",err
	}
	return fileName,nil
}

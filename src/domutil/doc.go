package domutil

type DocInfo struct {
	usedBodyColor bool
	usedBodyImage bool
	overflowX string
	overflowY string
	baseUrl  string
	innerWidth string
	htmlEl *nodeInfo
	bodyEl *nodeInfo
	DocType DocType
	InIframe bool
}

type DocType struct {
	Type,PublicId,SystemId  string
}

func (this DocType) String() (ret string) {
	if this.Type == "" {
		return  "<!DOCTYPE html>"
	}
	ret = "<!DOCTYPE " + this.Type
	if (this.PublicId != "") {
		ret += " PUBLIC \"" + this.PublicId + "\""
	}
	if (this.SystemId != "") {
		ret += " \"" + this.SystemId + "\""
	}
	ret +=">"
	return
}


package main

import (
	"fmt"
	"os/exec"
	"time"
	"flag"
	"strconv"
	"log"
	"strings"
	"io"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"errors"
	"os"
	"domutil"
)



const chromeBin = "/home/belka2/MProg/Webkit/chromium/src/out/Default/chrome"
var chromeArgs = []string{ "--disable-setuid-sandbox","--disable-web-security", "data:" }

var debugPort *int = flag.Int("port", 12820, "Debug Port")
var debugChromeStdout *bool = flag.Bool("debugChromeStdout",false,"Print Chrome Stdout")
var cookieStr *string = flag.String("cookie","","Start Cookies")
var userAgent *string = flag.String("userAgent","","UserAgent")

var mainUrl *string = flag.String("url",
//	"https://www.facebook.com/plugins/like.php?action=like&app_id=172525162793917&channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FDU1Ia251o0y.js%3Fversion%3D41%23cb%3Df3381c1174%26domain%3Dwww.hiddenoctopus.com%26origin%3Dhttp%253A%252F%252Fwww.hiddenoctopus.com%252Ff3ac18c584%26relation%3Dparent.parent&container_width=56&font=arial&href=http%3A%2F%2Fwww.hiddenoctopus.com%2Fplay%2Fdoodle-defender-2&layout=box_count&locale=en_US&ref=.VN88t1n_HEg.like&sdk=joey&send=false&show_faces=false&width=55",
	//"http://www.hiddenoctopus.com/play/upgrade-complete-2",
	//"https://vine.co/v/OP77aamTbgJ",
	//"http://www.ibm.com/us/en/",
	//"http://www.rafael.co.il/Marketing/196-1704-en/Marketing.aspx",
	//"http://www.hiddenoctopus.com/",
	//"https://www.youtube.com/watch?v=z-Qcbun-Z64",
	//"http://www.indiegala.com/monday",
	//"http://127.0.0.1/404.php", // 404 error
	"http://127.0.0.1/t.php", // Frame with 5 timeout
	//"http://lenta.ru/",


	//"http://127.0.0.1:9000/",
	// "http://hghltd.yandex.net/yandbtm?fmode=inject&url=http%3A%2F%2Fsosihuy.tumblr.com%2F&tld=ru&lang=en&la=&text=sosihuy&l10n=ru&mime=html&sign=c52aa19367aa4c9d103618be58182dfa&keyno=0",
	//"http://synrc.com/lj/erlang.syntax",
	//"http://127.0.0.1/iframe1.php", // Frame with innerem frame
	//"http://127.0.0.1/302.php", // Fair 302 main page redirect
	"Url to fetch")

func outReader(pipe io.Reader,waiter chan int,started chan int) {
	var msg = make([]byte, 0x40000)
	finished := func() { waiter <- 1 }

	defer finished()

	var err error
	isStarted:= false

	for {
		n := 0
		if n, err = pipe.Read(msg); err != nil {
			if err == io.EOF {
				break
			}
			log.Printf("ERROR: outReader %#v",err)
			return
		}
		if ! isStarted {
			isStarted = true
			started <- 1
		}
		if *debugChromeStdout {
			log.Printf("READ:[%s]", msg[:n])
		}

	}
}


func getWebSocketUrl() (webSocketUrl,webSocketBgUrl string,err error){

	type StatusJson struct {
		Description string
		DevtoolsFrontendUrl string
		Id string
		Title string
		Url string
		WebSocketDebuggerUrl string
		Type string
	}

	url := fmt.Sprintf("http://127.0.0.1:%d/json",*debugPort)

	var res *http.Response

	for  i:= 0 ; i < 10 ; i++ {
		res, err = http.Get(url)
		if err != nil {
			log.Printf("WARN:getWebSocketUrl #%v",err)
			time.Sleep(1000 * time.Millisecond)
		} else {
			break
		}
	}

	if ( res == nil ) {
		err =  errors.New("cant connect to chrome debug url: " + url)
		return
	}

	var body []byte
	if body, err = ioutil.ReadAll(res.Body); err!= nil {
		return
	}

	//log.Print(string(body))

	statusJson:= make([]StatusJson,0)

	json.Unmarshal(body, &statusJson)
	//statusJson[0].description ="AAAAA"



	var WebSocketBackgroundUrl = ""
	var WebSocketPageUrl = ""

	for _,v := range statusJson {
		if v.Type == "background_page" && WebSocketBackgroundUrl == "" {
			WebSocketBackgroundUrl = v.WebSocketDebuggerUrl
		}
		if v.Type == "page" && v.Url == "data:" && WebSocketPageUrl == "" {
			WebSocketPageUrl = v.WebSocketDebuggerUrl
		}
	}

	if  WebSocketPageUrl == "" {
		err =  errors.New("Bad response debug url: '" + url + "' body=" + string(body))
		return
	}

	if  WebSocketBackgroundUrl == "" {
		err =  errors.New("Bad response debug url (no WebSocketBackgroundUrl): '" + url + "' body=" + string(body))
		return
	}

	return WebSocketPageUrl,WebSocketBackgroundUrl,nil
}

func executeJs(contextId int,ws *domutil.WebSocket,jsString string,args interface{}) (res domutil.InMessage,err error) {
	byteArgs,err := json.Marshal(args)
	if err != nil {
		return
	}

	res, err = ws.Cmd(&domutil.RuntimeEvaluateMessage{domutil.OutMessage{Method :"Runtime.evaluate"},domutil.RuntimeEvaluateMessageParams{
		ContextId:contextId,
		ReturnByValue: true,
		ObjectGroup:"",
		Expression: fmt.Sprintf("(%s).apply(null,[%s])",jsString,string(byteArgs)),
	}})

	if err != nil {
		return
	}

	if ( res.Result.WasThrown ) {
		err = errors.New("ExecuteJs failed: " + res.Result.ExceptionDetails.Text)
	}

	return
}


func executeJsStrArg(contextId int,ws *domutil.WebSocket,jsString string,args string) (res domutil.InMessage,err error) {
	res, err = ws.Cmd(&domutil.RuntimeEvaluateMessage{domutil.OutMessage{Method :"Runtime.evaluate"},domutil.RuntimeEvaluateMessageParams{
	ContextId:contextId,
	ReturnByValue: true,
	ObjectGroup:"",
	Expression: fmt.Sprintf("(%s).apply(null,[%s])",jsString,args),
}})

	log.Println(res,err)

	if err != nil {
		return
	}

	if ( res.Result.WasThrown ) {
		err = errors.New("ExecuteJs failed: " + res.Result.ExceptionDetails.Text)
	}

	return
}


func setCookies(ws *domutil.WebSocket,cookieFile string) (err error) {
	content,err := ioutil.ReadFile(cookieFile)
	if ( err != nil) {
		return
	}

	var v []domutil.AddCookieMessageParams
	err = json.Unmarshal(content,&v)
	if ( err != nil) {
		return
	}

	for i:= range v {
		_,err = ws.Cmd(&domutil.AddCookieMessage{domutil.OutMessage{Method :"Page.addCookie"},v[i]})
		if err != nil {
			log.Printf("Page.addCookie error %v",err)
			return
		}
	}

	return
}

func extractStringFromMap(m map[string]interface{},keys []string) ([]string,error) {
	res:=make([]string,len(keys))
	for i:=0;i<len(keys);i++ {
		r,ok := m[keys[i]]
		if ! ok {
			return nil,errors.New(fmt.Sprintf("%s not found",keys[i]))
		}
		rs,ok:=r.(string)
		if ! ok {
			return nil,errors.New(fmt.Sprintf("%s not string",keys[i]))
		}
		res[i]=rs
	}
	return res,nil
}



func prepareRequests(ws *domutil.WebSocket) (err error) {
	_, err = ws.Cmd(&domutil.OutMessage{Method :"Page.enable"} )
	if err != nil {
		log.Printf("ERROR: Page.enable %v", err)
		return
	}

	//log.Printf("Page.enable %v", result)

	_, err = ws.Cmd(&domutil.OutMessage{Method :"Console.enable"} )
	if err != nil {
		log.Printf("Console.enable %v", err)
		return
	}

	_, err = ws.Cmd(&domutil.OutMessage{Method :"Network.enable"} )
	if err != nil {
		log.Printf("Network.enable %v", err)
		return
	}


	if *userAgent != "" {
		_,err = ws.Cmd(&domutil.SetUserAgentOverrideMessage{domutil.OutMessage{Method :"Network.setUserAgentOverride"},domutil.SetUserAgentOverrideMessageParams{*userAgent}})
		if err != nil {
			log.Printf("Network.setUserAgentOverride %v", err)
			return
		}

	}

	if *cookieStr != "" {
		err = setCookies(ws,*cookieStr)
		if err != nil {
			log.Printf("setCookies %v", err)
			return
		}
	}

	return
}

func setWindowSize(ws *domutil.WebSocket,width,height int) (err error ) {
	ws.Debug = true

	cmdJsArgs:= fmt.Sprintf("{height:%d,width:%d}",height,width)
	for  i:=0 ; i < 5 ;i++ {
		_,err = executeJsStrArg(1,ws,"updateWindow",cmdJsArgs)
		if err == nil {
			return
		}
		log.Printf("Update window error Try %d/5 Err: %#v",i+1,err)
		time.Sleep(500*time.Millisecond)
	}

	ws.Debug = false

	return
}

func processWebsocket(ws *domutil.WebSocket,wsBg *domutil.WebSocket,tmpDir string) (err error) {

	if err = setWindowSize(wsBg,1024,768);err!=nil {
		log.Printf("ERROR: setWindowSize failed %#v",err)
		return
	}


	if err = prepareRequests(ws) ; err != nil {
		log.Printf("prepareRequests failed %v",err)
		return
	}

	_, err = ws.Cmd(&domutil.OutMessage{Method :"Runtime.enable"} )
	if err != nil {
		log.Printf("Runtime.enable %v", err)
		return
	}

	openResult,err := ws.Cmd(&domutil.PageNavigateMessage{domutil.OutMessage{Method :"Page.navigate"},domutil.PageNavigateMessageParams{Url:*mainUrl}})
	if err != nil {
		log.Printf("Open error %v",err)
		return
	}

	log.Println("--------------------" )
	log.Printf("OpenResult MainFrameId:%s",openResult.Result.FrameId)
	log.Println("--------------------" )


	go func() {
		for {
			inMsg := <-ws.JSCallback
			log.Println("JSCallback inoked " + inMsg)

			ws.Cmd(&domutil.PageHandleJavascriptDialogMessage{domutil.OutMessage{Method :"Page.handleJavaScriptDialog"},domutil.PageHandleJavascriptDialogMessageParams{
				Accept: true,
				PromptText: "",
			}})

		}
	}()
	// Wait for execution context Message

	executionContextResultChan := make(chan domutil.InMessage)
	executionContextErrChan := make(chan error)

	go func() {
		executionContextResult,err := ws.WaitFor(func (item domutil.IInMessage) bool {
			switch item.(type) {
				case  domutil.InMessage: return item.(domutil.InMessage).Method == "Runtime.executionContextCreated"
			}
			return false
		} )
		if err != nil {
			log.Printf("Wait executionContextCreated %v",err)
			executionContextErrChan <- err
		} else {
			executionContextResultChan <- executionContextResult.(domutil.InMessage)
		}
	}()

	_, err = ws.Cmd(&domutil.OutMessage{Method :"Runtime.enable"} )
	if err != nil {
		log.Printf("Runtime.enable %v", err)
		return
	}

	var firstContextId int

	select {
	case executionContextResult:= <- executionContextResultChan:
		firstContextId = executionContextResult.Params.Context.Id
		if firstContextId == 0 {
			log.Printf("mainContextId not found in executionContextResult %v",executionContextResult)
			return
		}
	case err = <- executionContextErrChan:
		log.Printf("Wait executionContextCreated %v",err)
		return
	}

	log.Printf("mainContextId = %d",firstContextId)


	// TODO: Page.frameStoppedLoading race conditions
	log.Println("----------- WAITFOR--- Page.frameStoppedLoading")
	waitResult,err := ws.WaitFor(func (item domutil.IInMessage) bool {
		switch item.(type) {
		case  domutil.InMessage: return item.(domutil.InMessage).Method == "Page.frameStoppedLoading" && item.(domutil.InMessage).Params.FrameId == openResult.Result.FrameId
		}
		return false
	} )

	if err != nil {
		log.Printf("Load error %v",err)
		return
	}


	// Test Page on errror

	var page *domutil.HttpRequestInfo
	for _,v := range ws.SentHttpRequests {
		if v.Url == *mainUrl {
			page = &v
			break
		}
	}

	if ( page == nil ) {
		return errors.New("Requested page was not loaded")
	}

	if ( page.HttpStatus != 200 ) {
		return errors.New(fmt.Sprintf("Requested page HTTPStatus:%d State:%d",page.HttpStatus,page.State ))
	}



	log.Printf("Page Succesfully Loaded: %v",waitResult)

	for ws.LastRequestTimeSeq + 3 > time.Now().Unix() {
		time.Sleep(1000 * time.Millisecond)
	}


	/*log.Println("Capture Screenshot")

	if _,err =ws.Cmd(&OutMessage{Method:"Page.getResourceTree"}); err != nil {
		log.Printf("Page.getResourceTree error %v",err)
		return
	}
	*/

/*
	_,err = ws.Cmd(&CSSGetMatchedStylesForNodeMessage{OutMessage{Method :"CSS.getComputedStyleForNode"},CSSGetMatchedStylesForNodeParams{NodeId:10,ExcludePseudo:false,ExcludeInherited:false}})
	_,err = ws.Cmd(&CSSGetMatchedStylesForNodeMessage{OutMessage{Method :"CSS.getMatchedStylesForNode"},CSSGetMatchedStylesForNodeParams{NodeId:10,ExcludePseudo:false,ExcludeInherited:false}})
	_,err = ws.Cmd(&CSSGetMatchedStylesForNodeMessage{OutMessage{Method :"CSS.getInlineStylesForNode"},CSSGetMatchedStylesForNodeParams{NodeId:10,ExcludePseudo:false,ExcludeInherited:false}})
*/

	log.Println("-------------------\nReady For dump\n-------------------")


	if err = domutil.MakeDump(ws,tmpDir); err != nil {
		log.Printf("mkDump error %v",err)
		return
	}
	/*
		log.Println("DOM.resolveNode")

		var node InMessage
		if node,err =ws.Cmd(&DOMResolveNodeMessage{OutMessage{Method:"DOM.resolveNode"},DOMResolveNodeParams{NodeId:11} }); err != nil {
			log.Printf("DOM.resolveNode error %v",err)
			return
		}

		log.Println("node.Result.Object.ObjectId",node.Result.Object.ObjectId)
		if _,err =ws.Cmd(&RuntimeGetPropertiesMessage{OutMessage{Method:"Runtime.getProperties"},RuntimeGetPropertiesParams{ObjectId:node.Result.Object.ObjectId} }); err != nil {
			log.Printf("Runtime.getProperties error %v",err)
			return
		}

	*/
	return


		/*
		var resourcesTree InMessage
		if resourcesTree,err =ws.Cmd(&OutMessage{Method:"Page.getResourceTree"}); err != nil {
			log.Printf("Page.getResourceTree error %v",err)
			return
		}

		log.Println(resourcesTree.Result)
		*/

/*
	log.Println("--------DUMPED RESOURCES------------" )



	for url,resource := range resourcesCache {
		log.Println(url,resource)
		if r,ok:=resourcesPool[url];ok {
			ret,err:=ws.Cmd(&NetworkGetResponseBody{OutMessage{Method :"Network.getResponseBody"},NetworkGetResponseBodyParams{RequestId:r.RequestId}})
			if err != nil {
				log.Printf("Resource %s ERROR %v",url,err)
				continue
			}
			var data []byte
			if ret.Result.Base64Encoded {
				if data, err = base64.StdEncoding.DecodeString(ret.Result.Body); err != nil {
					log.Printf("Resource %s ERROR %v",url,err)
					continue
				}
			} else {
				data = []byte(ret.Result.Body)
			}
			log.Println("Save file ",tmpDir + "/" + resource.FileName)
			err = ioutil.WriteFile(tmpDir + "/" + resource.FileName,data,0644)
			if err != nil {
				log.Printf("Resource %s ERROR %v",url,err)
				continue
			}

		} else {
			log.Printf("Resource %s ERROR NOTFOUND",url)
		}
	}

	log.Println("--------------------" )
*/
	/*
	msg,err := waitAnswer(ws,incomingMessages,fatalError)

	if err != nil {
		return
	}

	log.Printf("MEssage answer %#v",msg)
	*/

	return
}

func launchChrome(chromeDir string,extensionsDir string,stdoutClosed chan int)(chromeCmd *exec.Cmd,wsUrl string,wsBgUrl string,err error) {
	var execArgs []string = chromeArgs[:len(chromeArgs)]

	err = domutil.PrepareChromeExtensions(extensionsDir)
	if err != nil {
		log.Println("prepareChromeExtensions: " + err.Error())
		return
	}




	execArgs = append(execArgs,"--remote-debugging-port="+strconv.Itoa(*debugPort))
	execArgs = append(execArgs,"--user-data-dir="+chromeDir)
	execArgs = append(execArgs,"--load-extension="+extensionsDir)
	execArgs = append(execArgs,"--ppapi-flash-path=" + "/home/belka2/MProg/Webkit/chromium/src/chrome/tools/test/reference_build/chrome_linux/PepperFlash/libpepflashplayer.so")
	//execArgs = append(execArgs,"--enable-kiosk-mode")

	log.Printf("==> Executing: %s %s\n", chromeBin, strings.Join(execArgs, " "))

	chromeCmd = exec.Command(chromeBin, execArgs...)

	outPipe, err := chromeCmd.StderrPipe()
	if err != nil {
		return nil,"","",err
	}

	if err := chromeCmd.Start(); err != nil {
		return nil,"","",err
	}

	appStarted := make(chan int)

	go outReader(outPipe,stdoutClosed,appStarted)

	select {
	case <-appStarted:
		log.Printf("Chrome successfully started")
	case <-stdoutClosed:
		return chromeCmd,"","",errors.New("Chrome closed stdout before writing to it")
	}

	for i:=0; i< 10;i++ {
		if wsUrl,wsBgUrl,err = getWebSocketUrl();err== nil {
			break
		}
		log.Printf("WsUrl not found.Sleep and try again try=%d.",i+1)
		time.Sleep(1000 * time.Millisecond)
	}

	if ( err != nil ) {
		return chromeCmd,"","",err
	}

	log.Printf("WebSocketURL:%s",wsUrl)

	return chromeCmd,wsUrl,wsBgUrl,err
}

func startApp() {
	tmpDir, err := ioutil.TempDir("/tmp/", "gothumbi")
	if err != nil {
		log.Panic(err)
	}

	chromeDir, err := ioutil.TempDir("/tmp/", "gothumbiChrome")
	if err != nil {
		log.Panic(err)
	}

	extensionsDir, err := ioutil.TempDir("/tmp/", "gothumbiChromeExt")
	if err != nil {
		log.Panic(err)
	}

	cleanup := func() {
		log.Printf("Remove temp chrome dir %s",chromeDir)
		err := os.RemoveAll(chromeDir)
		if err != nil {
			log.Println("Cleanup error:"+err.Error())
		}
		err = os.RemoveAll(extensionsDir)
		if err != nil {
			log.Println("Cleanup error:"+err.Error())
		}
	}
	//defer cleanup()

	stdoutClosed := make(chan int)

	processError  := make(chan error)
	processResult := make(chan int)

	cmdProcess,wsUrl,wsBgUrl,err := launchChrome(chromeDir,extensionsDir,stdoutClosed)

	if (err != nil ) {
		if ( cmdProcess != nil ) {
			log.Println("Try to kill chrome process")
			cmdProcess.Process.Kill()
		}
		log.Fatal("Cant launch chrome " + err.Error())
	}

	ws, err := domutil.WebSocketConnect(wsUrl)

	if (err != nil ) {
		if ( cmdProcess != nil ) {
			log.Println("Try to kill chrome process")
			cmdProcess.Process.Kill()
		}
		log.Fatal("Cant create websocket" + err.Error())
	}

	wsBg, err := domutil.WebSocketConnect(wsBgUrl)

	if (err != nil ) {
		if ( cmdProcess != nil ) {
			log.Println("Try to kill chrome process")
			cmdProcess.Process.Kill()
		}
		log.Fatal("Cant create BG websocket" + err.Error())
	}

	go ws.WsReader()
	go wsBg.WsReader()

	processPage := func () {
		err:=processWebsocket(ws,wsBg,tmpDir)


		if (err != nil ) {
			processError <- err
		} else {
			processResult <- 1
		}
	}

	go processPage()

	var isError = false
	select {
	case <-stdoutClosed:
		log.Println("Error: stdoutClosed")
		isError = true
	case err = <- processError:
		log.Printf("Error: %v",err)
		isError = true
	case err = <- ws.FatalError:
		log.Printf("FatalError: %v",err)
		isError = true
	case <- processResult:
		log.Println("Successfully Finished. tmpDir:" + tmpDir)
	}


	if ( cmdProcess != nil) {
		time.Sleep(1000 * time.Second)
		cmdProcess.Process.Kill()
	}

	cleanup()

	if isError {
		log.Fatal("Finished with error")
	}
}

func main() {
	flag.Parse()

	startApp()

}

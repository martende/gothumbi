package main

import (
	"io/ioutil"
	"log"
)

const TestJs =`function(a) {
console.log("AAAA:"+a);
return {"X":a};
}`

func readOrPanic(fname string) string {
	data,err := ioutil.ReadFile(fname)
	if err != nil {
		log.Panic(err)
	}
	return string(data)
}

var ClientDumpJs = readOrPanic("js/tweets2.js")

var BackgoundJs = readOrPanic("js/background.js")
var ManifestJson = readOrPanic("js/manifest.json")
var BackgroundCSS = readOrPanic("js/background.css")

package socks5server


import (
	"fmt"
	"io"
	"log"
	"net"
//	"strings"
	"time"
//	"httpserver"
)

const (
	connectCommand   = uint8(1)
	bindCommand      = uint8(2)
	associateCommand = uint8(3)
	ipv4Address      = uint8(1)
	fqdnAddress      = uint8(3)
	ipv6Address      = uint8(4)
)

const (
	successReply uint8 = iota
	serverFailure
	ruleFailure
	networkUnreachable
	hostUnreachable
	connectionRefused
	ttlExpired
	commandNotSupported
	addrTypeNotSupported
)

var (
	unrecognizedAddrType = fmt.Errorf("Unrecognized address type")
)

// AddressRewriter is used to rewrite a destination transparently
type AddressRewriter interface {
	Rewrite(addr *AddrSpec) *AddrSpec
}

// AddrSpec is used to return the target AddrSpec
// which may be specified as IPv4, IPv6, or a FQDN
type AddrSpec struct {
	FQDN string
	IP   net.IP
	Port int
}
/*
type conn interface {
	Write([]byte) (int, error)
	RemoteAddr() net.Addr
}
*/
func (a *AddrSpec) String() string {
	if a.FQDN != "" {
		return fmt.Sprintf("%s (%s):%d", a.FQDN, a.IP, a.Port)
	}
	return fmt.Sprintf("%s:%d", a.IP, a.Port)
}

// handleRequest is used for request processing after authentication
func (s *Server) handleRequest(conn net.Conn, bufConn io.Reader) error {
	// Read the version byte
	header := []byte{0, 0, 0}
	if _, err := io.ReadAtLeast(bufConn, header, 3); err != nil {
		return fmt.Errorf("Failed to get command version: %v", err)
	}

	// Ensure we are compatible
	if header[0] != socks5Version {
		return fmt.Errorf("Unsupported command version: %v", header[0])
	}

	// Read in the destination address
	dest, err := readAddrSpec(bufConn)
	if err != nil {
		if err == unrecognizedAddrType {
			if err := sendReply(conn, addrTypeNotSupported, nil); err != nil {
				return fmt.Errorf("Failed to send reply: %v", err)
			}
		}
		return fmt.Errorf("Failed to read destination address: %v", err)
	}

	// Resolve the address if we have a FQDN
	if dest.FQDN != "" {
		addr, err := s.config.Resolver.Resolve(dest.FQDN)
		if err != nil {
			if err := sendReply(conn, hostUnreachable, nil); err != nil {
				return fmt.Errorf("Failed to send reply: %v", err)
			}
			return fmt.Errorf("Failed to resolve destination '%v': %v", dest.FQDN, err)
		}
		dest.IP = addr
	}

	// Apply any address rewrites
	realDest := dest
	if s.config.Rewriter != nil {
		realDest = s.config.Rewriter.Rewrite(dest)
	}

	// Switch on the command
	switch header[1] {
	case connectCommand:
		return s.handleConnect(conn, bufConn, dest, realDest)
	case bindCommand:
		return s.handleBind(conn, bufConn, dest, realDest)
	case associateCommand:
		return s.handleAssociate(conn, bufConn, dest, realDest)
	default:
		if err := sendReply(conn, commandNotSupported, nil); err != nil {
			return fmt.Errorf("Failed to send reply: %v", err)
		}
		return fmt.Errorf("Unsupported command: %v", header[1])
	}
}

type wrappedConnection struct {
	net.Conn
	prefix []byte
	prefixLen uint64
	readedN uint64
}

func wrapConnection(conn net.Conn,prefix []byte) *wrappedConnection {
	self := &wrappedConnection{conn,prefix,uint64(len(prefix)),0}
	return self
}

func (self *wrappedConnection) Read(b []byte) (n int, err error) {
	/*if self.readedN == 0 {
		n = 1
		b[0] = byte('G')
	} else */if self.readedN < self.prefixLen  {
		var b2read uint64 = uint64(len(b))
		if b2read + self.readedN > self.prefixLen {
			b2read = self.prefixLen - self.readedN
		}

		//log.Printf("COPY %d %d",self.readedN,self.readedN+b2read)
		//log.Printf("PREFIX (%s)",string(self.prefix))

		copy(b,self.prefix[self.readedN:self.readedN+b2read])

		//log.Printf("B (%s)",string(b))

		if ( b2read <  uint64(len(b)) ) {
			n,err = self.Conn.Read(b[b2read:])
			n += int(b2read)
		} else {
			n = int(b2read)
		}
	} else {
		n,err = self.Conn.Read(b)
	}

	self.readedN += uint64(n)

	//log.Println("READED ",n,string(b))

	return
}

// handleConnect is used to handle a connect command
func (s *Server) handleConnect(conn net.Conn, bufConn io.Reader, dest, realDest *AddrSpec) error {
	destAddr := net.TCPAddr{IP: realDest.IP, Port: realDest.Port}
	/*target, err := net.DialTCP("tcp", nil, &addr)
	if err != nil {
		msg := err.Error()
		resp := hostUnreachable
		if strings.Contains(msg, "refused") {
			resp = connectionRefused
		} else if strings.Contains(msg, "network is unreachable") {
			resp = networkUnreachable
		}
		if err := sendReply(conn, resp, nil); err != nil {
			return fmt.Errorf("Failed to send reply: %v", err)
		}
		return fmt.Errorf("Connect to %v failed: %v", dest, err)
	}
	defer target.Close()
*/

	local :=conn.LocalAddr().(*net.TCPAddr)
	// Send success
	//local := target.LocalAddr().(*net.TCPAddr)
	bind := AddrSpec{IP: local.IP, Port: local.Port + 1 }
	if err := sendReply(conn, successReply, &bind); err != nil {
		return fmt.Errorf("Failed to send reply: %v", err)
	}


	// Start proxying
	//errCh := make(chan error, 2)

	// Synchrone call !!!
	var sign []byte = make([]byte,3,3)
	n,err := conn.Read(sign)
	if err != nil  {
		log.Println("SOCKS5 remote connection closed")
		conn.Close()
		return nil
	}
	if n != 3  {
		log.Println("ERROR BAD SIGN LENGTH ",n)
		conn.Close()
		return nil
	}

	//log.Printf("Frist Request byte %c(%02x)",sign[0],sign[0])

	conn2 := wrapConnection(conn,sign)

	// OPTIONS,GET,HEAD,POST,PUT,DELETE,TRACE,CONNECT

	if (sign[0] == 'O' && sign[1] == 'P') || (sign[0] == 'G' && sign[1] == 'E') || (sign[0] == 'H' && sign[1] == 'E') || (sign[0] == 'D' && sign[1] == 'E') ||
			(sign[0] == 'T' && sign[1] == 'R') || (sign[0] == 'C' && sign[1] == 'O') || (sign[0] == 'P' && (sign[1] == 'U' || sign[1] == 'O') ) {
		s.VirtualWebServer.ServeConnection(conn2,destAddr)
	} else {
		s.VirtualWebServer.ServeSSLConnection(conn2,destAddr)
	}
	//|| sign[0] == 'P' || sign[0] == 'O' || sign[]
		//s.VirtualWebServer.ServeConnection(conn2,destAddr)




	/*	go proxy("target", target, bufConn, errCh)
		go proxy("client", conn, target, errCh)

	// Wait
	select {
	case e := <-errCh:
		return e
	}
	*/
	return nil
}

// handleBind is used to handle a connect command
func (s *Server) handleBind(conn net.Conn, bufConn io.Reader, dest, realDest *AddrSpec) error {

	// TODO: Support bind
	if err := sendReply(conn, commandNotSupported, nil); err != nil {
		return fmt.Errorf("Failed to send reply: %v", err)
	}
	return nil
}

// handleAssociate is used to handle a connect command
func (s *Server) handleAssociate(conn net.Conn, bufConn io.Reader, dest, realDest *AddrSpec) error {


	// TODO: Support associate
	if err := sendReply(conn, commandNotSupported, nil); err != nil {
		return fmt.Errorf("Failed to send reply: %v", err)
	}
	return nil
}

// readAddrSpec is used to read AddrSpec.
// Expects an address type byte, follwed by the address and port
func readAddrSpec(r io.Reader) (*AddrSpec, error) {
	d := &AddrSpec{}

	// Get the address type
	addrType := []byte{0}
	if _, err := r.Read(addrType); err != nil {
		return nil, err
	}

	// Handle on a per type basis
	switch addrType[0] {
	case ipv4Address:
		addr := make([]byte, 4)
		if _, err := io.ReadAtLeast(r, addr, len(addr)); err != nil {
			return nil, err
		}
		d.IP = net.IP(addr)

	case ipv6Address:
		addr := make([]byte, 16)
		if _, err := io.ReadAtLeast(r, addr, len(addr)); err != nil {
			return nil, err
		}
		d.IP = net.IP(addr)

	case fqdnAddress:
		if _, err := r.Read(addrType); err != nil {
			return nil, err
		}
		addrLen := int(addrType[0])
		fqdn := make([]byte, addrLen)
		if _, err := io.ReadAtLeast(r, fqdn, addrLen); err != nil {
			return nil, err
		}
		d.FQDN = string(fqdn)

	default:
		return nil, unrecognizedAddrType
	}

	// Read the port
	port := []byte{0, 0}
	if _, err := io.ReadAtLeast(r, port, 2); err != nil {
		return nil, err
	}
	d.Port = (int(port[0]) << 8) | int(port[1])

	return d, nil
}

// sendReply is used to send a reply message
func sendReply(w io.Writer, resp uint8, addr *AddrSpec) error {
	// Format the address
	var addrType uint8
	var addrBody []byte
	var addrPort uint16
	switch {
	case addr == nil:
		addrType = ipv4Address
		addrBody = []byte{0, 0, 0, 0}
		addrPort = 0

	case addr.FQDN != "":
		addrType = fqdnAddress
		addrBody = append([]byte{byte(len(addr.FQDN))}, addr.FQDN...)
		addrPort = uint16(addr.Port)

	case addr.IP.To4() != nil:
		addrType = ipv4Address
		addrBody = []byte(addr.IP.To4())
		addrPort = uint16(addr.Port)

	case addr.IP.To16() != nil:
		addrType = ipv6Address
		addrBody = []byte(addr.IP.To16())
		addrPort = uint16(addr.Port)

	default:
		return fmt.Errorf("Failed to format address: %v", addr)
	}

	// Format the message
	msg := make([]byte, 6+len(addrBody))
	msg[0] = socks5Version
	msg[1] = resp
	msg[2] = 0 // Reserved
	msg[3] = addrType
	copy(msg[4:], addrBody)
	msg[4+len(addrBody)] = byte(addrPort >> 8)
	msg[4+len(addrBody)+1] = byte(addrPort & 0xff)

	// Send the message
	_, err := w.Write(msg)
	return err
}

// proxy is used to suffle data from src to destination, and sends errors
// down a dedicated channel
func proxy(name string, dst io.Writer, src io.Reader, errCh chan error) {
	// Copy
	n, err := io.Copy(dst, src)

	// Log, and sleep. This is jank but allows the otherside
	// to finish a pending copy
	log.Printf("[DEBUG] socks: Copied %d bytes to %s", n, name)
	time.Sleep(10 * time.Millisecond)

	// Send any errors
	errCh <- err
}

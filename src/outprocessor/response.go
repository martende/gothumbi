package outprocessor

import (
"httpserver"
"log"
"io"
"fmt"
"net"
"time"
"crypto/tls"
//"socks"
	"bytes"
	"errors"
)

const http_error_500f = "HTTP/1.1 500 Error\r\n\r\n<html><head><title>Internal Server Error</title></head><body><h1>%s</h1></body></html>"

type ResponseProcessor struct {}

type BufferReaderCloser struct {
	*bytes.Buffer
}

func wrap2ReaderCloser(buf *bytes.Buffer) BufferReaderCloser  {
	return BufferReaderCloser{Buffer:buf}
}

func (self BufferReaderCloser) Close() error {
	return nil
}

var socksList = newSocksList()

func cloneHeader(h httpserver.Header) httpserver.Header {
	h2 := make(httpserver.Header, len(h))
	for k, vv := range h {
		vv2 := make([]string, len(vv))
		copy(vv2, vv)
		h2[k] = vv2
	}
	return h2
}

func (self *ResponseProcessor) ServeWebSocket(conn net.Conn, r *httpserver.Request) error {
	var h =  cloneHeader(r.Header)
	var url = r.URL
	if  r.IsSSL {
		url.Scheme = "wss"
	}  else {
		url.Scheme = "ws"
	}

	var req = &httpserver.Request{
		Method:     r.Method,
		URL:        url,
		Proto:      r.Proto,//"HTTP/1.1",
		ProtoMajor: r.ProtoMajor,//1,
		ProtoMinor: r.ProtoMinor,//1,
		Header:     h,
		Body:       r.Body,
		Host:       r.Host,
	}

	var dbgUrl = fmt.Sprintf("%s %s://%s%s",r.Method,url.Scheme,url.Host,url.Path)

	log.Printf("ResponseProcessor Serve: %s ",dbgUrl)

	target, err := net.DialTCP("tcp", nil, &r.DestAddr)
	if err != nil {
		if _,err := conn.Write([]byte(fmt.Sprintf(http_error_500f,err.Error()))); err != nil {
			log.Println("HTTP500 page Write error",err)
			return err
		}
		return err
	}
	defer target.Close()

	if err := req.Write(target); err != nil {
			if _,err := conn.Write([]byte(fmt.Sprintf(http_error_500f,err.Error()))); err != nil {
			log.Println("HTTP500 page Write error",err)
			return err
		}
		return err
	}

	errCh := make(chan error,2)

	go proxyWS("target",target,conn,errCh)
	go proxyWS("client",conn,target,errCh)

	select {
		case e := <-errCh:
		return e
	}
	return nil
}

func proxyWS(name string, dst io.Writer, src io.Reader, errCh chan error) {
	// Copy
	n, err := io.Copy(dst, src)

	// Log, and sleep. This is jank but allows the otherside
	// to finish a pending copy
	log.Printf("[DEBUG] socks: Copied %d bytes to %s", n, name)
	time.Sleep(10 * time.Millisecond)

	// Send any errors
	errCh <- err
}


func (self *ResponseProcessor) ServeHTTP(w httpserver.ResponseWriter, r *httpserver.Request) {

	switch r.Host {
		case "safebrowsing-cache.google.com", "translate.googleapis.com", "ssl.gstatic.com":
			//w.Write([]byte(fmt.Sprintf("SOSITE HUY %d\n",i)))
			httpserver.Error(w, "Xui", 500)
		default:
			if r.URL.Path=="/favicon.ico" {
				httpserver.Error(w, "Xui", 500)
			} else {
				var url = r.URL

				if r.IsSSL {
					url.Scheme = "https"
				}  else {
					url.Scheme = "http"
				}
				url.Host  = r.Host

				var dbgUrl = fmt.Sprintf("%s %s://%s%s",r.Method,url.Scheme,url.Host,url.Path)
				log.Printf("ResponseProcessor Serve: %s ",dbgUrl)

				var h =  cloneHeader(r.Header)
				//h.Set("User-Agent","AAA")

				var req = &httpserver.Request{
					Method:     r.Method,
					URL:        url,
					Proto:      r.Proto,//"HTTP/1.1",
					ProtoMajor: r.ProtoMajor,//1,
					ProtoMinor: r.ProtoMinor,//1,
					Header:     h,
					Body:       r.Body,
					Host:       r.Host,
				}

				if err :=serveRequest(req,w,&dbgUrl,3); err!=nil {
					log.Printf("Request '%s' error %s",dbgUrl,err)
					httpserver.Error(w, err.Error(), httpserver.StatusInternalServerError)
				}
			}
	}
}


func serveWithSocks(req *httpserver.Request,dbgUrl *string,socksHost string) (*httpserver.Response,error){
	var transport = &httpserver.Transport{
		//Proxy: nil,
		//Dial: socks.DialSocksProxy(socks.SOCKS5,socksHost,3*time.Second),
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		TLSHandshakeTimeout: 10 * time.Second,
		ResponseHeaderTimeout: 10 * time.Second,
	}

	client := &httpserver.Client{
		Transport: transport,
		Timeout: 60 * time.Second,
	}



	resp,err := client.Do(req)

	if err != nil {
		log.Printf("Request [SOCKS:%s]%s : ConnectionError %s",socksHost,*dbgUrl,err)
		socksList.MarkBad(socksHost)
		return nil,err
	}

	log.Printf("Request [SOCKS:%s]%s : Header received STATUS:%d ContentLength:%d\n%#v",socksHost,*dbgUrl,resp.StatusCode, resp.ContentLength, resp)

	var buf *bytes.Buffer = new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	log.Printf("Request [SOCKS:%s]%s : Body received Length:%d",socksHost,*dbgUrl,buf.Len())

	// check quality of response
	if resp.ContentLength > 0 {
		if resp.ContentLength != int64(buf.Len()) {
			log.Printf("Request [SOCKS:%s]%s : resp.ContentLength(%d) != readedData(%d)",socksHost,*dbgUrl,resp.ContentLength,buf.Len() )
			return nil,errors.New("Bad data Length")
		}
	} else {
//  cssTest does not work well yet
//	if resp.Header.Get("Content-type") == "text/css" {
//		var cssOk = cssTest(buf)
//		if ! cssOk {
//			log.Printf("Request [SOCKS:%s]%s : Bad css content",socksHost,*dbgUrl)
//			return nil,errors.New("Bad css content")
//		}
//	}
	}

	resp.Body = wrap2ReaderCloser(buf)

	return resp,nil
}

func cssTest(cssBuf *bytes.Buffer) bool {
	bl := cssBuf.Len()
	bb := cssBuf.Bytes()

	var k = 0

	for i:= 0; i < bl ; i++ {
		switch bb[i] {
			case '{': k++;
			case '}': k--;
		}
	}

	return k == 0
}

func serveRequest(req *httpserver.Request,w httpserver.ResponseWriter,dbgUrl *string,retriesCnt int) error {
	var err error
	var resp *httpserver.Response

	for retriesCnt>0 {
		var socksHost = socksList.GetWorkingProxy()

		resp ,err= serveWithSocks(req,dbgUrl,socksHost)
		if err == nil {
			break
		}
		retriesCnt-=1
	}

	if err != nil {
		return err
	}

	for h:= range resp.Header {
		w.Header().Set(h,resp.Header.Get(h))
	}
	w.WriteHeader(resp.StatusCode)

	if _,err := io.Copy(w, resp.Body); err != nil {
		return err
	}

	log.Printf("Request %s : Body Complete",*dbgUrl)

	return nil
}

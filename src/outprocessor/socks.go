package outprocessor

import (
	"time"
	"sync"
	"log"
)


type SocksRecord struct {
	addr string
	lastAccessTime time.Time
	bad bool
	lastBadTime time.Time
}

type SocksList struct {
	sync.Mutex
	socks []SocksRecord
	socksMap map[string] *SocksRecord
	idx int
}

func newSocksList() SocksList {
	var socks = []SocksRecord{
		SocksRecord{addr:"127.0.0.1:8001"}, // BAD
		SocksRecord{addr:"127.0.0.1:20805"},
		SocksRecord{addr:"127.0.0.1:20806"},
		SocksRecord{addr:"127.0.0.1:20807"},
		SocksRecord{addr:"127.0.0.1:20808"},
		SocksRecord{addr:"127.0.0.1:20809"},
		SocksRecord{addr:"127.0.0.1:20810"},
		SocksRecord{addr:"127.0.0.1:20811"},
		SocksRecord{addr:"127.0.0.1:20812"},
		SocksRecord{addr:"127.0.0.1:20813"},
		SocksRecord{addr:"127.0.0.1:20814"},
		SocksRecord{addr:"127.0.0.1:20815"},
	}
	var socksMap = make(map[string]*SocksRecord)
	for i := range socks {
		socksMap[socks[i].addr] = &socks[i]
	}
	var sl = SocksList{socks:socks,socksMap:socksMap}
	return sl
}


func (self *SocksList) GetWorkingProxy() string {
	//return self.socks[0].addr
	self.Lock()

	ls := len(self.socks)

	var sr *SocksRecord

	i:= self.idx

	for {
		if ! self.socks[i].bad {
			sr = &self.socks[i]
			break
		}
		i+=1
		if i >= ls {
			i = 0
		}
		if i == self.idx {
			break
		}
	}

	if sr == nil {
		log.Println("WARN: ALL SOCKS are marked as bad - reset bad status. and choose first")
		for i := range self.socks {
			self.socks[i].bad = false
		}
		self.idx = 0
		sr = &self.socks[self.idx]
	}

	self.idx++

	if self.idx >= ls {
		self.idx = 0
	}

	sr.lastAccessTime = time.Now()

	if self.idx >= len(self.socks) {
		self.idx = 0
	}

	self.Unlock()
	return sr.addr

}

func (self *SocksList) MarkBad(socksAddr string) {
	self.Lock()
	self.socksMap[socksAddr].bad = true
	self.socksMap[socksAddr].lastBadTime = time.Now()
	self.Unlock()
}

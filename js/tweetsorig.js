// todo:
// preserve original image filename (or url) (or at least put in into title)
// fix outgoing links
// fix links within the page (with #anchors)
// test download url
// test <frame src=image.exe
// test <frame src=image.gif
// handle 401 auth dialog?
// save favicon
// multi background-image:
// limit download file size
// p:first-line         http://www.w3schools.com/css/tryit.asp?filename=trycss_firstline
// repack png files (png made from canvas.toDataURL() can be huge yet well packable)
// css :visited, :focus
// fix <a href="javascript: (tc:http://archive.is/DIyi http://www.ghanaweb.com/GhanaHomePage/people/pop-up.php?ID=125)
// bug: background-image: url(...), initial; http://archive.is/ouBBc
// todo: scroll scribd.com like twitter/facebook. tc:http://archive.is/lnwgi
// todo:  !!! preserve id of element witch matches hashtag of requested url
// todo: mht support http://www.gwern.net/docs/sr/2013-11-29-mdpro-moonbear.mht

// todo: disable js when rearchiving from web.archive.org
//   tc: https://archive.today/http://web.archive.org/web/20130826063914/http://www.ft.com/cms/s/0/ab28f708-0a7e-11e3-aeab-00144feabdc0.html%23axzz3CIeY7Fpp
//   tc: https://archive.today/http://www.miamiherald.com/static/insite/login.html?goto=*

var page = require('webpage').create(),
    fs = require('fs'),
    system = require('system');


var urltoload = system.args[1];
if (!urltoload.match(/^\w+:\/\//))
  urltoload = atob(urltoload); // decode base64
var cookietoload = system.args[2]; assert(cookietoload.match(/^cookie=/), "cookietoload.match(/^cookie=/)"); cookietoload=atob(cookietoload.substring(7));
var requesterIP = system.args[3] || "unknown";
var requesterUserAgent = atob(system.args[4]) || "unknown";
var tipchangeset = system.args[5] || 'unknown';
var timeout = parseInt(system.args[6]) || 2000; // delay after load completed
var shortid = parseInt(system.args[7]) || 0;
var readyurl = system.args[8] || phantom.exit(19); // unused actually
var tryNumber = timeout % 1000;
if (!(1<=tryNumber && tryNumber<=10)) tryNumber = 0; // exepcted 1 for 1st try, then 2, then 3

var cookieFileName = null;
function useCookieFile(name) {
  console.log(JSON.stringify({what:"LOG", msg:"--------------------------- entering facebookmode"}))

  // read cookies from file
  if (cookietoload=="") { // if user the supplied cookie, do not use the file
    cookieFileName = name + "-cookies.txt";
    try {
      var f = fs.open(cookieFileName, "rb");
      phantom.cookies = JSON.parse(f.read());
      f.close();
    } catch (e) {
      console.log(JSON.stringify({what:"LOG", msg:"error reading " + cookieFileName + " ="+e}))
    }
  }
}

function setFacebookMode() {
  useCookieFile("facebook");
  page.settings.userAgent = "hggh Screener (http://screener.brachium-system.net/)";
}

function isBlogspotUrl     (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*(blogspot|blogger)\.(com?\.)?[a-z]+(\/|$)/ ); }

function isWSJournalUrl    (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*wsj\.com(\/|$)/ ); }

function isGithubUrl       (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*github\.com(\/|$)/ ); }

function isInstagramUrl    (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*(instagram\.com|instagr\.am)(\/|$)/ ); } // *.instagram.com *.instagr.am
function isFacebookUrl     (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*(facebook\.com|fb\.com|fb\.me)(\/|$)/ ); } // *.facebook.com *.fb.com *.fb.me

function isRedditUrl       (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*(reddit\.com|redd\.it)(\/|$)/ ); } // *.reddit.com *.redd.it
function isRedditOver18    (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*reddit\.com\/over18\?/ ); }

function isTwitterUrl      (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*twitter\.com(\/|$)/ ); }

function isSoupIo          (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*soup\.io(\/|$)/ ); }
function isWeiboUrl        (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*weibo\.com(\/|$)/ ); }

// function isScribdUrl       (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*scribd\.com(\/|$)/ ); }

function isQuoraUrl        (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*(quora\.com|qr\.ae)(\/|$)/ ); }

function isSoundcloudUrl   (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*soundcloud\.com(\/|$)/ ); }
function isImgurUrl        (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*imgur\.com(\/|$)/      ); }
function isSexComUrl       (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*sex\.com(\/|$)/        ); }
function isTumblrUrl       (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*tumblr\.com(\/|$)/        ); }
function isWikimapiaUrl    (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*wikimapia\.org(\/|$)/  ); }

function isAdflyUrl        (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*adf\.ly(\/|$)/ ); }

function isMedscapeUrl     (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*medscape\.com(\/|$)/ ); }
function isMedscapeLoginUrl(url) { return null != url.match( /^https?:\/\/login\.medscape\.com\/login\/sso\/getlogin/ ); }

function isDreamwidthUrl     (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*dreamwidth\.org(\/|$)/ ); }
function isLivejournalUrl     (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*livejournal\.com(\/|$)/ ); }
function isLivejournalLoginUrl(url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*livejournal\.com\/\?returnto=/ ); }

function isTMGOnlineMediaUrl(url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*tmgonlinemedia\.nl(\/|$)/ ); }

function isVkUrl           (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*(vk\.com|vkontakte\.ru)(\/|$)/ ); }

function isLinkedinUrl      (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*(linkedin\.com|linkd\.in)(\/|$)/ ); }
function isLinkedinLoginUrl (url) { return null != url.match( /^https?:\/\/www\.linkedin\.com\/uas\/login/ ); }
function isLinkedinSignupUrl(url) { return null != url.match( /^https?:\/\/www\.linkedin\.com\/reg\/join-pprofile/ ) || url == "https://www.linkedin.com/reg/join?trk=login_reg_redirect"; }

function isMegalodonJp     (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*megalodon\.jp(\/|$)/ ); }
function is9gagCom         (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*9gag\.com(\/|$)/ ); }
function isEigomangaCom    (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*eigomanga\.com(\/|$)/ ); }
function isArstechnicaCom  (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*arstechnica\.com(\/|$)/ ); }

function isBannedUrl      (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*(dosrcnsbncias\.com|37tube\.com|boneyadvd\.com)(\/|$)/ ); }
function isYandexCache    (url) { return null != url.match( /^https?:\/\/([a-z0-9-]+\.)*hghltd\.yandex\.net\/yandbtm/ ); }


if (isYandexCache(urltoload)) {
  // todo: query http://yandex.ru/yandsearch?text=$url&rpt=rad and find new cache url
  // var page = require('webpage').create();
  // href="http://hghltd.yandex.net/yandbtm?fmode=inject&amp;url=http%3A%2F%2Fwww.imit.ru%2Fnews%3Fid%3D729&amp;tld=ru&amp;lang=ru&amp;la=1405009408&amp;text=http%3A%2F%2Fwww.imit.ru%2Fnews%3Fid%3D729&amp;l10n=ru&amp;mime=html&amp;sign=3299173b92ff537a31ecaa3d0239f94b&amp;keyno=0">Ð¡Ð¾Ñ…Ñ€Ð°Ð½Ñ‘Ð½Ð½Ð°Ñ ÐºÐ¾Ð¿Ð¸Ñ</a><
}


page.settings.webSecurityEnabled = false;
page.settings.loadImages = true;
var customHeaders = {
        "Accept-Language" : "en,de;q=0.8,ja;q=0.5",  // todo: user's Accept-Language?
        "Referer"         : (tryNumber==2 || tryNumber==3 || is9gagCom(urltoload) || isEigomangaCom(urltoload) || isArstechnicaCom(urltoload) ? "" : (isMegalodonJp(urltoload) ? urltoload : "http://www.google.com/")),
        "X-Forwarded-For" : (requesterIP!="unknown" ? requesterIP /* some sites customize page for the user (market.yandex.ru), some sites put ban on this IP (instagram.com), todo: randomize last digit */
                                                   : "193.4.58.17"), /* iceland by default */
};
page.customHeaders = customHeaders;
if (!isWSJournalUrl(urltoload)) { // reuse referer after 301 redirect
  delete customHeaders["Referer"];
}

if (isBannedUrl(urltoload)) {
  phantom.exit(18);
}

if (isFacebookUrl(urltoload)  ||
    isVkUrl(urltoload)        ||
    isQuoraUrl(urltoload)     ||
    isImgurUrl(urltoload)     ||
    isSexComUrl(urltoload)    ||
    isTumblrUrl(urltoload)    ||
    isTwitterUrl(urltoload) /* does not work with twitter */) {
  page.viewportSize = { width: 1024, height: 5000 };
} else {
  page.viewportSize = { width: 1024, height: 768 };
}
page.clipRect = { top: 0, left: 0, width: 1024, height: 768 };

page.settings.userAgent = "Mozilla/5.0 (compatible; Windows NT 5.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/19.0.1084.36 Safari/535.19"
//page.settings.userAgent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.11";


if (isFacebookUrl(urltoload)) {
  setFacebookMode();
} else if (isInstagramUrl(urltoload) || isGithubUrl(urltoload) || isArstechnicaCom(urltoload)) {
  page.customHeaders = customHeaders = { };
  page.settings.userAgent = "Googlebot/2.1 (+http://www.googlebot.com/bot.html)"; // ask for simplest layout possible (no ajax, etc)
  page.settings.userAgent = "wget/1.114";
} else if (isTwitterUrl(urltoload)) {
  page.settings.userAgent = "wget/1.114";
} else if (isSoundcloudUrl(urltoload)) {
  page.settings.userAgent = "Mozilla/5.0 (compatible; Windows NT 6.1; WOW64) AppleWebKit/535.29 (KHTML, like Gecko) Chrome/29.0.1084.36 Safari/535.29"
} else if (isQuoraUrl(urltoload)) {
  useCookieFile("quora");
} else if (isMedscapeUrl(urltoload)) {
  useCookieFile("medscape");
} else if (isLivejournalUrl(urltoload)) {
  useCookieFile("livejournal");
} else if (isVkUrl(urltoload)) {
  useCookieFile("vk");
} else if (isWeiboUrl(urltoload)) {
  cookietoload="cookie=YF-Page-G0=b5853766541bcc934acef7f6116c26d1; YF-Ugrow-G0=ad83bc19c1269e709f753b172bddb094; YF-V5-G0=55fccf7be1706b6814a78384fa94e30c; UOR=notebook.yesky.com,widget.weibo.com,notebook.yesky.com; SUB=_2AkMjXDwCf8NhqwJRmPkXzW_la4RwyQHEiebDAHzsJxJTHmM37JIVddYEdxzsQXI_SpW9k3zobCfB; SUBP=0033WrSXqPxfM72-Ws9jqgMF55529P9D9WhdmINMoR.ucn9fhZVfyd6.; _s_tentry=passport.weibo.com; Apache=3752413049029.7944.1409332101052; SINAGLOBAL=3752413049029.7944.1409332101052; ULV=1409332101057:1:1:1:3752413049029.7944.1409332101052:";
} else if (isLinkedinUrl(urltoload)) {
  // always login
} else if (urltoload.indexOf(".consultant.ru/") >=0 || urltoload.indexOf(".crainsnewyork.com/") >=0) {
  page.settings.userAgent = "Mozilla/5.0 (compatible; Googlebot/2.1)"; // not full page but at least something
}


if(cookietoload!="") {
  var domain2 = urltoload.replace( /^https?:\/\/(?:[a-z0-9-]+\.)*([a-z0-9-]+\.[a-z]+)\/.*/ , function(full, d2) { return d2; });
  //console.log("<!-- urltoload="+urltoload+" -->");
  //console.log("<!-- domain2  ="+domain2+" -->");
  //console.log("<!-- cookietoload="+cookietoload+" -->");
  var a=cookietoload.split("; ");
  var m=[];
  for(var i=0; i<a.length; i++) {
    var kv=a[i];
    var pos=kv.indexOf("=");
    assert(pos>0, "pos>0");
    //console.log("<!-- k=`"+kv.substring(0,pos)+"' v=`"+kv.substring(pos+1)+"' -->");
    m.push({
      "domain": "."+domain2,
      "expires": "Thu, 26 Sep 2020 03:51:15 GMT", // new Date(new Date().getTime()+1000)
      "expiry": 1380163875,
      "httponly": false,
      "name": kv.substring(0,pos),
      "path": "/",
      "secure": false,
      "value": kv.substring(pos+1)
     });
  }
  phantom.cookies = m;
}



function milliseconds_current() { return (new Date()).getTime(); }
var timeStart = milliseconds_current();
var timeLastActivity = timeStart;
function milliseconds_since_start() { return milliseconds_current() - timeStart; }
function milliseconds_since_last_activity() { return milliseconds_current() - timeLastActivity; }

var internedurls = {}, internedurls_len = 0; // todo: unite with resources?
function intern(u) {
  return (u in internedurls) ? -1 : (internedurls[u] = ++internedurls_len);
}

//setTimeout(function(){ console.log("..."); }, 3000);
var urlsinprogress = {};
function outreq(method, url, stage) {
  var id = intern(url);
  timeLastActivity = milliseconds_current();
  if(id>0) {
    urlsinprogress[url] = true;
    console.log(JSON.stringify({what:"BEGIN", id:id, stage:stage, method:method, url:url}));
  }
}


function outres(url, status, ctype, textsize, stage) {
  timeLastActivity = milliseconds_current();
  if (url in internedurls) {
    var id = internedurls[url];
    delete urlsinprogress[url];
    if(status==null) status = 0;
    console.log(JSON.stringify({what:"END", id:id, stage:stage, status:status, ctype:ctype, textsize:textsize}));
  }
}



var firstRequestedUrl = "";
var firstSuccessfulResponse = null;
page.onResourceRequested = function (req) {
//  console.log(JSON.stringify(req));
  if (!firstRequestedUrl) {
    page.customHeaders = customHeaders // no more referer
    firstRequestedUrl = req.url;
  }
  if(req.url.indexOf("data:")!=0) {
    outreq(req.method, req.url, 1)
  }
};

function encode_utf8(s) { return unescape(encodeURIComponent(s)); }

function decode_utf8(s) { return decodeURIComponent(escape(s)); }

function esc2(url) { // url as it is in [img=src] -> url in onResourceRequested/onResourceReceived
  return url.replace(/^(\w+:\/\/[^/]+)(.*)$/, function (url, schemedomain, uri) {
    return schemedomain + uri
      .replace(/%(?![0-9A-F][0-9A-F])/ig, '%25')
      .replace(/[ \[\]]/g, function(c) {
        return '%' + c.charCodeAt(0).toString(16).toUpperCase();
      });
  });
}

var resources ={}
page.onResourceReceived = function (res) {
  //console.log(JSON.stringify(res));
  if(res.stage == "start" && res.url.indexOf("data:")!=0) {
    if (res.status != 301 && res.status != 302) {
      if(!firstSuccessfulResponse || res.id < firstSuccessfulResponse.id)
        firstSuccessfulResponse = res;
    }
  }
  if(res.stage == "end" && res.url.indexOf("data:")!=0) {
    var ctype = (res.contentType||'').split(';')[0]
    outres(res.url, res.status, ctype, res.text ? res.text.length : 0, 1);
    resources[res.url] = res;
    for(var i=0; i<res.headers.length; i++) {
      if(res.headers[i].name == "Location") {
        var to = absolutizeURI(res.url, res.headers[i].value);
        if(res.url != to)
          resources[res.url] = to;
      }
    }
  }
};


page.onConsoleMessage = page.onAlert = page.onConfirm = function(s) {
  return false;
}

function assert(cond) {
  if(!cond) {
    var a = 'Assertion failed:';
    for(var i=1; i<arguments.length; i++)
      a+=' ' + arguments[i]
    console.log(JSON.stringify({what:"ERROR", code:16, message:"Assertion failed", debug:a}));
    phantom.exit(16);
  }
}

function stripHighByte(s) {
  var a = '';
  for (var i=0; i<s.length; i++)
    a += String.fromCharCode(s.charCodeAt(i) & 255);
  return a;
}

function GET(url) {
  try {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false /*synchronous*/);
    xhr.overrideMimeType('text/plain; charset=x-user-defined');
    xhr.timeout = 3000;
    xhr.send(null);
    return xhr;
  } catch (e) {
    console.log(JSON.stringify({what:"LOG", msg:"exc="+e}))
    return null;
  }
}


/*
 * Encode a string as utf-8.
 * For efficiency, this assumes the input is valid utf-16.
 */
function str2rstr_utf8(input)
{
  var output = "";
  var i = -1;
  var x, y;

  while(++i < input.length)
  {
    /* Decode utf-16 surrogate pairs */
    x = input.charCodeAt(i);
    y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
    if(0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF)
    {
      x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
      i++;
    }

    /* Encode output as utf-8 */
    if(x <= 0x7F)
      output += String.fromCharCode(x);
    else if(x <= 0x7FF)
      output += String.fromCharCode(0xC0 | ((x >>> 6 ) & 0x1F),
                                    0x80 | ( x         & 0x3F));
    else if(x <= 0xFFFF)
      output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F),
                                    0x80 | ((x >>> 6 ) & 0x3F),
                                    0x80 | ( x         & 0x3F));
    else if(x <= 0x1FFFFF)
      output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07),
                                    0x80 | ((x >>> 12) & 0x3F),
                                    0x80 | ((x >>> 6 ) & 0x3F),
                                    0x80 | ( x         & 0x3F));
  }
  return output;
}

/************************************************************/

var seenshas = {};
function ventiUpload(tagname /* 'tpl' or 'img' or 'scr' */, text) {
  assert(text.length<50*1000*1000, "file too big " + text.length);
  if (tagname == 'tpl') {
    //var sha = '0000000000000000000000000000000000000000';
    var sha = sha1(str2rstr_utf8(text)).toLowerCase();
    console.log(JSON.stringify({what:"FILE", tag:tagname, sha:sha, text:text.length}))
    return sha;
  } else if (tagname == 'img' || tagname == 'scr') {
    var sha = sha1(text).toLowerCase();
    if (!(sha in seenshas)) {
      seenshas[sha] = 1
      console.log(JSON.stringify({what:"FILE", tag:tagname, sha:sha, text:text.length}))
      var f = fs.open("vent1/" + sha + ".png", "wb");
      f.write(text);
      f.close();
    }
    return sha;
  }
}

function fext(path) {
  var r = /\.([a-z0-9]+)(?:#|$)/i.exec(path);
  return r ? '.'+r[1] : '';
}

function ventiUploadOrData(tagname, text, ext, ctype) {
  // do not upload small files to venti
  if (text.length <= 1024)
    return "data:" + ctype + ";base64," + btoa(text);

  var shahex = ventiUpload(tagname, text);
  if(shahex)
    return 'vent1/' + shahex + '.png';
  else
    return "data:" + ctype + ";base64," + btoa(text);
}

function memoizable(f) {
  var cache={};
  return function(a) {
    return (a in cache) ? cache[a] : (cache[a] = f(a));
  }
}

// convert data or http url to venti
var ventiurl = memoizable(function (url) {
  //console.log('ventiurl', url.substring(0, 80), url.length>80 ? '___' : '');
  var text='', ctype='';
  if(rx = /^data:([a-z0-9/+-]+)(;[a-z0-9]+=[^;]*)*;base64,\s*([0-9A-Z/+]+=*)$/i.exec(unescape(url))) {
    ctype = rx[1];
    try {
      text = atob(rx[3]);
    } catch(e) { // invalid base64
      return 'about:blank';
    }
  } else if(url.match(/^data:/)) {
    console.log(JSON.stringify({what:"LOG", msg:"strange dataurl " + url}));
    return 'about:blank';
  } else if(url.match(/^about:/)) {
    return "about:blank";
  } else if(!url.match(/^(ftp|http|https|file):\/\//)) { // todo: file:// is insecure
    console.log(JSON.stringify({what:"LOG", msg:"strange url " + url}));
    return url;
  } else {
    var try_direct_download = false;
    var z = esc2(url);
    if(z in resources) {
      var counter = 0;
      do { // follow 301/302-redirect
        z = resources[z];
        if (counter++ > 20) {
          console.log(JSON.stringify({what:"LOG", msg:"redirect loop " + esc2(url)}));
          return '';
        }
      } while(typeof(z) == 'string');
      if (!z) {
        console.log(JSON.stringify({what:"LOG", msg:"in resources, but follower does not " + esc2(url)}));
        try_direct_download = true;
      } else if(!z.text) {
        console.log(JSON.stringify({what:"LOG", msg:"in resources, but empty " + esc2(url)}));
      } else if( z.status/100!=2) {
        console.log(JSON.stringify({what:"LOG", msg:"resources, but status=" + z.status + " " + esc2(url)}));
        try_direct_download = true;
      } else {
        text = z.text;
        ctype = (z.contentType||'').split(';')[0];
      }
    } else {
      console.log(JSON.stringify({what:"LOG", msg:"not in resources " + z}));
      try_direct_download = true;
    }

    // not found in the files downloaded by browser, try to download
    // if loading_aborted_by_timeout then we have really slow site and will not go on with downloads
    if(try_direct_download && milliseconds_since_start()<120000) {
      outreq("GET", url, 2)
      var xhr = GET(url);
      if (xhr && xhr.readyState==4 && (xhr.status/100==2 || url.match(/^file:/))) {
        text = stripHighByte(xhr.responseText);
        ctype = (xhr.getResponseHeader('Content-Type')||'').split(';')[0];
        resources[esc2(url)] = {text:text, status:xhr.status, contentType:ctype};
        outres(url, xhr.status, ctype, text.length, 2);
      } else {
        outres(url, xhr ? xhr.status : 0, "", 0, 2);
      }
    }
  }

  if(text) {
    var ext = ctype=='image/png' ? '.png' : fext(url); // fake ext for canvas
    return ventiUploadOrData('img', text, ext, ctype);
  }

  return '';
});

function renderme(html) {
  // uniq filename
  var filename = milliseconds_current() + Math.random() + '.png';

  // render html
  var page = require('webpage').create();
  var width = 32, height = 32;
  html.replace(/\bwidth\s*:\s*(\d+)/, function(uri, w) { width = w; });
  html.replace(/\bheight\s*:\s*(\d+)/, function(uri, h) { height = h; });
  page.viewportSize = {'width':width, 'height':height};
  page.content = '<html><body style="margin:0">' + html + '</body></html>';
  page.render(filename);

  // read back
  try {
    var f = fs.open(filename, 'rb');
    var text = f.read();
    f.close();
    fs.remove(filename)
    return text;
  } catch(e) {
    console.log(JSON.stringify({what:"LOG", msg:"renderme failed " + e}));
    return atob('R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='); // 1x1 gif
  }
}

// gateway from browser context to phantomjs context
page.onPrompt = function(cmd, s) {
  if(cmd == 'log')              console.log(JSON.stringify({what:"LOG", msg:"in page context " + s}));
  else if(cmd == 'renderme')    return ventiUploadOrData('img', renderme(s), '.png', 'image/png');
  else if(cmd == 'ventiurl')    return ventiurl(s);
  else if(cmd == 'linkurl')     {}
  else if(cmd == 'exit') { // for asserts in page context
    console.log(JSON.stringify({what:"ERROR", code:12, message:"Assertion failed", debug:s}));
    phantom.exit(12);
  }
};


// https://gist.github.com/1088850
function absolutizeURI(base, href) {// RFC 3986
  function parseURI(url) {
    var m = String(url).replace(/^\s+|\s+$/g, '').match(/^([^:\/?#]+:)?(\/\/(?:[^:@]*(?::[^:@]*)?@)?(([^:\/?#]*)(?::(\d*))?))?([^?#]*)(\?[^#]*)?(#[\s\S]*)?/);
    // authority = '//' + user + ':' + pass '@' + hostname + ':' port
    return (m ? {href: m[0]||'',  protocol: m[1]||'',  authority: m[2]||'',  host: m[3]||'', hostname: m[4]||'', port: m[5]||'', pathname: m[6]||'', search: m[7]||'', hash: m[8]||''} : null);
  }
  function removeDotSegments(input) {
    var output = [];
    input.replace(/^(\.\.?(\/|$))+/, '')
         .replace(/\/(\.(\/|$))+/g, '/')
         .replace(/\/\.\.$/, '/../')
         .replace(/\/?[^\/]*/g, function (p) {
      if (p === '/..') {
        output.pop();
      } else {
        output.push(p);
      }
    });
    return output.join('').replace(/^\//, input.charAt(0) === '/' ? '/' : '');
  }

  href = parseURI(href || '');
  base = parseURI(base || '');

  return !href || !base ? null : (href.protocol || base.protocol) +
         (href.protocol || href.authority ? href.authority : base.authority) +
         removeDotSegments(href.protocol || href.authority || href.pathname.charAt(0) === '/' ? href.pathname : (href.pathname ? ((base.authority && !base.pathname ? '/' : '') + base.pathname.slice(0, base.pathname.lastIndexOf('/') + 1) + href.pathname) : base.pathname)) +
         (href.protocol || href.authority || href.pathname ? href.search : (href.search || base.search)) +
         href.hash;
}


// code to be executed in browser context
function onpage() {
  var transparent1x1gif = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

  function dataurlpixel(fillstyle) {
    var c=document/*any document ok?*/.createElement('canvas');
    c.width=1; c.height=1;
    var ctx=c.getContext('2d');
    ctx.fillStyle=fillstyle;
    ctx.fillRect(0,0,1,1);
    return c.toDataURL();
  }

  function AssertionError(message) {
     this.message = message;
  }
  if (typeof Error == "function") { // can be "object", tc: http://gifavs.com/g/470587
    AssertionError.prototype = new Error;
  }

  function assert(cond) {
    if(!cond) {
      var a = 'Assertion failed:';
      for(var i=1; i<arguments.length; i++)
        a+=' ' + arguments[i]
      throw new AssertionError(a);
    }
  }

  function addpx(v) { v=v+''; return v.match(/^[0-9.]+$/) ? v + 'px' : v; }
  function atr(s) {
    return "\"" + (s+"").replace(/[&"\x00-\x1F<>]/g, function(c) { return '&#' + c.charCodeAt(0) + ';'; }) + "\"";
  }
  function linkurl(s) {  // a=href, form=action, ..
    s = ''+s
    s = s.replace(/^http:\/\/\w+\.archive\.org\/web\/\d+\//, '');
    if(s.match(/^javascript:/)) s = '#';
    return s;
  }
  function txt(s) {
    return s.replace(/[<>\&]/g, function(c) { return '&#' + c.charCodeAt(0) + ';'; });
  }


  function expandstyle(css) { // Array(k, v, prio)
    var k = css[0], v = css[1];
    if (v == "")
      return [css];
    if (k=="border") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [
        ["border-top-width", vv[0]], ["border-right-width", vv[0]], ["border-bottom-width", vv[0]], ["border-left-width", vv[0]],
        ["border-top-style", vv[1]], ["border-right-style", vv[1]], ["border-bottom-style", vv[1]], ["border-left-style", vv[1]],
        ["border-top-color", vv[2]], ["border-right-color", vv[2]], ["border-bottom-color", vv[2]], ["border-left-color", vv[2]]
      ];
      if(vv.length==1) return [
        ["border-top-width", vv[0]], ["border-right-width", vv[0]], ["border-bottom-width", vv[0]], ["border-left-width", vv[0]],
        ["border-top-style", vv[0]], ["border-right-style", vv[0]], ["border-bottom-style", vv[0]], ["border-left-style", vv[0]],
        ["border-top-color", vv[0]], ["border-right-color", vv[0]], ["border-bottom-color", vv[0]], ["border-left-color", vv[0]]
      ];
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="border-top") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [["border-top-width", vv[0]], ["border-top-style", vv[1]], ["border-top-color", vv[2]]];
      if(vv.length==1) return [["border-top-width", vv[0]], ["border-top-style", vv[0]], ["border-top-color", vv[0]]]; // "inherit", "initial"
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="border-right") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [["border-right-width", vv[0]], ["border-right-style", vv[1]], ["border-right-color", vv[2]]];
      if(vv.length==1) return [["border-right-width", vv[0]], ["border-right-style", vv[0]], ["border-right-color", vv[0]]]; // "inherit", "initial"
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="border-bottom") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [["border-bottom-width", vv[0]], ["border-bottom-style", vv[1]], ["border-bottom-color", vv[2]]];
      if(vv.length==1) return [["border-bottom-width", vv[0]], ["border-bottom-style", vv[0]], ["border-bottom-color", vv[0]]]; // "inherit", "initial"
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="border-left") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [["border-left-width", vv[0]], ["border-left-style", vv[1]], ["border-left-color", vv[2]]];
      if(vv.length==1) return [["border-left-width", vv[0]], ["border-left-style", vv[0]], ["border-left-color", vv[0]]]; // "inherit", "initial"
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="border-width") {
      var vv = v.split(" ");
      if(vv.length==1) return [["border-top-width", vv[0]], ["border-right-width", vv[0]], ["border-bottom-width", vv[0]], ["border-left-width", vv[0]]];
      if(vv.length==2) return [["border-top-width", vv[0]], ["border-right-width", vv[1]], ["border-bottom-width", vv[0]], ["border-left-width", vv[1]]];
      if(vv.length==3) return [["border-top-width", vv[0]], ["border-right-width", vv[1]], ["border-bottom-width", vv[2]], ["border-left-width", vv[1]]];
      if(vv.length==4) return [["border-top-width", vv[0]], ["border-right-width", vv[1]], ["border-bottom-width", vv[2]], ["border-left-width", vv[3]]];
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="border-style") {
      var vv = v.split(" ");
      if(vv.length==1) return [["border-top-style", vv[0]], ["border-right-style", vv[0]], ["border-bottom-style", vv[0]], ["border-left-style", vv[0]]];
      if(vv.length==2) return [["border-top-style", vv[0]], ["border-right-style", vv[1]], ["border-bottom-style", vv[0]], ["border-left-style", vv[1]]];
      if(vv.length==3) return [["border-top-style", vv[0]], ["border-right-style", vv[1]], ["border-bottom-style", vv[2]], ["border-left-style", vv[1]]];
      if(vv.length==4) return [["border-top-style", vv[0]], ["border-right-style", vv[1]], ["border-bottom-style", vv[2]], ["border-left-style", vv[3]]];
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="border-color") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==1) return [["border-top-color", vv[0]], ["border-right-color", vv[0]], ["border-bottom-color", vv[0]], ["border-left-color", vv[0]]];
      if(vv.length==2) return [["border-top-color", vv[0]], ["border-right-color", vv[1]], ["border-bottom-color", vv[0]], ["border-left-color", vv[1]]];
      if(vv.length==3) return [["border-top-color", vv[0]], ["border-right-color", vv[1]], ["border-bottom-color", vv[2]], ["border-left-color", vv[1]]];
      if(vv.length==4) return [["border-top-color", vv[0]], ["border-right-color", vv[1]], ["border-bottom-color", vv[2]], ["border-left-color", vv[3]]];
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="border-radius") {
      var vv, ww, tl, tr, br, bl;
      if(v.indexOf("/") >= 0) {
        var a = v.split("/");
        vv = a[0].split(" "), ww = a[1].split(" ")
      } else {
        vv = ww = v.split(" ");
      }
           if(vv.length==1) { tl = vv[0]; tr = vv[0]; br = vv[0]; bl = vv[0]; }
      else if(vv.length==2) { tl = vv[0]; tr = vv[1]; br = vv[0]; bl = vv[1]; }
      else if(vv.length==3) { tl = vv[0]; tr = vv[1]; br = vv[2]; bl = vv[1]; }
      else if(vv.length==4) { tl = vv[0]; tr = vv[1]; br = vv[2]; bl = vv[3]; }
      else {
        prompt2("log", "style drop " + k + " " + v);
        return [];
      }

           if(ww.length==1) { tl += " " + ww[0]; tr += " " + ww[0]; br += " " + ww[0]; bl += " " + ww[0]; }
      else if(ww.length==2) { tl += " " + ww[0]; tr += " " + ww[1]; br += " " + ww[0]; bl += " " + ww[1]; }
      else if(ww.length==3) { tl += " " + ww[0]; tr += " " + ww[1]; br += " " + ww[2]; bl += " " + ww[1]; }
      else if(ww.length==4) { tl += " " + ww[0]; tr += " " + ww[1]; br += " " + ww[2]; bl += " " + ww[3]; }
      else {
        prompt2("log", "style drop " + k + " " + v);
        return [];
      }

      return [["border-top-left-radius", tl],
              ["border-top-right-radius", tr],
              ["border-bottom-right-radius", br],
              ["border-bottom-left-radius", bl]];
    }
    if (k=="margin") {
      var vv = v.split(" ");
      if(vv.length==1) return [["margin-top", vv[0]], ["margin-right", vv[0]], ["margin-bottom", vv[0]], ["margin-left", vv[0]]];
      if(vv.length==2) return [["margin-top", vv[0]], ["margin-right", vv[1]], ["margin-bottom", vv[0]], ["margin-left", vv[1]]];
      if(vv.length==3) return [["margin-top", vv[0]], ["margin-right", vv[1]], ["margin-bottom", vv[2]], ["margin-left", vv[1]]];
      if(vv.length==4) return [["margin-top", vv[0]], ["margin-right", vv[1]], ["margin-bottom", vv[2]], ["margin-left", vv[3]]];
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="padding") {
      var vv = v.split(" ");
      if(vv.length==1) return [["padding-top", vv[0]], ["padding-right", vv[0]], ["padding-bottom", vv[0]], ["padding-left", vv[0]]];
      if(vv.length==2) return [["padding-top", vv[0]], ["padding-right", vv[1]], ["padding-bottom", vv[0]], ["padding-left", vv[1]]];
      if(vv.length==3) return [["padding-top", vv[0]], ["padding-right", vv[1]], ["padding-bottom", vv[2]], ["padding-left", vv[1]]];
      if(vv.length==4) return [["padding-top", vv[0]], ["padding-right", vv[1]], ["padding-bottom", vv[2]], ["padding-left", vv[3]]];
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="outline") {
      var vv = v.split(" ");
      if(vv.length==3) return [["outline-color", vv[0]], ["outline-style", vv[1]], ["outline-width", vv[2]]];
      if(vv.length==1) return [["outline-color", vv[0]], ["outline-style", vv[0]], ["outline-width", vv[0]]]; // initial, inherit
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="list-style") {
      var vv = v.split(" ");
      if(vv.length==3) return [["list-style-type", vv[0]], ["list-style-position", vv[1]], ["list-style-image", vv[2]]];
      if(vv.length==1) return [["list-style-type", vv[0]], ["list-style-position", vv[0]], ["list-style-image", vv[0]]]; // initial, inherit
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="background" && v=="inherit") {
      return [];
    }
    if (k=="background" && v!="initial") {
      assert(false, "to be parsed " + k + " " + v);
    }
    if (k=="background-position") {
      var vv = v.split(" ");
      if(vv.length==2) return [["background-position-x", vv[0]], ["background-position-y", vv[1]]];
      if(vv.length==1) return [["background-position-x", vv[0]], ["background-position-y", vv[0]]];
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    if (k=="background-repeat") {
      var vv = v.split(" ");
      if(vv.length==2) return [["background-repeat-x", vv[0]], ["background-repeat-y", vv[1]]];
      if(vv.length==1) {
        if(v=="repeat-x") return [["background-repeat-x", "repeat"], ["background-repeat-y", "no-repeat"]];
        if(v=="repeat-y") return [["background-repeat-x", "no-repeat"], ["background-repeat-y", "repeat"]];
        return [["background-repeat-x", v], ["background-repeat-y", v]];
      }
      prompt2("log", "style drop " + k + " " + v);
      return [];
    }
    return [css];
  }


  // after this, the order of styles should be unsignificant?
  function dedupstyle(css /* [[k, v, prio], ...] */) {
    var css2 = [];
    for(var i=0; i<css.length; i++)  if(!css[i][2]) css2 = css2.concat(expandstyle(css[i])); // not important
    for(var i=0; i<css.length; i++)  if( css[i][2]) css2 = css2.concat(expandstyle(css[i])); // important

    var seen = {};
    for(var i=0; i<css2.length; i++) {
      var k = css2[i][0];
      if(k in seen)
        css2[seen[k]] = null;
      seen[k] = i;
    }

    var css4 = [];
    for(var i=0; i<css2.length; i++)
      if(css2[i] != null && css2[i][1]!='' /*empty value*/)
        css4.push(css2[i]);

    return css4;
  }


  // optional step, reducing length of style="" sring and making it firefox compatible
  function reducestyle(cs, styles) {
    var hash = {};
    for(var i=0; i<styles.length; i++) {
      var k = styles[i][0], v = styles[i][1];
      assert(!(k in hash), "!!! dup " + k + " " + v);
      hash[k] = v;
    }

    var allmargins = ("margin-top" in hash) && ("margin-right" in hash) && ("margin-bottom" in hash) && ("margin-left" in hash);
    var allpaddings = ("padding-top" in hash) && ("padding-right" in hash) && ("padding-bottom" in hash) && ("padding-left" in hash);
    var allbordercolor = ("border-top-color" in hash) && ("border-right-color" in hash) && ("border-bottom-color" in hash) && ("border-left-color" in hash);
    var allliststyle = ("list-style-type" in hash) && ("list-style-position" in hash) && ("list-style-image" in hash);

    //var bgcoloroverride = false;
    var css = "";
    for(var i=0; i<styles.length; i++) {
      var k = styles[i][0], v = styles[i][1];

      if(k=="background-repeat-x" || k=="background-repeat-y" || k=="background-repeat" ||
         k=="background-position-x" || k=="background-position-y" || k=="background-position" ||
         k=="background-attachment" || // always 'scroll', not 'fixed' (at least for <body>), tc:http://www.w3.org/TR/html4/present/graphics.html
         k=="border-top-width" || k=="border-right-width" || k=="border-bottom-width" || k=="border-left-width" ||
         k=="border-top-style" || k=="border-right-style" || k=="border-bottom-style" || k=="border-left-style" ||
         allmargins && (k=="margin-top" || k=="margin-right" || k=="margin-bottom" || k=="margin-left") ||
         allpaddings && (k=="padding-top" || k=="padding-right" || k=="padding-bottom" || k=="padding-left") ||
         allbordercolor && (k=="border-top-color" || k=="border-right-color" || k=="border-bottom-color" || k=="border-left-color") ||
         allliststyle && (k=="list-style-type" || k=="list-style-position" || k=="list-style-image") ||
         k=="outline-color" || k=="outline-style" || k=="outline-width" ) {
      } else if(v=="initial" && (k=="background-image" || k=="background-attachment" || k=="background-origin" || k=="background-clip" ||
                                 k=='border-left-color' || k=='border-right-color' || k=='border-top-color' || k=='border-bottom-color')) {
         // not inheritable, initial is the same as absense
      } else if(k=="-webkit-user-select" || k.indexOf("-webkit-transition")==0) {
         // forget it
      } else if (k=="box-sizing") {
        css += "box-sizing: " + v + "; ";
        css += "-moz-box-sizing: " + v + "; ";  // tc:css-tricks.com
        css += "-ms-box-sizing: " + v + "; ";
      } else if (k=="background-color") {
        css += 'background-color: ' + (v=='initial' ? 'transparent' : v) + "; "; // <input> has 'white' default, so 'transparent' is needed
      } else if (k=="background-image" || k=="list-style-image" || k=="cursor" || k=="-webkit-border-image" || k=="list-style-image") {
        if(v.match(/-webkit-/)) {
          var w = /*e ? e.offsetWidth  :*/ Math.max(parseInt(cs.getPropertyValue('width')), 8); // at least 8x8
          var h = /*e ? e.offsetHeight :*/ Math.max(parseInt(cs.getPropertyValue('height')), 8);
          var renderhtml = '<div style="width:' + addpx(w) + ';'+
                                      'height:' + addpx(h) + ';'+
                             'background-clip:' + cs.getPropertyValue('background-clip') + ';'+
                           'background-origin:' + cs.getPropertyValue('background-origin') + ';'+
                           'background-repeat:' + cs.getPropertyValue('background-repeat') + ';'+
                         'background-position:' + cs.getPropertyValue('background-position') + ';'+
                            'background-image:' + v + '"></div>';
          if(k=="background-image") // stretch image??
            css += 'background-size: 100% 105%;'; // y=105% for opera to avoid cycle
          v = 'url(' + prompt2('renderme', renderhtml) + ')';
        } else {
          v = v.replace(/url\(([^)]+)\)/g, function(m, url) {
            return 'url(' + (prompt2('ventiurl', url) || transparent1x1gif) + ')';
          });
        }
        css += k + ':' + v + ';';
        if(k.match(/^-webkit-/)) {
          css += k.replace(/^-webkit-/, '-moz-') + ':' + v + ';';
          css += k.replace(/^-webkit-/, '-o-') + ':' + v + ';';
        }
      } else if(k.indexOf("-webkit-transform")==0) { // tc: maps.bing.com
        css += k.replace(/^-webkit-/, '-moz-') + ':' + v + ';';
        css += k.replace(/^-webkit-/, '-ms-') + ':' + v + ';';
        css += k.replace(/^-webkit-/, '-o-') + ':' + v + ';';
        css += k + ':' + v + ';';
      } else {
        css += k + ':' + v + ';';
      }
    }

    assert(!("background-repeat" in hash), "!!! background-repeat");
    assert(!("background-position" in hash), "!!! background-position");

    if (("background-position-x" in hash) || ("background-position-y" in hash)) {
      var x = hash["background-position-x"] || "0px"; if(x=="initial") x="0px";
      var y = hash["background-position-y"] || "0px"; if(y=="initial") y="0px";
      if (!(x == '0px' && y == '0px') && !x.match(/[ ,]/) && !y.match(/[ ,]/))
        css += "background-position: " + x + " " + y + "; ";
    }

    if (("background-repeat-x" in hash) || ("background-repeat-y" in hash)) {
      var x = (hash["background-repeat-x"] || "initial").split(',')[0];
      var y = (hash["background-repeat-y"] || "initial").split(',')[0];
      if(x=="initial" && y=="initial") {} //css += "background-repeat: repeat; ";
      else if(x==y)                           css += "background-repeat: " + x + "; ";
      else if(x=="repeat" && y=="no-repeat") css += "background-repeat: repeat-x; ";
      else if(x=="no-repeat" && y=="repeat") css += "background-repeat: repeat-y; ";
      else assert(false, " background-repeat x=" + x + " y=" + y);
    }

    if (("outline-color" in hash) || ("outline-style" in hash) || ("outline-width" in hash)) {
      var color = hash["outline-color"] || "invert"; if(color =="initial") color ="invert";
      var style = hash["outline-style"] || "none";   if(style =="initial") style ="none";
      var width = hash["outline-width"] || "medium"; if(width =="initial") width ="medium";
      css +=  "outline: " + color + " " + style + " " + width + "; "
    }

    if (allliststyle) {
      var t = hash["list-style-type"]     ; if(t =="initial") t ="disc";
      var p = hash["list-style-position"] ; if(p =="initial") p ="outside";
      var i = hash["list-style-image"]    ; if(i =="initial") i ="none";
      css +=  "list-style: " + t + " " + p + " " + i + "; "
    }

    var xx = [["border-", "-width", "border-width", "medium"],
              ["border-", "-style", "border-style", "none"]]; // not specified by standard
    if (allmargins) xx.push(["margin-", "", "margin", "0px"]);
    if (allpaddings) xx.push(["padding-", "", "padding", "0px"]);
    if (allbordercolor) xx.push(["border-", "-color", "border-color", "white"]); // none? initial? white?
    for(var i=0; i<xx.length; i++) {
      var w = xx[i][0], z = xx[i][1], style = xx[i][2], def = xx[i][3];
      if(((w+"top"+z) in hash) || ((w+"right"+z) in hash) || ((w+"bottom"+z) in hash) || ((w+"left"+z) in hash)) {
        var t = hash[w+"top"   +z] || def; if(t=="initial") t=def;
        var r = hash[w+"right" +z] || def; if(r=="initial") r=def;
        var b = hash[w+"bottom"+z] || def; if(b=="initial") b=def;
        var l = hash[w+"left"  +z] || def; if(l=="initial") l=def;
             if(r!=l) css += style + ": " + t + " " + r + " " + b + " " + l + "; "
        else if(t!=b) css += style + ": " + t + " " + r + " " + b + "; "
        else if(t!=r) css += style + ": " + t + " " + r + "; "
        else          css += style + ": " + t + "; "
      }
    }
    return css;
  }

  function fixstyle(e, cs, css1) {
    // http://maps.yandex.ru
    // http://ya.ru
    // https://github.com/gildas-lormeau/SingleFile/blob/master/WebContent/core/scripts/common/docprocessor.js
    for(var i=0; i<css1.length; i++) {
      // tc:http://www.chartsinfrance.net/Mylene-Farmer/news-67938.html, http://www.cuni.cz/IFORUM-2430.html
      if(typeof(css1[i][1]) != "string")
        css1[i][1] = "";

      css1[i][1] = css1[i][1].replace(/^\s+|\s+$/g, "") // trim space in css value (it happens)
      if(css1[i][0]=='position' && css1[i][1]=='fixed') {
        css1[i][1] = 'absolute';
      }
      if (css1[i][0]=='height' && css1[i][1].indexOf('%')>=0) {
        // min-height would be more logical here, but it does not work (tc:ya.ru)
        css1[i][1] = addpx(e ? e.offsetHeight : cs.getPropertyValue("height")); // in pixels instead of %
      }
    }
    return reducestyle(cs, dedupstyle(css1));
  }


  // fake span for ::before and ::after
  function mkspan2(cs, cssarr) {
    if(cssarr.length==0)
      return "";
    var content = "", css = []; //, styles = cssstr.split("; "); // todo: mind ''
    for(var i=0; i<cssarr.length; i++) {
      var k = cssarr[i][0], v = cssarr[i][1];
      if (k=='content') {
        if(v == 'none' || v == 'normal') {
        } else if(v == 'open-quote' || v == 'close-quote' || v == 'no-open-quote' || v == 'no-close-quote') {
          // todo? tc:http://www.samediggi.no/?AId=3158&back=1&MId1=2450&MId2=2522&MId3=&
        } else if(v.length>=2 && v[0]==v[v.length-1] && (v[0]=="'" || v[0]=='"')) {
          content = txt(v.substring(1, v.length-1));
        } else {
          content = v.replace(/url\(([^)]+)\)/g, function(m, url) {
            return '<img src=' + atr(prompt2('ventiurl', url) || transparent1x1gif) + '>';
          });
        } // else 'content: inherit', 'content: -webkit-gradient'...
        // http://www.w3.org/community/webed/wiki/CSS/Properties/content
        // tc:http://trac.webkit.org/browser/trunk/LayoutTests//fast/gradients/generated-gradients.html?format=raw
      } else {
        css.push([k, v]);
      }
    }
    return "<span style=" + atr(fixstyle(null, cs, css)) + ">" + content + "</span>";
  }


  function processdoc(doc, htmlstyle) {
//prompt2("log", "pd " + doc +" "+ doc.defaultView +" "+ doc.body +" "+ doc.documentElement +" "+ doc.rootElement +" "+ doc.childNodes.length);
    if(doc.toString() == '[object SVGDocument]') {
      function svg2xml(a) { // todo: <image xlink:href=
        if(a == "[object Comment]") {
          return '';
        } else if(a == "[object Text]") {
          return txt(a.data);
        } else if(a == "[object CDATASection]") {
          return "<![CDATA[" + txt(a.data) + "]]>";
        } else {
          var o = '<' + a.tagName;
          for(var i=0; i<a.attributes.length; i++)
            o += ' ' + a.attributes[i].name + '=' + atr(a.attributes[i].value);
          o += '>';
          for(var i=0; i<a.childNodes.length; i++)
            o += svg2xml(a.childNodes[i]);
          return o + '</' + a.tagName + '>';
        }
      }

      assert(doc.rootElement != null, "doc.rootElement == null, malformed svg")
      var csroot = doc.defaultView.getComputedStyle(doc.rootElement);
      return {url:        doc.URL,
              doctype:    '',
              title:      (''+doc.title),
              css:        '',
              compatmode: (''+doc.compatMode),
              body:       '<div class="svgbody"' +
                                 ' style="width:' + addpx(csroot.getPropertyValue("width")) + ';' +
                                        'height:' + addpx(csroot.getPropertyValue("height")) + ';' +
                          '">' + svg2xml(doc.rootElement) + '</div>'};
    }

    assert(doc.documentElement != null, "doc.documentElement == null");
    assert(typeof(doc.documentElement.tagName)=="string", "typeof(doc.documentElement.tagName)=" + typeof(doc.documentElement.tagName)); // http://www.esc-history.com/
    assert(typeof(doc.URL)                    =="string", "typeof(doc.URL)="                     + typeof(doc.URL));                     // http://creaturesfrance.free.fr/

    if (doc.documentElement.tagName.toLowerCase() == "feed" && doc.documentElement.toString() == "[object Element]") {
      assert(false, "Atom not supported"); // todo: Atom
    } else if (doc.documentElement.tagName.toLowerCase() == "rss" && doc.documentElement.toString() == "[object Element]") {
      assert(false, "RSS not supported"); // todo: RSS
    } else if (doc.documentElement.tagName.toLowerCase() == "html" && doc.documentElement.toString() == "[object HTMLHtmlElement]") {
      // ok
    } else if (doc.documentElement.tagName.toLowerCase() == "svg"  && doc.documentElement.toString() == "[object SVGSVGElement]") {
      // ok
      // it differs from svg case above, doc.toString()=="[object Document]"
    } else {
      assert(false, "doc.documentElement.tagName=" + doc.documentElement.tagName + " doc.documentElement.toString()=" + doc.documentElement.toString());
    }

    var wmipp = doc.getElementById('wm-ipp');  // hide wayback machine toolbar
    if (wmipp && wmipp.tagName=='DIV') {
      wmipp.style.display = 'none';
    }


    var cshtml = doc.defaultView.getComputedStyle(doc.documentElement /*<html> <svg>*/);
    assert(cshtml != null,  "cshtml == null");
    assert(cshtml.toString() == "[object CSSStyleDeclaration]", "wrong cshtml: " + cshtml.toString());
    var csbody = doc.body ? doc.defaultView.getComputedStyle(doc.body) : null;
    var usedbodycolor = csbody && cshtml.getPropertyValue("background-color") == 'rgba(0, 0, 0, 0)';
    // body color should not hide image moved from <body> to <html>? http://www.linkdex.com/about/features/
    var usedbodyimage = usedbodycolor && cshtml.getPropertyValue("background-image") == 'none';

    function processe(e) { // [html, css]

//prompt2("log", "pe " + e.tagName + " " + e);
      function cssfor(pseudo) {
        var css = [];
        var c = doc.defaultView.getMatchedCSSRules(e, pseudo);
        if (c!=null) {
          for(var i=0; i<c.length; i++) {
            var style = c[i].style; // CSSStyleDeclaration
            for(var j=0; j<style.length; j++)
              css.push([style[j], style.getPropertyValue(style[j]), style.getPropertyPriority(style[j])]);
          }
        }
        return css;
      }

      // e.style==null for a strange element with e.tagName=='result' and e.toString=="[object Element]"
      // tc(unstable): http://www.collider.com/2010/03/10/christopher-nolan-speaks-updates-on-dark-knight-sequel-and-superman-man-of-steel/
      if(e == "[object Text]" || e == "[object Comment]" || e.style==null || typeof(e.tagName)!="string")
        return {html:"", css:""};

      var tag = e.tagName.toLowerCase();

      if(tag == "script" || tag == "noscript" || tag == "noframes" || tag == "audio" ||
         tag == "title" || tag == "style" || tag == "link" || tag == "param") {
        return {html:"", css:""};
      }

      var cs = doc.defaultView.getComputedStyle(e);
      // do not emit invisible items (this will remove '<input type="hidden"'> as well)
//      if(cs.getPropertyValue("display") == "none") {
//        return {html:"", css:""};
//      }
      if(doc.URL.match(/^http:\/\/\w+\.archive\.org\/\d+\//) &&
         tag=='div' && (e.id=='wm-ipp' || e.id=='positionHome')) { // wayback machine toolbar or iframe with error
        return {html:"", css:""};
      }

      var css1 = cssfor('');
      if(e.style.toString() == "[object CSSStyleDeclaration]") { // may be something else, it happens
        for(var j=0; j<e.style.length; j++) {
          if(typeof(e.style.getPropertyValue) == "function" && typeof(e.style.getPropertyPriority) == "function") { // this happens too, check outside cycle doesnt help
            css1.push([e.style[j], e.style.getPropertyValue(e.style[j]), e.style.getPropertyPriority(e.style[j])]);
          }
        }
      } else {
        prompt2("log", e.style.toString() + " instead of [object CSSStyleDeclaration]")
      }
      if(cs.getPropertyValue("text-align")=='-webkit-auto')
        css1 = [['text-align',    cs.getPropertyValue("direction")=='rtl' ? 'right' : 'left']].concat(css1);

      var needCloseTag = true;
      var o = "<" + tag;
      var eltcss = '';

      if (e.toString().match(/^\[object SVG/)) {
        for(var i=0; i<e.attributes.length; i++) {
          var k = e.attributes[i].name, v = e.attributes[i].value;
          if(e.tagName.toLowerCase()=='image' && k=='href')
            k = 'xlink:href';
          if(e.tagName.toLowerCase()=='image' && k=='xlink:href')
            v = prompt2('ventiurl', absolutizeURI(doc.baseURI, v)) || transparent1x1gif;
          if(k!='style')
            o += ' ' + k + '=' + atr(v);
        }
      }
      else if (tag == "html") {
        css1 = css1.concat([['overflow-x', 'visible', 'important'],
                            ['overflow-y', 'visible', 'important'],
                            ['height',              '', 'important'],
                            ['background-color',    '', 'important'],  // will be used on html1
                            ['background-size',     '', 'important'],
                            ['background-position', '', 'important'],
                            ['background-image',    '', 'important'],
                            ['background-repeat',   '', 'important']]);
        o = '<div class="html"';
        tag = "div";
      }
      else if (tag == "body") {
        css1 = [["vertical-align", "bottom"],
                ['min-height',    cs.getPropertyValue("height")       ],
                ['color',         cs.getPropertyValue('color')        ], // text=
                ['margin-left',   cs.getPropertyValue('margin-left'  )], // leftmargin=, marginwidth=
                ['margin-top',    cs.getPropertyValue('margin-top'   )], // topmargin=, marginheight=
                ['margin-right',  cs.getPropertyValue('margin-right' )], // rightmargin=, marginwidth=
                ['margin-bottom', cs.getPropertyValue('margin-bottom')]] // bottommargin=, marginheight=
          .concat(css1)
          .concat([['overflow-x', 'visible', 'important'],
                   ['overflow-y', 'visible', 'important'],
                   ['height',     '',        'important']]);

        if (usedbodycolor)
          css1 = css1.concat([['background-color', '', 'important']]);

        if (usedbodyimage)  // <body>'s image used for html1, should not be used twice
          css1 = css1.concat([['background-size',     '', 'important'],
                              ['background-position', '', 'important'],
                              ['background-image',    '', 'important'],
                              ['background-repeat',   '', 'important']]);

        o = '<div class="body"';
        tag = "div";
      }
      else if (tag == "frameset") {
        var border = e.getAttribute('border') || e.getAttribute('framespacing') || '6';
        if(!border.match(/^\d+$/)) border='0';

        var o = '<table class="frameset" border="0" cellpadding="0" cellspacing="0">';
        var rows = (e.rows||'').indexOf(',')>0;

        if (!rows) o += '<tr>';
        var prevch = null;
        for (var i = 0; i < e.childNodes.length; i++) {
          var ch = e.childNodes[i];

          // <frameset> recureses, <frame> be converted to <div>
          // no else meaningful inside frameset
          if (ch.tagName == "FRAMESET" || ch.tagName == "FRAME") {
            if(prevch) {
              var border2 = border;
              var fb1 = prevch.tagName != "FRAME" || prevch.frameBorder==='0';
              var fb2 = ch.tagName != "FRAME" || ch.frameBorder==='0';
              if(fb2 && fb1) border2 = '0';

              if(border2!='0') {
                if (rows) o += '<tr>';
                o += '<td style="background-color: silver;">';
                o += '<div style="width: ' + border2 + 'px; height: ' + border2 + 'px;"></div>';
                o += '</td>\n';
                if (rows) o += '</tr>';
              }
            }
            if (rows) o += '<tr>';
            var child = processe(ch);
            o += '<td style="vertical-align:top">' + child.html + '</td>';
            eltcss += child.css;
            if (rows) o += '</tr>';
            prevch = ch;
          }
        }
        if (!rows) o += '</tr>';
        o += '</table>';
        return {html: o, css: eltcss};
      }
      else if (tag == "frame" || tag == "iframe") { // todo: mind position:fixed -> position:absolute
        var noscroll = (e.scrolling||'').toLowerCase()=='no' || e.scrolling=='0';
        css1 = [['display', 'inline-block'], ['vertical-align', 'bottom'],
                ['width',  e.width  ? addpx(e.width)  : cs.getPropertyValue("width")], // always
                ['height', e.height ? addpx(e.height) : cs.getPropertyValue("height")],
                ['margin-left',   cs.getPropertyValue('margin-left'  )], // leftmargin=, marginwidth=
                ['margin-top',    cs.getPropertyValue('margin-top'   )], // topmargin=, marginheight=
                ['margin-right',  cs.getPropertyValue('margin-right' )], // rightmargin=, marginwidth=
                ['margin-bottom', cs.getPropertyValue('margin-bottom')], // bottommargin=, marginheight=
                ['overflow-x', noscroll ? 'hidden' : 'auto'],
                ['overflow-y', noscroll ? 'hidden' : 'auto']].concat(css1);
        if(tag=='frame') // <frameset> takes care on <frame>'s border
          css1 = [['border-width', '0']].concat(css1);
        else
          css1 = [['border-style', 'inset'],
                  ['border-width', addpx(e.frameBorder=='0' ? '0' : '2') + ';']].concat(css1);
        if (e.contentDocument == null || e.contentDocument.documentElement == null)
          return {html: '', css: eltcss};
        try { // ignore asserts. todo: it does no work, assert causes phantom.exit(12);
          var processed = processdoc(e.contentDocument, fixstyle(e, cs, css1) );
          return {html: processed.body, css: eltcss + processed.css};
        } catch(e) {
          return {html: '', css: eltcss};
        }
      }
      else if (tag == "meta") {
        needCloseTag = false;
        // no need to store <meta content="text/html; charset=utf-8" http-equiv="content-type">, it will be utf8 anyway
        if (e.httpEquiv && e.httpEquiv.toLowerCase() == "content-type")
          return {html: '', css: ''};
        if (!e.name || !(e.name.toLowerCase() == "description" || e.name.toLowerCase() == "keywords")) // store only "name" and "keywords"
          return {html: '', css: ''};
        if (e.name     ) o += " name="       + atr(e.name);
        if (e.httpEquiv) o += " http-equiv=" + atr(e.httpEquiv);
        if (e.content  ) o += " content="    + atr(e.content);
      }
      else if (tag == "textarea") {
        if (e.cols  != 20) o += " cols="         + atr(e.cols);
        if (e.rows   != 2) o += " rows="         + atr(e.rows);
        if (e.placeholder) o += " placeholder="  + atr(e.placeholder); // type=text, html5
        if (e.name       ) o += " name="         + atr(e.name);
        if (e.disabled   ) o += ' disabled="disabled"';
        if (e.readOnly   ) o += ' readonly="readonly"';
      }
      else if (tag == "label") {
        if (e.htmlFor) o += " for=" + atr(e.htmlFor);
      }
      else if (tag == "object" || tag == "applet" || tag == "video" || tag == "embed") {
        o = '<div class=' + atr(tag) + '';
        tag = "div";
        css1 = [['background-color',  'rgba(224, 224, 224, 0.5)'],
                ['display',           'inline-block'],
                ['margin',            '0'],
                ['padding',           '0'],
                ['border-width',      '0'],
                ['width',             addpx(e.width || 300)],
                ['height',            addpx(e.height || 150)]].concat(css1);
      }
      else if (tag == "canvas") {
        needCloseTag = false; // for <img>
        o = '<img src=' + atr(tag=='canvas' ? prompt2('ventiurl', e.toDataURL()) : transparent1x1gif);
        css1 = [['border-width', '0'],
                ['width',  addpx(e.width || 300)],
                ['height', addpx(e.height || 150)]].concat(css1);
      }
      else if (tag == "form") {
        if (e.acceptCharset) o += " accept-charset=" + atr(e.acceptCharset);
        if (e.action       ) o += " action="         + atr(linkurl(e.action));
        if (e.method       ) o += " method="         + atr(e.method);
        if (e.target       ) o += " target="         + atr(e.target);
        if (e.encoding != "application/x-www-form-urlencoded")
                             o += " enctype="        + atr(e.encoding);
      }
      else if (tag == "fieldset") {
        if (e.disabled) o += ' disabled="disabled"';
      }
      else if (tag == "del" || tag == "ins") {
        if (e.cite    ) o += " cite="     + atr(linkurl(e.cite));
        if (e.datetime) o += " datetime=" + atr(e.datetime);
      }
      else if (tag == "blockquote" || tag == "q") {
        if (e.cite    ) o += " cite="     + atr(linkurl(e.cite));
      }
      else if (tag == "button") {
        if (e.type != "submit") o += " type="   + atr(e.type);
        if (e.disabled        ) o += ' disabled="disabled"';
        if (e.name            ) o += " name="   + atr(e.name);
        if (e.value           ) o += " value="  + atr(e.value);
      }
      else if (tag == "input") {
        needCloseTag = false;
        if (e.type                   != "text") o += " type="         + atr(e.type);
        if (e.accept                          ) o += " accept="       + atr(e.accept); // type=file
        if (e.placeholder                     ) o += " placeholder="  + atr(e.placeholder); // type=text, html5
        if (e.getAttribute('autosave') != null) o += " autosave="     + atr(e.getAttribute('autosave')); // tc:vaadin.com
        if (e.getAttribute('results')  != null) o += " results="      + atr(e.getAttribute('results'));
        if (e.maxLength               < 500000) o += " maxlength="    + atr(e.maxLength);
        if (e.name                            ) o += " name="         + atr(e.name);
        if (e.size                       != 20) o += " size="         + atr(e.size);
        if (e.src                             ) o += " src="          + atr(prompt2('ventiurl', e.src) || transparent1x1gif);
        if (e.checked                         ) o += ' checked="checked"';
        if (e.disabled                        ) o += ' disabled="disabled"';
        if (e.readOnly                        ) o += ' readonly="readonly"';
        o += " value=" + atr(e.value); // <input type="submit" value="">, empty value must be present
      }
      else if (tag == "img") {
        needCloseTag = false;
        if (e.alt     ) o += " alt="      + atr(e.alt);
        if (e.isMap   ) o += ' ismap="ismap"';
        if (e.longDesc) o += " longdesc=" + atr(linkurl(e.longDesc));
        if (e.lowsrc  ) o += " lowsrc="   + atr(prompt2('ventiurl', e.lowsrc) || transparent1x1gif);
        if (e.name    ) o += " name="     + atr(e.name);
        if (e.src     ) o += " src="      + atr(prompt2('ventiurl', e.src) || transparent1x1gif);
        if (e.useMap  ) o += " usemap="   + atr(e.useMap);
        if (e.getAttribute('vspace')  != null) css1 = [["margin-top", addpx(e.vspace)],
                                                       ["margin-bottom", addpx(e.vspace)]].concat(css1);
        if (e.getAttribute('hspace')  != null) css1 = [["margin-left", addpx(e.hspace)],
                                                       ["margin-right", addpx(e.hspace)]].concat(css1);
        if (e.getAttribute('border')  != null) css1 = [["border-width", addpx(e.border)]].concat(css1);
        // height="0" must go to css, it is not the same as height absense (tc: http://www.render.ru/books/show_book.php?book_id=1622)
        if (e.getAttribute('width')   != null) css1 = [["width", addpx(e.width)]].concat(css1);
        if (e.getAttribute('height')  != null) css1 = [["height", addpx(e.height)]].concat(css1);
      }
      else if (tag == "hr") {
        needCloseTag = false;
        if (e.noshade) o += ' noshade="noshade"';
        if (e.getAttribute('width') != null) o += " width=" + atr(e.width);
        if (e.getAttribute('size')  != null) o += " size=" + atr(e.size);
      }
      else if (tag == "br") {
        needCloseTag = false;
        if (e.clear) o += " clear=" + atr(e.clear);
      }
      else if (tag == "font" || tag == "basefont") {
        if (e.size ) o += " size=" + atr(e.size);
        if (e.color) o += " color=" + atr(e.color);
        if (e.face ) o += " face=" + atr(e.face);
      }
      else if (tag == "marquee") {
        if (e.bgColor) css1 = [["background-color", e.bgColor]].concat(css1);
      }
      else if (tag == "li") {
        if (e.value) o += " value=" + atr(e.value);
        if (e.type  ) css1 = [["list-style-type", e.type]].concat(css1); // 1|a|A|i|I|disc|circle|square
      }
      else if (tag == "ol") {
        if (e.reversed  ) o += ' reversed="reversed"';
        if (e.start != 1) o += " start=" + atr(e.start);
      }
      else if (tag == "ul") {
        if (e.type  ) css1 = [["list-style-type", e.type]].concat(css1); // disc|circle|square
      }
      else if (tag == "select") {
        if (e.disabled ) o += ' disabled="disabled"';
        if (e.multiple ) o += ' multiple="multiple"';
        if (e.size != 0) o += " size=" + atr(e.size);
      }
      else if (tag == "option") {
        if (e.disabled) o += ' disabled="disabled"';
        if (e.selected) o += ' selected="selected"';
        if (e.label   ) o += " label=" + atr(e.label);
        if (e.value   ) o += " value=" + atr(e.value);
      }
      else if (tag == "a") {
        if (e.href    ) o += " href="     + atr(linkurl(e.href));
        if (e.hreflang) o += " hreflang=" + atr(e.hreflang);
        if (e.name    ) o += " name="     + atr(e.name); // html4
        if (e.target  ) o += " target="   + atr(e.target);
        if (e.type    ) o += " type="     + atr(e.type); // media type
        if (doc.body && doc.body.link)  css1 = [['color', cs.getPropertyValue("color")]].concat(css1);
      }
      else if (tag == "area") {
        if (e.alt     ) o += " alt="     + atr(e.alt);
        if (e.coords  ) o += " coords="  + atr(e.coords);
        if (e.href    ) o += " href="    + atr(linkurl(e.href));
        if (e.shape   ) o += " shape="   + atr(e.shape);  // default|rect|circle|poly
        if (e.target  ) o += " target="  + atr(e.target);
      }
      else if (tag == "optgroup") {                 //  todo: HTMLOptGroupElement
        if (e.disabled) o += ' disabled="disabled"';
        if (e.label   ) o += " label=" + atr(e.label);
      }
      else if (tag == "menu") {
        if (e.type ) o += " type="   + atr(e.type);
        if (e.label) o += " label="  + atr(e.label);
      }
      else if (tag == "table") {
        if (e.frame       ) o += " frame="       + atr(e.frame);    // Not supported in HTML5
        if (e.rules       ) o += " rules="       + atr(e.rules);    // Not supported in HTML5
        if (e.caption     ) o += " caption="     + atr(e.caption);  // Not supported in HTML5
        if (e.getAttribute('background') != null) {  // www.xxc.ru
          var url = absolutizeURI(doc.baseURI, e.getAttribute('background'));
          css1 = [["background-image", "url(" + url + ")"]].concat(css1);
        }
        if (e.bgColor     ) css1 = [["background-color", cs.getPropertyValue("background-color")]].concat(css1);
        if (e.getAttribute('width')       != null) css1 = [["width", addpx(e.width)]].concat(css1); // todo: width="100%"
        if (e.getAttribute('height')      != null) css1 = [["height", addpx(e.getAttribute('height'))]].concat(css1); // there is no e.height in WebKit DOM, tc: www.xxx.com
        if (e.getAttribute('cellspacing') != null) css1 = [['border-collapse', 'separate'],
                                                           ["border-spacing", addpx(e.cellSpacing)]].concat(css1);
        if (e.getAttribute('border')      != null) css1 = [["border-style", 'outset'], // mic.org
                                                           ["border-color", '#999'],
                                                           ["border-width", addpx(e.border)]].concat(css1);
        if (e.getAttribute('bordercolor') != null) {  // http://www.stm.info/info/comm-09/co090914.htm
          css1 = [["border-style", 'solid'],
                  ["border-color", e.getAttribute('bordercolor')],
                  ["border-width", addpx(3)]].concat(css1);
        }
      }
      else if (tag == "col" || tag == "colgroup") {
        if (e.ch       ) o += " char="    + atr(e.ch);
        if (e.chOff    ) o += " charoff=" + atr(e.chOff);
        if (e.span != 0) o += " span="    + atr(e.span);
        if (e.bgColor  ) css1 = [["background-color", cs.getPropertyValue("background-color")]].concat(css1);
        if (e.vAlign   ) css1 = [["vertical-align", addpx(e.vAlign)]].concat(css1);
        if (e.getAttribute('width')  != null) css1 = [["width", addpx(e.width)]].concat(css1);
      }
      else if (tag == "tbody" || tag == "tfoot" || tag == "thead" || tag == "tr") {
        if (e.ch          ) o += " char="    + atr(e.ch);
        if (e.chOff       ) o += " charoff=" + atr(e.chOff);
        if (e.bgColor     ) css1 = [["background-color", cs.getPropertyValue("background-color")]].concat(css1);
        if (e.vAlign      ) css1 = [["vertical-align", addpx(e.vAlign)]].concat(css1);
      }
      else if (tag == "td" || tag == "th") {

        for(var table = e; table; table = table.parentNode) {
          if(table.tagName == 'TABLE') {
            if (table.border && table.border!='0') // inset 1px color-from-table-css
              css1 = [["border-top-color",    cs.getPropertyValue("border-top-color")],
                      ["border-left-color",   cs.getPropertyValue("border-left-color")],
                      ["border-right-color",  cs.getPropertyValue("border-right-color")],
                      ["border-bottom-color", cs.getPropertyValue("border-bottom-color")],
                      ["border-style", 'inset'],
                      ["border-width", '1px']].concat(css1);
            if (table.getAttribute('cellpadding') != null)
              css1 = [["padding", addpx(table.cellPadding)]].concat(css1);
            break;
          }
        }

        css1 = [['color', cs.getPropertyValue("color")]].concat(css1); // body=text if BackCompat?
        if (e.abbr        ) o += " abbr="      + atr(e.abbr);
        if (e.axis        ) o += " axis="      + atr(e.axis);
        if (e.ch          ) o += " char="      + atr(e.ch);
        if (e.chOff       ) o += " charoff="   + atr(e.chOff);
        if (e.headers     ) o += " headers="   + atr(e.headers);
        if (e.colSpan != 1) o += " colspan="   + atr(e.colSpan);
        if (e.rowSpan != 1) o += " rowspan="   + atr(e.rowSpan);
        if (e.getAttribute('background') != null) { // www.mic.org
          var url = absolutizeURI(doc.baseURI, e.getAttribute('background'));
          css1 = [["background-image", "url(" + url + ")"]].concat(css1);
        }
        if (e.bgColor     ) css1 = [["background-color", cs.getPropertyValue("background-color")]].concat(css1);
        if (e.noWrap      ) css1 = [["white-space", "nowrap"]].concat(css1);
        if (e.vAlign      ) css1 = [["vertical-align", addpx(e.vAlign)]].concat(css1); // valign=30 (tc:gazpromao.narod.ru)
        if (e.getAttribute('width')  != null) css1 = [["width",  addpx(e.width )]].concat(css1);
        if (e.getAttribute('heigth') != null) css1 = [["height", addpx(e.height)]].concat(css1);
      }
      else if (tag == "p") { // tc:ibm.com, <ul>, <div> cannot be inside <p>
        if(doc.compatMode != 'BackCompat') { // in BackCompat, <p> is so special, better not do the change
          css1 = [["margin-top", cs.getPropertyValue("margin-top")],
                  ["margin-bottom", cs.getPropertyValue("margin-bottom")]].concat(css1);
          o = '<div';
          tag = 'div'
        }
      }
      else if (tag == "xmp") {
        o = "<pre";
        tag = 'pre'
      }
      else {
        // <p>, <span>, <div>, <b>, <pre>, <code>, <h1> and so on
      }

      //if (e.lang != "") o += " lang=" + atr(e.lang);
      //if (e.id       ) o += " id=" + atr(e.id);           // no needed, debug only
      //if (!e.toString().match(/^\[object SVG/) && e.className) o += " class=" + atr(e.className); // no needed, debug only

      if (e.dir  ) css1 = [["direction", e.dir]].concat(css1);
      if (e.align) o += " align=" + atr(e.align);
      if (e.title) o += " title=" + atr(e.title);


      //  todo: filter out ihherited types which the parent has
      if(tag != "head" && tag != "title" && tag != "base" && tag != "meta") { // they have css, but it is senseless
        var cssstr = fixstyle(e, cs, css1);
        if (cssstr)
          o += " style=" + atr(cssstr);
      }

      if(needCloseTag) {
        var ochildren = "";
        for (var i = 0; i < e.childNodes.length; i++) {
          if (e.childNodes[i] != "[object Text]") { // <tag>
            var child = processe(e.childNodes[i]);
            ochildren += child.html;
            eltcss += child.css;
          } else {
            var t = e.childNodes[i].data;
            if (cs.getPropertyValue('white-space').indexOf('pre')!=0) { // not <pre> nor <xmp> nor <textarea>
              t = t.replace(/[\r\n\t ]+/g /* \s+ except &nbsp; */, ' ').replace(/^ | $/, '\n');
            }
            var firstletter;
            if(i==0 && (firstletter = cssfor('first-letter')).length>0) {
              t = t.replace(/^(\s*)(\S)(.*)$/m, function(c, p1, p2, p3) { // replace the 1st non-space letter
                var csfl = doc.defaultView.getComputedStyle(e, 'after')
                return txt(p1) + '<span style=' + atr(fixstyle(null, csfl, firstletter)) + '>' + txt(p2) + '</span>' + txt(p3);
              });
            } else {
              t = txt(t);
            }
            ochildren += t;
          }
        }

        if (tag == "head") // do not emit <head> but emit its children
          o = ochildren;
        else {
          o += '>';
          if (e.parentNode && e.parentNode.toString().match(/^\[object SVG/)) {
            // do not insert <span> inside <svg>...</svg>, tc:http://nickqizhu.github.io/dc.js/
            o += ochildren;
          } else {
            o += mkspan2(doc.defaultView.getComputedStyle(e, 'before'), cssfor('before'));
            o += ochildren;
            o += mkspan2(doc.defaultView.getComputedStyle(e, 'after'), cssfor('after'));
          }
          o += '</' + tag + '>';
        }
      } else {
        o += '/>';
      }

      return {html:o, css:eltcss};
    } // processe

    prompt2("log", "get doctype");
    var doctype = "";
    if(doc.doctype) {
        var dt = doc.doctype;
        if (dt) {
           doctype += "<!DOCTYPE " + dt.nodeName;
           if (dt.publicId) {
             doctype += " PUBLIC " + atr(dt.publicId);
             if (dt.systemId) doctype += " " + atr(dt.systemId);
           } else if (dt.systemId)
             doctype += " SYSTEM " + atr(dt.systemId);
           if (dt.internalSubset)
             doctype += " [" + dt.internalSubset + "]";
           doctype += ">";
        }
    } else {
      doctype = "<!-- no doctype -->\n";
    }


    // keep html5 (new tags, also keyboard icon on google.com), but lift BackCompat to html 4.01
    if (doc.compatMode=='BackCompat' || doctype.indexOf('frameset.dtd')>0) {
      doctype = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'; // still BackCompat
              + '\n<!-- ' + doctype + ' -->'; // old doctype
    }
//    doctype += '\n<!-- compatMode ' + doc.compatMode + ' -->';


    prompt2("log", "get css");
    function ocss(z) {
      if (z == "[object CSSMediaRule]") {
        var o = "";
        for(var k=0; k<z.cssRules.length; k++)
          o += ocss(z.cssRules[k]);
        return o;
      }
      if (z == "[object CSSFontFaceRule]") {
//        prompt2("log", "@font-face {" + z.style.cssText + "}");
//        return "@font-face {" + z.style.cssText + "}\n";
        var srcs = [], o = '@font-face {\n'
        for(var j=0; j<z.style.length; j++) {
          var k = z.style[j], v = z.style.getPropertyValue(k);
          if(k=='src') {
            v = v.replace(/url\(([^)]+)\)(\s*format\([^)]+\)|)/g, function(a, url, rest) {
              var du = prompt2('ventiurl', url);
              if(du)
                srcs.push(' url(' + du + ')' + rest);
            });
            o += 'src:';
            for(var i=0; i<srcs.length; i++) {
              if(i>0)o += ',';
              o += srcs[i];
            }
            o += ';\n';
          } else {
            o += k + ':' + v + ';\n';
          }
        }
        return srcs.length==0 /* no url put to venti */ ? '' : o + '}\n';
      }
      if (z == "[object CSSImportRule]" ) {
        var o = "";
        if (z.styleSheet!=null) // inside @media print (tc:www.fbi.gov)
          for(var k=0; k<z.styleSheet.cssRules.length; k++)
            o += ocss(z.styleSheet.cssRules[k]);
        return o;
      }
      return "";
    }

    var head = '';
    var ss = doc.styleSheets;
    for(var i=0; i<ss.length; i++) {
      assert(ss[i].cssRules!=null, "restart with --web-security=no");
      for(var j=0; j<ss[i].cssRules.length; j++)
        head += ocss(ss[i].cssRules[j]) ;
    }


    prompt2("log", "get bg");


    var html = '<div class="html1"';
    html += ' style="width: ' + doc.defaultView.innerWidth + 'px;';
    html += 'text-align: left;'; // do not inherit from outer frame
    html += 'overflow-x: ' + (cshtml.getPropertyValue("overflow-x")=='hidden'?'hidden':'auto') + ';';
    html += 'overflow-y: ' + (cshtml.getPropertyValue("overflow-y")=='hidden'?'hidden':'auto') + ';';

    // choose color for fake 'screen' background, must not be transparent anyway
    var bgcolor = (usedbodycolor ? csbody : cshtml).getPropertyValue("background-color");
    html += ' background-color: ' + bgcolor + ';';

    var bgimage = (usedbodyimage ? csbody : cshtml).getPropertyValue("background-image");
    if (bgimage!='none') {
      bgimage = bgimage.replace(/url\(([^)]+)\)/g, function(m, url) {
        return 'url(' + (prompt2('ventiurl', url) || transparent1x1gif) + ')';
      });
      html += ' background-image: '    + bgimage + ';';
      html += ' background-repeat: '   + (usedbodyimage ? csbody : cshtml).getPropertyValue("background-repeat") + ';';
      html += ' background-size: '     + (usedbodyimage ? csbody : cshtml).getPropertyValue("background-size") + ';'; // tc: http://typeclassopedia.bitbucket.org/#slide-4
      html += ' background-position: ' + (usedbodyimage ? csbody : cshtml).getPropertyValue("background-position-x").split(' ')[0] + ' '
                                       + (usedbodyimage ? csbody : cshtml).getPropertyValue("background-position-y").split(' ')[0] + ';';
    }
    // same as position:static, but makes point for further fixeds and absolutes
    // must be before `+=htmlstyle` (tc:maps.google.com, it has `position:absolute`)
    html += 'position: relative;';
    html += htmlstyle;
    // z-index must be after `+=htmlstyle` as it may have "z-index: -1", tc:www.davidrumsey.com, http://archive.is/OMCwI
    html += '; z-index: 0';
    html += '">';

    prompt2("log", "get body");
    var ch = processe(doc.documentElement/*<html> <svg>*/);

    html += ch.html;
    html += '</div>';
    return {'url':doc.URL, 'doctype':doctype, 'title':(''+doc.title), 'css':head + ch.css, 'body':html, 'compatmode':doc.compatMode};
  } // processdoc

  try {
    return processdoc(document,
      // min-height needed only if there is an absolute/fixed positioned element, todo: check for it
      // document.height is undefined for svg (tc:http://croczilla.com/bits_and_pieces/svg/samples/xbl2/xbl2.xml)
      document.height
        ? 'min-height: ' + addpx(Math.max(document.height, document.defaultView.innerHeight)) + ';'  // td:ya.ru, css-tricks.com, python.org
        : 'height: '     + addpx(document.defaultView.innerHeight) + ';'
      );
  } catch(e) {
    prompt2("exit", e.toString());
    return '';
  }
}


  function txt(s) {
    return s.replace(/[<>\&]/g, function(c) { return '&#' + c.charCodeAt(0) + ';'; });
  }

function savepng() {
  console.log(JSON.stringify({what:"LOG", msg:"render png"}));
  var pngfile = milliseconds_current() + Math.random() + '.png';
  page.scrollPosition = { top: 0, left: 0 };
  page.render(pngfile);
  console.log(JSON.stringify({what:"LOG", msg:"upload png"}));
  try {
    var f = fs.open(pngfile, "rb");
    assert(f, 'error open png');
    var blob = f.read();
    f.close();
    //fs.remove(pngfile)
    assert(blob, 'error read png');
    console.log(JSON.stringify({what:"LOG", msg:"upload png " + blob.length}));
    var ventipng = ventiUpload('scr', blob);
    assert(ventipng, 'error save png to venti');
    return 'vent1://' + ventipng + '.png?size=' + blob.length;
  } catch(e) {
    return '';
  }
}

function removeArchivePrefix(s) {
  return s/*.replace(/^http:\/\/\w+\.archive\.org\/web\/\d+\//, '')*/;
}

// todo: also headers "X-Archive-Orig-Date: Fri, 17 Jun 2011 00:01:21 GMT"
function timeFromUrl(s) {
  var d = new Date(); // current time
//  s.replace(/^http:\/\/\w+\.archive\.org\/web\/(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)\//,
//    function(prefix, yyyy, mm, dd, h, m, s) {
//      d.setUTCFullYear(yyyy);
//      d.setUTCMonth(mm);
//      d.setUTCDate(dd);
//      d.setUTCHours(h);
//      d.setUTCMinutes(m);
//      d.setUTCSeconds(s);
//      d.setUTCMilliseconds(0);
//    });
  return d;
}


function pad(s, len, c) {
  while(s.length < len)
    s = c + s;
  return s;
}

function save() {
  // todo: disable socks here
  console.log(JSON.stringify({what:"LOG", msg:"render tpl"}));
  var processed = page.evaluate("function() { " + absolutizeURI + onpage + "return onpage() }");

    var f = fs.open("index.html", "wb");
    f.write("<style type='text/css'>" + processed.css + "</style>");
    f.write(processed.body);
    f.close();

  var ventipng = savepng(); // todo: execute from within page.evaluate (via prompt2), to sync DOM and Image snapshots

  processed.requesterip = requesterIP;
  processed.requesteruseragent = requesterUserAgent;
  processed.tipchangeset = tipchangeset;
  processed.shortid = shortid;
  processed.cookie = cookietoload;

  processed.time = timeFromUrl(processed.url).getTime();

  processed.requestedurl = removeArchivePrefix(firstRequestedUrl);
  processed.url = removeArchivePrefix(processed.url);
  processed.ventipng = ventipng;
  if (firstSuccessfulResponse) { // undefined for urls like 'about://plugins'
    processed.mainhtml = {
      url         : firstSuccessfulResponse.url,
      status      : firstSuccessfulResponse.status,
      statustext  : firstSuccessfulResponse.statusText,
      headers     : firstSuccessfulResponse.headers,
      contenttype : firstSuccessfulResponse.contentType||'',
    };
    //if (processed.mainhtml.contenttype.match(/^text/)) // no need to save binary image here
    //  processed.mainhtml.text = btoa(resources[firstSuccessfulResponse.id].text)
  } else {
    console.log(JSON.stringify({what:"ERROR", code:18, message:"Invalid page", debug:"no mainhtml, maybe zero-length answer"}));
    phantom.exit(18);
  }
  var tpl = JSON.stringify(processed);

  var ventitpl = ventiUpload('tpl', tpl);
  assert(ventitpl, 'error save tpl to venti');

  console.log(JSON.stringify({what:"LOG", msg:"ventitpl=" + ventitpl}));


  // store fb cookies for the next sessions so they will not ask for login
  if (cookieFileName && loginAttempts>0) {
    try {
      // todo: use write totmp file then rename to about concurrent writes
      var f = fs.open(cookieFileName, "wb");
      f.write(JSON.stringify(phantom.cookies, null, " "));
      f.close();
    } catch (e) {
      console.log(JSON.stringify({what:"LOG", msg:"error saving cookies=" + e}));
    }
  }

  console.log(JSON.stringify({what:"ERROR", code:0, message:"Ok", debug:"Ok"}));
  phantom.exit(0);
}

var tumblrPageCounter = 30;
var facebookClicks = 30;
window.setInterval(function(){

  // expand lj comments
  if (count>0 /* do not click while page is loading */ && (isLivejournalUrl(page.url) || isDreamwidthUrl(page.url))) {
    page.evaluate(function onpage() {
      try {
        prompt2("log", "--------------------- lj!");
        var numclicks=0;
        for (var i=0; i<document.all.length && numclicks<100; i++) {
          var e = document.all[document.all.length-1-i];
          if (e.tagName=="A" &&
              !e.clicked &&
              e.innerText!=null &&
              e.innerText.toString().match(/^\s*(Expand|Ausklappen|Ð Ð°Ð·Ð²ÐµÑ€Ð½ÑƒÑ‚ÑŒ)\s*$/)) {
            prompt2("log", "--------------------- expand lj comment |" + e.innerText + "|");
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, e);
            e.dispatchEvent(evt);
            e.clicked = 1;
            numclicks += 1;
          }
        }
      } catch(e) {}
    });
  }

  // expand blogspot 'archivedate collapsed' if any
  if (isBlogspotUrl(page.url)) {
    page.evaluate(function onpage() {
      try {
                  prompt2("log", "--------------------- blogspot2!");
                  var numclicks=0;
                  for (var i=0; i<document.all.length && numclicks<5; i++) {
                    var e = document.all[i];
                    if (e.tagName=="LI" && e.className=="archivedate collapsed") {
                      for (var j = 0; j < e.childNodes.length; j++) {
                        var a = e.childNodes[j];
                        if (a.tagName == "A" && a.className=="toggle") {
                          prompt2("log", "--------------------- expand blogspot archive comment |" + a.innerText + "|");
                          var evt = document.createEvent("MouseEvents");
                          evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, a);
                          a.dispatchEvent(evt);
                          numclicks += 1;
                        }
                      }
                    }
                  }
      } catch(e) {}
    });
  }


  if (isVkUrl(page.url)) {
    // click on "Expand text..."
    page.evaluate(function onpage() {
      try {
        var numclicks=0;
        for (var i=0; i<document.all.length && numclicks<2; i++) {
          var e = document.all[i];
          if (e.tagName == "A" && !e.clicked && (e.innerText == "Expand text.." || e.innerText == "Expand text...")) {
            prompt2("log", "--------------------- expand vk |" + e.innerText + "|");
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, e);
            e.dispatchEvent(evt);
            e.clicked=1;
            numclicks+=1;
          }
        }
      } catch(e) {}
    });

    page.scrollPosition = { top: page.scrollPosition.top+500, left: 0 };
  }

  if (isImgurUrl(page.url) || isQuoraUrl(page.url) || isSexComUrl(page.url)) {
    page.scrollPosition = { top: page.scrollPosition.top+500, left: 0 };
  } else if (isTumblrUrl(page.url)) {
    if (--tumblrPageCounter >= 0)
      page.scrollPosition = { top: page.scrollPosition.top+5000, left: 0 };
  }

  if (isFacebookUrl(page.url)) {
    // click on "Show Older Stories"
    page.evaluate(function onpage() {
      try {
        for (var i=0; i<document.all.length; i++) {
          var e = document.all[i];
          if (e.tagName == "A" && (e.innerText == "Show Older Stories" || e.innerText == "See More Recent Stories" || e.innerText == "See More")) {
            if (--facebookClicks >= 0) {
              var evt = document.createEvent("MouseEvents");
              evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, e);
              e.dispatchEvent(evt);
            }
          }
        }
      } catch(e) {}
    });

    page.scrollPosition = { top: page.scrollPosition.top+250, left: 0 };
  }

  if (isTwitterUrl(page.url)) {
    // click on "View content". tc: https://twitter.com/hatano_yui/media
    page.evaluate(function onpage() {
      try {
        for (var i=0; i<document.all.length; i++) {
          var e = document.all[i];
          if (e.tagName == "BUTTON" && (e.innerText == "View content")) {
            prompt2("log", "--------------------- expand twitter |" + e.tagName + " " + e.innerText + "|");
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, e);
            e.dispatchEvent(evt);
          }
        }
      } catch(e) {}
    });
  }


}, 500);

page.onShouldInterruptJs = function () {
  console.log("onSIJ");
  return false;
}


// watchdog
window.setTimeout(function(){
  console.log(JSON.stringify({what:"ERROR", code:14, message:"Page too slow", debug:"180sec watchdog"}));
  phantom.exit(14);
}, 180000);

var iii=0;
window.setInterval(function(){ iii++; console.log("["+iii+"]"); }, 1000);

// there are never-finished pages (www.qq.com)
window.setTimeout(function() {
    console.log(JSON.stringify({what:"LOG", msg:"90sec elapsed, page still loading, make it end."}));

    if (page.url == "about:blank") {
      console.log(JSON.stringify({what:"ERROR", code:17, message:"Network error", debug:"page.url==about:blank after 90sec"}));
      //document.getElementById("status").outerHTML="<H1>Error 17</H1>"
      phantom.exit(17);
    } else {
      save();
    }

  }, 90000);



var count = 0;
var loginAttempts = 0;
page.onLoadFinished = function (status) {
    // Check for page load success
  console.log(JSON.stringify({what:"LOG", msg:"onLoadFinished count="+count}));
  if(count++ > 0) return;

  console.log(JSON.stringify({what:"LOG", msg:"ok " + status + "|" + page.url}));

  if (page.url == "about:blank") { // www.euronews.com returns status=="fail"
    console.log(JSON.stringify({what:"ERROR", code:15, message:"Network error", debug:"page.url==about:blank"}));
    phantom.exit(15);
    return;
  }

  var timeLoadFinished = milliseconds_current();
  function milliseconds_since_load_finished() { return milliseconds_current() - timeLoadFinished; }

  function maybesave() {
    console.log(JSON.stringify({what:"LOG", msg:"since_start=" + milliseconds_since_start() +
                                                " since_load_finished=" + milliseconds_since_load_finished() +
                                                " since_last_activity=" + milliseconds_since_last_activity() +
                                                " urlsinprogress=" + Object.keys(urlsinprogress).length}));
    // extend timeout if download is in progress
    if (milliseconds_since_last_activity() < 3000)
      window.setTimeout(maybesave, 1000);
    else if (Object.keys(urlsinprogress).length!=0 && milliseconds_since_last_activity() < 10000)
      window.setTimeout(maybesave, 1000);
    else if (page.viewportSize.height > 768) {
      page.viewportSize = { width: 1024, height: 768 };
      window.setTimeout(maybesave, 1000);
    } else if (isTwitterUrl(page.url) && page.scrollPosition.top < 50000) {
      page.scrollPosition = { top: page.scrollPosition.top+5000, left: 0 };
      window.setTimeout(maybesave, 1000);
    //} else if (isScribdUrl(page.url) && page.scrollPosition.top < 100000) {
    //  page.scrollPosition = { top: page.scrollPosition.top+500, left: 0 };
    //  window.setTimeout(maybesave, 200);
    } else {
      console.log(JSON.stringify({what:"LOG", msg:"ok after delay=============== " + status + "|" + page.url}));

      if(!isFacebookUrl(urltoload) && isFacebookUrl(page.url)) {
        // redirected to facebook, so retry in facebook mode (with proper user-agent and cookies)
        setFacebookMode();
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
        page.open(urltoload);
      } else if (loginAttempts++ >= 3) { // try 2 times
        save();

      } else if(isRedditOver18(page.url) && !isRedditOver18(urltoload) && // bypass adult content warning
                page.evaluate(function() {
                  var submit = null;
                  for(var i=0; i<document.all.length; i++) {
                    var e = document.all[i];
                    if ((e.tagName=="INPUT" && e.type=="BUTTON" || e.tagName=="BUTTON") && e.value=="yes") { submit = e; break; }
                  }
                  prompt2("log", "--------------------------- submit " + submit);
                  if (submit) {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished

      } else if (isRedditUrl(page.url)) {
        function redditExpandOne() {
          return page.evaluate(function() {
            for (var i=document.all.length-1; i>=0; i--) {
              var e = document.all[i];
              if (e.tagName=="A" && e.onclick!=null && e.clicked!=1) {
                if (e.onclick.toString().indexOf("showcomment(this)")!=-1 ||
                    e.innerText=="[+]" && e.onclick.toString().indexOf("togglecomment(this)")!=-1) { /* no ajax expand */
                  //prompt2("log", "--------------------------- expand no-ajax reddit comment");
                  var evt = document.createEvent("MouseEvents");
                  evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, e);
                  e.dispatchEvent(evt);
                  e.clicked=1;
                } else if (e.onclick.toString().indexOf("morechildren(this")!=-1) { /* ajax expand */
                  prompt2("log", "--------------------------- expand ajax reddit comment");
                  var evt = document.createEvent("MouseEvents");
                  evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, e);
                  e.dispatchEvent(evt);
                  e.clicked=1;
                  return true; // go sleep
                }
              }
            }
            return false; // go save
          });
        }

        function recur() {
          if (redditExpandOne()) {
            //console.log("+++500!");
            setTimeout(function() { recur() }, 50);
          } else {
            //console.log("+++save!");
            setTimeout(function() { save() }, 5000);
          }
        }
        recur();
        count = 0; // allow reenter onLoadFinished

      } else if((isBlogspotUrl(page.url) || isBlogspotUrl(urltoload)) && // bypass adult content warning (todo: this can be fixed on reading saved template)
                page.evaluate(function() {
                  var submit = null;
                  for(j=0; j<document.all.length && submit==null; j++) {
                    var f = document.all[j];
                    if (f.tagName=="A" && f.href.match(/guestAuth=/)) { submit = f; break; }
                    if (f.tagName=="IFRAME") {
                      for(i=0; i<f.contentDocument.all.length && submit==null; i++) {
                        var e = f.contentDocument.all[i];
                        if (e.tagName=="A" && e.href.match(/guestAuth=/)) { submit = e; break; }
                      }
                    }
                  }
                  prompt2("log", "--------------------------- submit " + submit);
                  if (submit) {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isSoupIo(page.url) &&
                page.evaluate(function() {
                  var submit = null;
                  for(j=0; j<document.all.length && submit==null; j++) {
                    var e = document.all[j];
                    if (e.tagName=="INPUT" && e.type=="submit" && e.value=="Yes, continue") { submit = e; break; }
                  }
                  prompt2("log", "--------------------------- submit " + submit);
                  if (submit) {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isFacebookUrl(page.url) &&
                page.evaluate(function() {
                  var email = document.getElementById('email');
                  var pass = document.getElementById('pass');
                  var persist_box = document.getElementById('persist_box');

                  prompt2("log", "--------------------------- email " + email);
                  prompt2("log", "--------------------------- pass " + pass);
                  var submit = null;
                  if (document.all+"" != 'undefined') { // `document.all` can be `undefined` (tc:https://m.facebook.com/browse/likes?id=10152438969722192&actorid=698992191)
                    for(i=0; i<document.all.length; i++) {
                      var e = document.all[i];
                      if (e.tagName=="INPUT") {
                        if (email==null && e.name=="email") email = e;
                        if (pass ==null && e.name=="pass" ) pass  = e;
                        if (e.type=="submit" && (e.value=="Log In" ||
                                                 e.name == "login" ||
                                                 e.parentNode && e.parentNode.id=="loginbutton")) { submit = e; break; }
                      }
                    }
                  }
                  prompt2("log", "--------------------------- submit " + submit);
                  if (email && submit) {
                    var logins = ["NathanSRabinovich@camfex.cz",
                                  "NathanZRabinovich@camfex.cz"
                                  //"NathanCRabinovich@camfex.cz", disabled
                                  //"NathanHRabinovich@camfex.cz", disabled
                                  //"NathanQRabinovich@camfex.cz", disabled
                                  //"NathanIRabinovich@camfex.cz" disabled
                                 ];
                    email.value = logins[Math.floor(Math.random()*logins.length)];
                    if (pass) pass.value = "mmaaqq" // "mmaattyy"
                    if(persist_box) persist_box.checked = "checked";
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isVkUrl(page.url) &&
                page.evaluate(function() {
                  var code = document.getElementById('code');
                  var submit = document.getElementById('validate_btn');
                  if (code != null && submit != null) {
                    code.value = "8924";
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  var email = document.getElementById('email') || document.getElementById('quick_email');
                  var pass = document.getElementById('pass') || document.getElementById('quick_pass');
                  prompt2("log", "--------------------------- email " + email);
                  prompt2("log", "--------------------------- pass " + pass);
                  var submit = null;
                  for(i=0; i<document.all.length; i++) {
                    var e = document.all[i];
                    if (e.tagName=="A"      && e.innerText.match(/^Log in/) && e.href.match(/^javascript/)) { submit = e; break; }
                    if (e.tagName=="BUTTON" && e.innerText.match(/^Log in/)) { submit = e; break; }
                  }
                  prompt2("log", "--------------------------- submit " + submit);
                  if (email && pass && submit) {
                    //email.value = "nathan.rabinovich@camfex.cz"
                    email.value = "+420775168924"
                    pass.value = "qqq1734ae334sdu"
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isMedscapeLoginUrl(page.url) &&
                page.evaluate(function() {
                  var email  = document.getElementById('userId');
                  var pass   = document.getElementById('password');
                  var submit = document.getElementById('loginbtn') || document.getElementById('submitpass');
                  prompt2("log", "--------------------------- email " + email);
                  prompt2("log", "--------------------------- pass " + pass);
                  prompt2("log", "--------------------------- submit " + submit);
                  if (pass && submit) {
                    if (email) email.value = "nrm@camfex.cz"
                    pass.value = "mmaass"
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isAdflyUrl(page.url) &&
                page.evaluate(function() {
                  var skipad = document.getElementById('skip_ad_button');
                  if (skipad) {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, skipad);
                    skipad.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isLivejournalLoginUrl(page.url) &&
                page.evaluate(function() {
                  var user = null, pass = null, remember = null, submit = null;
                  for(i=0; i<document.all.length; i++) {
                    var e = document.all[i];
                    if (e.tagName=="INPUT") { // it has different designs
                      if (e.id == "login_user"     || e.name == "user")         { user = e;     prompt2("log", "--------------------------- user     " + user);     }
                      if (e.id == "login_password" || e.name == "password")     { pass = e;     prompt2("log", "--------------------------- pass     " + pass);     }
                      if (e.id == "login-rememb"   || e.name == "remember_me")  { remember = e; prompt2("log", "--------------------------- remember " + remember); }
                      if (e.id == "loginlj_submit" || e.value == "Log in")      { submit = e;   prompt2("log", "--------------------------- submit   " + submit);   }
                    }
                  }
                  if (user && pass && submit) {
                    user.value = "rab1n0v1ch"
                    pass.value = "MEQIJDQLDJKi9LK"
                    if (remember) remember.checked = "checked";
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;}) ) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if((isLivejournalUrl(page.url) || isDreamwidthUrl(page.url)) && // bypass adult content warning (todo: this can be fixed on reading saved template)
                page.evaluate(function() {
                  var submit = null;
                  for(i=0; i<document.all.length; i++) {
                    var e = document.all[i];
                    if (e && (e.tagName=="INPUT" || e.tagName=="BUTTON") && e.type=="submit" && e.name=="adult_check") { submit = e; break; }
                  }
                  if (submit) {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(page.url=="http://www.forbes.com/fdc/welcome_mjx.shtml" &&
                page.evaluate(function() {
                  var submit = null;
                  for(i=0; i<document.all.length; i++) {
                    var e = document.all[i];
                    if (e && e.tagName=="A" && e.id=="continue_link") { submit = e; break; }
                  }
                  if (submit) {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isQuoraUrl(page.url) && // !!! well, not every quora url has login form, but almost all urls show more info to an authentificated user
                                        // !!! so, one have to arcrive urls like https://www.quora.com from time to time to keep being signed in
                                        // !!! or https://www.quora.com/login/index?next=http%3A%2F%2Fwww.quora.com%2FDoes-Jimmy-Wales-realize-that-people-can-see-he-asked-this-question
                page.evaluate(function() {
                  var user = null, pass = null, remember = null, submit = null;
                  for(i=0; i<document.all.length; i++) {
                    var e = document.all[i];
                    if (e.tagName=="INPUT") { // it has different designs
                      if (e.type=="text"     && e.name=="email")              { user = e;     user.value = "quora-nr@camfex.cz"; prompt2("log", "--------------------------- user     " + user);     }
                      if (e.type=="password" && e.name=="password")           { pass = e;     pass.value = "mmaaggss";           prompt2("log", "--------------------------- pass     " + pass);     }
                      if (e.type=="checkbox" && e.name=="allow_passwordless") { remember = e; remember.checked = "checked";      prompt2("log", "--------------------------- remember " + remember); }
                      if (e.type=="submit"   && e.value=="Log In")            { submit = e;                                      prompt2("log", "--------------------------- submit   " + submit);   }
                    }
                  }
                  if (user && pass && submit) {
                    //user.value = "quora-nr@camfex.cz"
                    //pass.value = "mmaaggss"
                    //if (remember) remember.checked = "checked";
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isLinkedinSignupUrl(page.url) &&
                page.evaluate(function() {
                  var submit = null;
                  for(i=0; i<document.all.length; i++) {
                    var e = document.all[i];
                    if (e.tagName=="A" && e.innerText.match(/^Sign in/)) { submit = e; break; }
                  }
                  prompt2("log", "--------------------------- submit " + submit);
                  if (submit) {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        loginAttempts--; // it was not a login attempt
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if(isLinkedinLoginUrl(page.url) &&
                page.evaluate(function() {
                  // <input type="text" name="session_key" value="" id="session_key-login" tabindex="1" data-ime-mode-disabled="">
                  // <input type="password" name="session_password" value="" id="session_password-login" tabindex="2">
                  // <input type="submit" name="signin" value="Sign In" tabindex="3" class="btn-primary" id="btn-primary">
                  var email  = document.getElementById('session_key-login');
                  var pass   = document.getElementById('session_password-login');
                  var submit = document.getElementById('btn-primary');
                  prompt2("log", "--------------------------- email " + email);
                  prompt2("log", "--------------------------- pass " + pass);
                  prompt2("log", "--------------------------- submit " + submit);
                  if (email && pass && submit) {
                    email.value = "linkedin@camfex.cz"
                    pass.value = "mmaass"
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished
      } else if (page.url != urltoload && page.url == "http://www.linkedin.com/nhome/?trk=") { // post login url
        count = 0; // allow reenter onLoadFinished
        page.open(urltoload);

      } else if(isTMGOnlineMediaUrl(page.url) &&
                page.evaluate(function() {
                  var submit = null;
                  for(i=0; i<document.all.length; i++) {
                    var e = document.all[i];
                    if (e.tagName=="INPUT") {
                      prompt2("log", "--------------------------- e" + e.type + e.className);
                      if (e.type=="submit" && e.className=="CookiesOK") { submit = e; break; }
                    }
                  }
                  prompt2("log", "--------------------------- submit " + submit);
                  if (submit) {
                    var evt = document.createEvent("MouseEvents");
                    evt.initMouseEvent("click", true, true, window, 1, 1, 1, 1, 1, false, false, false, false, 0, submit);
                    submit.dispatchEvent(evt);
                    return true;
                  }
                  return false;})) {
        //timeStart = milliseconds_current(); // reset timeout's base
        count = 0; // allow reenter onLoadFinished

      } else if (page.evaluate(function() {
                   if (document.all+"" != 'undefined') { // `document.all` can be `undefined` (tc:http://git.kitenet.net/?p=joey/home.git;a=blob;f=bin/s;h=83b6f46592e9f194eac0e5ac33288cb02a2914ba;hb=HEAD)
                     for(i=0; i<document.all.length; i++) {
                       var e = document.all[i];
                       if (e.tagName=="SPAN" && e.innerText.match(/^Checking your browser before accessing/)) {
                         return true;
                       }
                     }
                   }
                   return false;
                 })) {
        // console.log("cloudflare! wait more!");
        window.setTimeout(maybesave, 5000);
      } else {
        // console.log("save!! " + page.url);
        save();
      }
    }
  }
  // Execute some DOM inspection within the page context
  window.setTimeout(maybesave, timeout);
};


page.open(urltoload);
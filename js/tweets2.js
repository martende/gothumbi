

// code to be executed in browser context
function onpage() {

  var prompt2 = prompt;
  var transparent1x1gif = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

  function dataurlpixel(fillstyle) {
    var c=document/*any document ok?*/.createElement('canvas');
    c.width=1; c.height=1;
    var ctx=c.getContext('2d');
    ctx.fillStyle=fillstyle;
    ctx.fillRect(0,0,1,1);
    return c.toDataURL();
  }

  function AssertionError(message) {
     this.message = message;
  }
  if (typeof Error == "function") { // can be "object", tc: http://gifavs.com/g/470587
    AssertionError.prototype = new Error;
  }

  function assert(cond) {
    if(!cond) {
      var a = 'Assertion failed:';
      for(var i=1; i<arguments.length; i++)
        a+=' ' + arguments[i]
      throw new AssertionError(a);
    }
  }

  function addpx(v) { v=v+''; return v.match(/^[0-9.]+$/) ? v + 'px' : v; }
  function atr(s) {
    return "\"" + (s+"").replace(/[&"\x00-\x1F<>]/g, function(c) { return '&#' + c.charCodeAt(0) + ';'; }) + "\"";
  }
  function linkurl(s) {  // a=href, form=action, ..
    s = ''+s
    s = s.replace(/^http:\/\/\w+\.archive\.org\/web\/\d+\//, '');
    if(s.match(/^javascript:/)) s = '#';
    return s;
  }
  function txt(s) {
    return s.replace(/[<>\&]/g, function(c) { return '&#' + c.charCodeAt(0) + ';'; });
  }


  function expandstyle(css) { // Array(k, v, prio)
    var k = css[0], v = css[1];
    if (v == "")
      return [css];
    if (k=="border") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [
        ["border-top-width", vv[0]], ["border-right-width", vv[0]], ["border-bottom-width", vv[0]], ["border-left-width", vv[0]],
        ["border-top-style", vv[1]], ["border-right-style", vv[1]], ["border-bottom-style", vv[1]], ["border-left-style", vv[1]],
        ["border-top-color", vv[2]], ["border-right-color", vv[2]], ["border-bottom-color", vv[2]], ["border-left-color", vv[2]]
      ];
      if(vv.length==1) return [
        ["border-top-width", vv[0]], ["border-right-width", vv[0]], ["border-bottom-width", vv[0]], ["border-left-width", vv[0]],
        ["border-top-style", vv[0]], ["border-right-style", vv[0]], ["border-bottom-style", vv[0]], ["border-left-style", vv[0]],
        ["border-top-color", vv[0]], ["border-right-color", vv[0]], ["border-bottom-color", vv[0]], ["border-left-color", vv[0]]
      ];
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="border-top") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [["border-top-width", vv[0]], ["border-top-style", vv[1]], ["border-top-color", vv[2]]];
      if(vv.length==1) return [["border-top-width", vv[0]], ["border-top-style", vv[0]], ["border-top-color", vv[0]]]; // "inherit", "initial"
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="border-right") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [["border-right-width", vv[0]], ["border-right-style", vv[1]], ["border-right-color", vv[2]]];
      if(vv.length==1) return [["border-right-width", vv[0]], ["border-right-style", vv[0]], ["border-right-color", vv[0]]]; // "inherit", "initial"
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="border-bottom") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [["border-bottom-width", vv[0]], ["border-bottom-style", vv[1]], ["border-bottom-color", vv[2]]];
      if(vv.length==1) return [["border-bottom-width", vv[0]], ["border-bottom-style", vv[0]], ["border-bottom-color", vv[0]]]; // "inherit", "initial"
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="border-left") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==3) return [["border-left-width", vv[0]], ["border-left-style", vv[1]], ["border-left-color", vv[2]]];
      if(vv.length==1) return [["border-left-width", vv[0]], ["border-left-style", vv[0]], ["border-left-color", vv[0]]]; // "inherit", "initial"
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="border-width") {
      var vv = v.split(" ");
      if(vv.length==1) return [["border-top-width", vv[0]], ["border-right-width", vv[0]], ["border-bottom-width", vv[0]], ["border-left-width", vv[0]]];
      if(vv.length==2) return [["border-top-width", vv[0]], ["border-right-width", vv[1]], ["border-bottom-width", vv[0]], ["border-left-width", vv[1]]];
      if(vv.length==3) return [["border-top-width", vv[0]], ["border-right-width", vv[1]], ["border-bottom-width", vv[2]], ["border-left-width", vv[1]]];
      if(vv.length==4) return [["border-top-width", vv[0]], ["border-right-width", vv[1]], ["border-bottom-width", vv[2]], ["border-left-width", vv[3]]];
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="border-style") {
      var vv = v.split(" ");
      if(vv.length==1) return [["border-top-style", vv[0]], ["border-right-style", vv[0]], ["border-bottom-style", vv[0]], ["border-left-style", vv[0]]];
      if(vv.length==2) return [["border-top-style", vv[0]], ["border-right-style", vv[1]], ["border-bottom-style", vv[0]], ["border-left-style", vv[1]]];
      if(vv.length==3) return [["border-top-style", vv[0]], ["border-right-style", vv[1]], ["border-bottom-style", vv[2]], ["border-left-style", vv[1]]];
      if(vv.length==4) return [["border-top-style", vv[0]], ["border-right-style", vv[1]], ["border-bottom-style", vv[2]], ["border-left-style", vv[3]]];
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="border-color") {
      var vv = v.split(" "); // todo: color can be in form 'rgb(0, 0, 0)'
      if(vv.length==1) return [["border-top-color", vv[0]], ["border-right-color", vv[0]], ["border-bottom-color", vv[0]], ["border-left-color", vv[0]]];
      if(vv.length==2) return [["border-top-color", vv[0]], ["border-right-color", vv[1]], ["border-bottom-color", vv[0]], ["border-left-color", vv[1]]];
      if(vv.length==3) return [["border-top-color", vv[0]], ["border-right-color", vv[1]], ["border-bottom-color", vv[2]], ["border-left-color", vv[1]]];
      if(vv.length==4) return [["border-top-color", vv[0]], ["border-right-color", vv[1]], ["border-bottom-color", vv[2]], ["border-left-color", vv[3]]];
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="border-radius") {
      var vv, ww, tl, tr, br, bl;
      if(v.indexOf("/") >= 0) {
        var a = v.split("/");
        vv = a[0].split(" "), ww = a[1].split(" ")
      } else {
        vv = ww = v.split(" ");
      }
           if(vv.length==1) { tl = vv[0]; tr = vv[0]; br = vv[0]; bl = vv[0]; }
      else if(vv.length==2) { tl = vv[0]; tr = vv[1]; br = vv[0]; bl = vv[1]; }
      else if(vv.length==3) { tl = vv[0]; tr = vv[1]; br = vv[2]; bl = vv[1]; }
      else if(vv.length==4) { tl = vv[0]; tr = vv[1]; br = vv[2]; bl = vv[3]; }
      else {
        prompt2("log style drop " + k + " " + v);
        return [];
      }

           if(ww.length==1) { tl += " " + ww[0]; tr += " " + ww[0]; br += " " + ww[0]; bl += " " + ww[0]; }
      else if(ww.length==2) { tl += " " + ww[0]; tr += " " + ww[1]; br += " " + ww[0]; bl += " " + ww[1]; }
      else if(ww.length==3) { tl += " " + ww[0]; tr += " " + ww[1]; br += " " + ww[2]; bl += " " + ww[1]; }
      else if(ww.length==4) { tl += " " + ww[0]; tr += " " + ww[1]; br += " " + ww[2]; bl += " " + ww[3]; }
      else {
        prompt2("log style drop " + k + " " + v);
        return [];
      }

      return [["border-top-left-radius", tl],
              ["border-top-right-radius", tr],
              ["border-bottom-right-radius", br],
              ["border-bottom-left-radius", bl]];
    }
    if (k=="margin") {
      var vv = v.split(" ");
      if(vv.length==1) return [["margin-top", vv[0]], ["margin-right", vv[0]], ["margin-bottom", vv[0]], ["margin-left", vv[0]]];
      if(vv.length==2) return [["margin-top", vv[0]], ["margin-right", vv[1]], ["margin-bottom", vv[0]], ["margin-left", vv[1]]];
      if(vv.length==3) return [["margin-top", vv[0]], ["margin-right", vv[1]], ["margin-bottom", vv[2]], ["margin-left", vv[1]]];
      if(vv.length==4) return [["margin-top", vv[0]], ["margin-right", vv[1]], ["margin-bottom", vv[2]], ["margin-left", vv[3]]];
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="padding") {
      var vv = v.split(" ");
      if(vv.length==1) return [["padding-top", vv[0]], ["padding-right", vv[0]], ["padding-bottom", vv[0]], ["padding-left", vv[0]]];
      if(vv.length==2) return [["padding-top", vv[0]], ["padding-right", vv[1]], ["padding-bottom", vv[0]], ["padding-left", vv[1]]];
      if(vv.length==3) return [["padding-top", vv[0]], ["padding-right", vv[1]], ["padding-bottom", vv[2]], ["padding-left", vv[1]]];
      if(vv.length==4) return [["padding-top", vv[0]], ["padding-right", vv[1]], ["padding-bottom", vv[2]], ["padding-left", vv[3]]];
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="outline") {
      var vv = v.split(" ");
      if(vv.length==3) return [["outline-color", vv[0]], ["outline-style", vv[1]], ["outline-width", vv[2]]];
      if(vv.length==1) return [["outline-color", vv[0]], ["outline-style", vv[0]], ["outline-width", vv[0]]]; // initial, inherit
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="list-style") {
      var vv = v.split(" ");
      if(vv.length==3) return [["list-style-type", vv[0]], ["list-style-position", vv[1]], ["list-style-image", vv[2]]];
      if(vv.length==1) return [["list-style-type", vv[0]], ["list-style-position", vv[0]], ["list-style-image", vv[0]]]; // initial, inherit
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="background" && v=="inherit") {
      return [];
    }
    if (k=="background" && v!="initial") {
      assert(false, "to be parsed " + k + " " + v);
    }
    if (k=="background-position") {
      var vv = v.split(" ");
      if(vv.length==2) return [["background-position-x", vv[0]], ["background-position-y", vv[1]]];
      if(vv.length==1) return [["background-position-x", vv[0]], ["background-position-y", vv[0]]];
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    if (k=="background-repeat") {
      var vv = v.split(" ");
      if(vv.length==2) return [["background-repeat-x", vv[0]], ["background-repeat-y", vv[1]]];
      if(vv.length==1) {
        if(v=="repeat-x") return [["background-repeat-x", "repeat"], ["background-repeat-y", "no-repeat"]];
        if(v=="repeat-y") return [["background-repeat-x", "no-repeat"], ["background-repeat-y", "repeat"]];
        return [["background-repeat-x", v], ["background-repeat-y", v]];
      }
      prompt2("log style drop " + k + " " + v);
      return [];
    }
    return [css];
  }


  // after this, the order of styles should be unsignificant?
  function dedupstyle(css /* [[k, v, prio], ...] */) {
    var css2 = [];
    for(var i=0; i<css.length; i++)  if(!css[i][2]) css2 = css2.concat(expandstyle(css[i])); // not important
    for(var i=0; i<css.length; i++)  if( css[i][2]) css2 = css2.concat(expandstyle(css[i])); // important

    var seen = {};
    for(var i=0; i<css2.length; i++) {
      var k = css2[i][0];
      if(k in seen)
        css2[seen[k]] = null;
      seen[k] = i;
    }

    var css4 = [];
    for(var i=0; i<css2.length; i++)
      if(css2[i] != null && css2[i][1]!='' /*empty value*/)
        css4.push(css2[i]);

    return css4;
  }


  // optional step, reducing length of style="" sring and making it firefox compatible
  function reducestyle(cs, styles) {
    var hash = {};
    for(var i=0; i<styles.length; i++) {
      var k = styles[i][0], v = styles[i][1];
      assert(!(k in hash), "!!! dup " + k + " " + v);
      hash[k] = v;
    }

    var allmargins = ("margin-top" in hash) && ("margin-right" in hash) && ("margin-bottom" in hash) && ("margin-left" in hash);
    var allpaddings = ("padding-top" in hash) && ("padding-right" in hash) && ("padding-bottom" in hash) && ("padding-left" in hash);
    var allbordercolor = ("border-top-color" in hash) && ("border-right-color" in hash) && ("border-bottom-color" in hash) && ("border-left-color" in hash);
    var allliststyle = ("list-style-type" in hash) && ("list-style-position" in hash) && ("list-style-image" in hash);

    //var bgcoloroverride = false;
    var css = "";
    for(var i=0; i<styles.length; i++) {
      var k = styles[i][0], v = styles[i][1];

      if(k=="background-repeat-x" || k=="background-repeat-y" || k=="background-repeat" ||
         k=="background-position-x" || k=="background-position-y" || k=="background-position" ||
         k=="background-attachment" || // always 'scroll', not 'fixed' (at least for <body>), tc:http://www.w3.org/TR/html4/present/graphics.html
         k=="border-top-width" || k=="border-right-width" || k=="border-bottom-width" || k=="border-left-width" ||
         k=="border-top-style" || k=="border-right-style" || k=="border-bottom-style" || k=="border-left-style" ||
         allmargins && (k=="margin-top" || k=="margin-right" || k=="margin-bottom" || k=="margin-left") ||
         allpaddings && (k=="padding-top" || k=="padding-right" || k=="padding-bottom" || k=="padding-left") ||
         allbordercolor && (k=="border-top-color" || k=="border-right-color" || k=="border-bottom-color" || k=="border-left-color") ||
         allliststyle && (k=="list-style-type" || k=="list-style-position" || k=="list-style-image") ||
         k=="outline-color" || k=="outline-style" || k=="outline-width" ) {
      } else if(v=="initial" && (k=="background-image" || k=="background-attachment" || k=="background-origin" || k=="background-clip" ||
                                 k=='border-left-color' || k=='border-right-color' || k=='border-top-color' || k=='border-bottom-color')) {
         // not inheritable, initial is the same as absense
      } else if(k=="-webkit-user-select" || k.indexOf("-webkit-transition")==0) {
         // forget it
      } else if (k=="box-sizing") {
        css += "box-sizing: " + v + "; ";
        css += "-moz-box-sizing: " + v + "; ";  // tc:css-tricks.com
        css += "-ms-box-sizing: " + v + "; ";
      } else if (k=="background-color") {
        css += 'background-color: ' + (v=='initial' ? 'transparent' : v) + "; "; // <input> has 'white' default, so 'transparent' is needed
      } else if (k=="background-image" || k=="list-style-image" || k=="cursor" || k=="-webkit-border-image" || k=="list-style-image") {
        if(v.match(/-webkit-/)) {
          var w = /*e ? e.offsetWidth  :*/ Math.max(parseInt(cs.getPropertyValue('width')), 8); // at least 8x8
          var h = /*e ? e.offsetHeight :*/ Math.max(parseInt(cs.getPropertyValue('height')), 8);
          var renderhtml = '<div style="width:' + addpx(w) + ';'+
                                      'height:' + addpx(h) + ';'+
                             'background-clip:' + cs.getPropertyValue('background-clip') + ';'+
                           'background-origin:' + cs.getPropertyValue('background-origin') + ';'+
                           'background-repeat:' + cs.getPropertyValue('background-repeat') + ';'+
                         'background-position:' + cs.getPropertyValue('background-position') + ';'+
                            'background-image:' + v + '"></div>';
          if(k=="background-image") // stretch image??
            css += 'background-size: 100% 105%;'; // y=105% for opera to avoid cycle
          v = 'url(' + prompt2('renderme ' + renderhtml) + ')';
        } else {
          v = v.replace(/url\(([^)]+)\)/g, function(m, url) {
            return 'url(' + (prompt2('ventiurl ' +url) || transparent1x1gif) + ')';
          });
        }
        css += k + ':' + v + ';';
        if(k.match(/^-webkit-/)) {
          css += k.replace(/^-webkit-/, '-moz-') + ':' + v + ';';
          css += k.replace(/^-webkit-/, '-o-') + ':' + v + ';';
        }
      } else if(k.indexOf("-webkit-transform")==0) { // tc: maps.bing.com
        css += k.replace(/^-webkit-/, '-moz-') + ':' + v + ';';
        css += k.replace(/^-webkit-/, '-ms-') + ':' + v + ';';
        css += k.replace(/^-webkit-/, '-o-') + ':' + v + ';';
        css += k + ':' + v + ';';
      } else {
        css += k + ':' + v + ';';
      }
    }

    assert(!("background-repeat" in hash), "!!! background-repeat");
    assert(!("background-position" in hash), "!!! background-position");

    if (("background-position-x" in hash) || ("background-position-y" in hash)) {
      var x = hash["background-position-x"] || "0px"; if(x=="initial") x="0px";
      var y = hash["background-position-y"] || "0px"; if(y=="initial") y="0px";
      if (!(x == '0px' && y == '0px') && !x.match(/[ ,]/) && !y.match(/[ ,]/))
        css += "background-position: " + x + " " + y + "; ";
    }

    if (("background-repeat-x" in hash) || ("background-repeat-y" in hash)) {
      var x = (hash["background-repeat-x"] || "initial").split(',')[0];
      var y = (hash["background-repeat-y"] || "initial").split(',')[0];
      if(x=="initial" && y=="initial") {} //css += "background-repeat: repeat; ";
      else if(x==y)                           css += "background-repeat: " + x + "; ";
      else if(x=="repeat" && y=="no-repeat") css += "background-repeat: repeat-x; ";
      else if(x=="no-repeat" && y=="repeat") css += "background-repeat: repeat-y; ";
      else assert(false, " background-repeat x=" + x + " y=" + y);
    }

    if (("outline-color" in hash) || ("outline-style" in hash) || ("outline-width" in hash)) {
      var color = hash["outline-color"] || "invert"; if(color =="initial") color ="invert";
      var style = hash["outline-style"] || "none";   if(style =="initial") style ="none";
      var width = hash["outline-width"] || "medium"; if(width =="initial") width ="medium";
      css +=  "outline: " + color + " " + style + " " + width + "; "
    }

    if (allliststyle) {
      var t = hash["list-style-type"]     ; if(t =="initial") t ="disc";
      var p = hash["list-style-position"] ; if(p =="initial") p ="outside";
      var i = hash["list-style-image"]    ; if(i =="initial") i ="none";
      css +=  "list-style: " + t + " " + p + " " + i + "; "
    }

    var xx = [["border-", "-width", "border-width", "medium"],
              ["border-", "-style", "border-style", "none"]]; // not specified by standard
    if (allmargins) xx.push(["margin-", "", "margin", "0px"]);
    if (allpaddings) xx.push(["padding-", "", "padding", "0px"]);
    if (allbordercolor) xx.push(["border-", "-color", "border-color", "white"]); // none? initial? white?
    for(var i=0; i<xx.length; i++) {
      var w = xx[i][0], z = xx[i][1], style = xx[i][2], def = xx[i][3];
      if(((w+"top"+z) in hash) || ((w+"right"+z) in hash) || ((w+"bottom"+z) in hash) || ((w+"left"+z) in hash)) {
        var t = hash[w+"top"   +z] || def; if(t=="initial") t=def;
        var r = hash[w+"right" +z] || def; if(r=="initial") r=def;
        var b = hash[w+"bottom"+z] || def; if(b=="initial") b=def;
        var l = hash[w+"left"  +z] || def; if(l=="initial") l=def;
             if(r!=l) css += style + ": " + t + " " + r + " " + b + " " + l + "; "
        else if(t!=b) css += style + ": " + t + " " + r + " " + b + "; "
        else if(t!=r) css += style + ": " + t + " " + r + "; "
        else          css += style + ": " + t + "; "
      }
    }
    return css;
  }

  function fixstyle(e, cs, css1) {
    // http://maps.yandex.ru
    // http://ya.ru
    // https://github.com/gildas-lormeau/SingleFile/blob/master/WebContent/core/scripts/common/docprocessor.js
    for(var i=0; i<css1.length; i++) {
      // tc:http://www.chartsinfrance.net/Mylene-Farmer/news-67938.html, http://www.cuni.cz/IFORUM-2430.html
      if(typeof(css1[i][1]) != "string")
        css1[i][1] = "";

      css1[i][1] = css1[i][1].replace(/^\s+|\s+$/g, "") // trim space in css value (it happens)
      if(css1[i][0]=='position' && css1[i][1]=='fixed') {
        css1[i][1] = 'absolute';
      }
      if (css1[i][0]=='height' && css1[i][1].indexOf('%')>=0) {
        // min-height would be more logical here, but it does not work (tc:ya.ru)
        css1[i][1] = addpx(e ? e.offsetHeight : cs.getPropertyValue("height")); // in pixels instead of %
      }
    }
    return reducestyle(cs, dedupstyle(css1));
  }


  // fake span for ::before and ::after
  function mkspan2(cs, cssarr) {
    if(cssarr.length==0)
      return "";
    var content = "", css = []; //, styles = cssstr.split("; "); // todo: mind ''
    for(var i=0; i<cssarr.length; i++) {
      var k = cssarr[i][0], v = cssarr[i][1];
      if (k=='content') {
        if(v == 'none' || v == 'normal') {
        } else if(v == 'open-quote' || v == 'close-quote' || v == 'no-open-quote' || v == 'no-close-quote') {
          // todo? tc:http://www.samediggi.no/?AId=3158&back=1&MId1=2450&MId2=2522&MId3=&
        } else if(v.length>=2 && v[0]==v[v.length-1] && (v[0]=="'" || v[0]=='"')) {
          content = txt(v.substring(1, v.length-1));
        } else {
          content = v.replace(/url\(([^)]+)\)/g, function(m, url) {
            return '<img src=' + atr(prompt2('ventiurl ' +url) || transparent1x1gif) + '>';
          });
        } // else 'content: inherit', 'content: -webkit-gradient'...
        // http://www.w3.org/community/webed/wiki/CSS/Properties/content
        // tc:http://trac.webkit.org/browser/trunk/LayoutTests//fast/gradients/generated-gradients.html?format=raw
      } else {
        css.push([k, v]);
      }
    }
    return "<span style=" + atr(fixstyle(null, cs, css)) + ">" + content + "</span>";
  }


  function processdoc(doc, htmlstyle) {
    if(doc.toString() == '[object SVGDocument]') {
      function svg2xml(a) { // todo: <image xlink:href=
        if(a == "[object Comment]") {
          return '';
        } else if(a == "[object Text]") {
          return txt(a.data);
        } else if(a == "[object CDATASection]") {
          return "<![CDATA[" + txt(a.data) + "]]>";
        } else {
          var o = '<' + a.tagName;
          for(var i=0; i<a.attributes.length; i++)
            o += ' ' + a.attributes[i].name + '=' + atr(a.attributes[i].value);
          o += '>';
          for(var i=0; i<a.childNodes.length; i++)
            o += svg2xml(a.childNodes[i]);
          return o + '</' + a.tagName + '>';
        }
      }

      assert(doc.rootElement != null, "doc.rootElement == null, malformed svg")
      var csroot = doc.defaultView.getComputedStyle(doc.rootElement);
      return {url:        doc.URL,
              doctype:    '',
              title:      (''+doc.title),
              css:        '',
              compatmode: (''+doc.compatMode),
              body:       '<div class="svgbody"' +
                                 ' style="width:' + addpx(csroot.getPropertyValue("width")) + ';' +
                                        'height:' + addpx(csroot.getPropertyValue("height")) + ';' +
                          '">' + svg2xml(doc.rootElement) + '</div>'};
    }

    assert(doc.documentElement != null, "doc.documentElement == null");
    assert(typeof(doc.documentElement.tagName)=="string", "typeof(doc.documentElement.tagName)=" + typeof(doc.documentElement.tagName)); // http://www.esc-history.com/
    assert(typeof(doc.URL)                    =="string", "typeof(doc.URL)="                     + typeof(doc.URL));                     // http://creaturesfrance.free.fr/

    if (doc.documentElement.tagName.toLowerCase() == "feed" && doc.documentElement.toString() == "[object Element]") {
      assert(false, "Atom not supported"); // todo: Atom
    } else if (doc.documentElement.tagName.toLowerCase() == "rss" && doc.documentElement.toString() == "[object Element]") {
      assert(false, "RSS not supported"); // todo: RSS
    } else if (doc.documentElement.tagName.toLowerCase() == "html" && doc.documentElement.toString() == "[object HTMLHtmlElement]") {
      // ok
    } else if (doc.documentElement.tagName.toLowerCase() == "svg"  && doc.documentElement.toString() == "[object SVGSVGElement]") {
      // ok
      // it differs from svg case above, doc.toString()=="[object Document]"
    } else {
      assert(false, "doc.documentElement.tagName=" + doc.documentElement.tagName + " doc.documentElement.toString()=" + doc.documentElement.toString());
    }

    var wmipp = doc.getElementById('wm-ipp');  // hide wayback machine toolbar
    if (wmipp && wmipp.tagName=='DIV') {
      wmipp.style.display = 'none';
    }


    var cshtml = doc.defaultView.getComputedStyle(doc.documentElement /*<html> <svg>*/);
    assert(cshtml != null,  "cshtml == null");
    assert(cshtml.toString() == "[object CSSStyleDeclaration]", "wrong cshtml: " + cshtml.toString());
    var csbody = doc.body ? doc.defaultView.getComputedStyle(doc.body) : null;
    var usedbodycolor = csbody && cshtml.getPropertyValue("background-color") == 'rgba(0, 0, 0, 0)';
    // body color should not hide image moved from <body> to <html>? http://www.linkdex.com/about/features/
    var usedbodyimage = usedbodycolor && cshtml.getPropertyValue("background-image") == 'none';

    function processe(e) { // [html, css]

//prompt2("log", "pe " + e.tagName + " " + e);
      function cssfor(pseudo) {
        var css = [];
        var c = doc.defaultView.getMatchedCSSRules(e, pseudo);
        if (c!=null) {
          for(var i=0; i<c.length; i++) {
            var style = c[i].style; // CSSStyleDeclaration
            for(var j=0; j<style.length; j++)
              css.push([style[j], style.getPropertyValue(style[j]), style.getPropertyPriority(style[j])]);
          }
        }
        return css;
      }

      // e.style==null for a strange element with e.tagName=='result' and e.toString=="[object Element]"
      // tc(unstable): http://www.collider.com/2010/03/10/christopher-nolan-speaks-updates-on-dark-knight-sequel-and-superman-man-of-steel/
      if(e == "[object Text]" || e == "[object Comment]" || e.style==null || typeof(e.tagName)!="string")
        return {html:"", css:""};

      var tag = e.tagName.toLowerCase();

      if(tag == "script" || tag == "noscript" || tag == "noframes" || tag == "audio" ||
         tag == "title" || tag == "style" || tag == "link" || tag == "param") {
        return {html:"", css:""};
      }

      var cs = doc.defaultView.getComputedStyle(e);
      // do not emit invisible items (this will remove '<input type="hidden"'> as well)
//      if(cs.getPropertyValue("display") == "none") {
//        return {html:"", css:""};
//      }
      if(doc.URL.match(/^http:\/\/\w+\.archive\.org\/\d+\//) &&
         tag=='div' && (e.id=='wm-ipp' || e.id=='positionHome')) { // wayback machine toolbar or iframe with error
        return {html:"", css:""};
      }

      var css1 = cssfor('');
      if(e.style.toString() == "[object CSSStyleDeclaration]") { // may be something else, it happens
        for(var j=0; j<e.style.length; j++) {
          if(typeof(e.style.getPropertyValue) == "function" && typeof(e.style.getPropertyPriority) == "function") { // this happens too, check outside cycle doesnt help
            css1.push([e.style[j], e.style.getPropertyValue(e.style[j]), e.style.getPropertyPriority(e.style[j])]);
          }
        }
      } else {
        prompt2("log "+  e.style.toString() + " instead of [object CSSStyleDeclaration]")
      }
      if(cs.getPropertyValue("text-align")=='-webkit-auto')
        css1 = [['text-align',    cs.getPropertyValue("direction")=='rtl' ? 'right' : 'left']].concat(css1);

      var needCloseTag = true;
      var o = "<" + tag;
      var eltcss = '';

      if (e.toString().match(/^\[object SVG/)) {
        for(var i=0; i<e.attributes.length; i++) {
          var k = e.attributes[i].name, v = e.attributes[i].value;
          if(e.tagName.toLowerCase()=='image' && k=='href')
            k = 'xlink:href';
          if(e.tagName.toLowerCase()=='image' && k=='xlink:href')
            v = prompt2('ventiurl ' + absolutizeURI(doc.baseURI, v)) || transparent1x1gif;
          if(k!='style')
            o += ' ' + k + '=' + atr(v);
        }
      }
      else if (tag == "html") {
        css1 = css1.concat([['overflow-x', 'visible', 'important'],
                            ['overflow-y', 'visible', 'important'],
                            ['height',              '', 'important'],
                            ['background-color',    '', 'important'],  // will be used on html1
                            ['background-size',     '', 'important'],
                            ['background-position', '', 'important'],
                            ['background-image',    '', 'important'],
                            ['background-repeat',   '', 'important']]);
        o = '<div class="html"';
        tag = "div";
      }
      else if (tag == "body") {
        css1 = [["vertical-align", "bottom"],
                ['min-height',    cs.getPropertyValue("height")       ],
                ['color',         cs.getPropertyValue('color')        ], // text=
                ['margin-left',   cs.getPropertyValue('margin-left'  )], // leftmargin=, marginwidth=
                ['margin-top',    cs.getPropertyValue('margin-top'   )], // topmargin=, marginheight=
                ['margin-right',  cs.getPropertyValue('margin-right' )], // rightmargin=, marginwidth=
                ['margin-bottom', cs.getPropertyValue('margin-bottom')]] // bottommargin=, marginheight=
          .concat(css1)
          .concat([['overflow-x', 'visible', 'important'],
                   ['overflow-y', 'visible', 'important'],
                   ['height',     '',        'important']]);

        if (usedbodycolor)
          css1 = css1.concat([['background-color', '', 'important']]);

        if (usedbodyimage)  // <body>'s image used for html1, should not be used twice
          css1 = css1.concat([['background-size',     '', 'important'],
                              ['background-position', '', 'important'],
                              ['background-image',    '', 'important'],
                              ['background-repeat',   '', 'important']]);

        o = '<div class="body"';
        tag = "div";
      }
      else if (tag == "frameset") {
        var border = e.getAttribute('border') || e.getAttribute('framespacing') || '6';
        if(!border.match(/^\d+$/)) border='0';

        var o = '<table class="frameset" border="0" cellpadding="0" cellspacing="0">';
        var rows = (e.rows||'').indexOf(',')>0;

        if (!rows) o += '<tr>';
        var prevch = null;
        for (var i = 0; i < e.childNodes.length; i++) {
          var ch = e.childNodes[i];

          // <frameset> recureses, <frame> be converted to <div>
          // no else meaningful inside frameset
          if (ch.tagName == "FRAMESET" || ch.tagName == "FRAME") {
            if(prevch) {
              var border2 = border;
              var fb1 = prevch.tagName != "FRAME" || prevch.frameBorder==='0';
              var fb2 = ch.tagName != "FRAME" || ch.frameBorder==='0';
              if(fb2 && fb1) border2 = '0';

              if(border2!='0') {
                if (rows) o += '<tr>';
                o += '<td style="background-color: silver;">';
                o += '<div style="width: ' + border2 + 'px; height: ' + border2 + 'px;"></div>';
                o += '</td>\n';
                if (rows) o += '</tr>';
              }
            }
            if (rows) o += '<tr>';
            var child = processe(ch);
            o += '<td style="vertical-align:top">' + child.html + '</td>';
            eltcss += child.css;
            if (rows) o += '</tr>';
            prevch = ch;
          }
        }
        if (!rows) o += '</tr>';
        o += '</table>';
        return {html: o, css: eltcss};
      }
      else if (tag == "frame" || tag == "iframe") { // todo: mind position:fixed -> position:absolute
        var noscroll = (e.scrolling||'').toLowerCase()=='no' || e.scrolling=='0';
        css1 = [['display', 'inline-block'], ['vertical-align', 'bottom'],
                ['width',  e.width  ? addpx(e.width)  : cs.getPropertyValue("width")], // always
                ['height', e.height ? addpx(e.height) : cs.getPropertyValue("height")],
                ['margin-left',   cs.getPropertyValue('margin-left'  )], // leftmargin=, marginwidth=
                ['margin-top',    cs.getPropertyValue('margin-top'   )], // topmargin=, marginheight=
                ['margin-right',  cs.getPropertyValue('margin-right' )], // rightmargin=, marginwidth=
                ['margin-bottom', cs.getPropertyValue('margin-bottom')], // bottommargin=, marginheight=
                ['overflow-x', noscroll ? 'hidden' : 'auto'],
                ['overflow-y', noscroll ? 'hidden' : 'auto']].concat(css1);
        if(tag=='frame') // <frameset> takes care on <frame>'s border
          css1 = [['border-width', '0']].concat(css1);
        else
          css1 = [['border-style', 'inset'],
                  ['border-width', addpx(e.frameBorder=='0' ? '0' : '2') + ';']].concat(css1);
        if (e.contentDocument == null || e.contentDocument.documentElement == null)
          return {html: '', css: eltcss};
        try { // ignore asserts. todo: it does no work, assert causes phantom.exit(12);
          var processed = processdoc(e.contentDocument, fixstyle(e, cs, css1) );
          return {html: processed.body, css: eltcss + processed.css};
        } catch(e) {
          return {html: '', css: eltcss};
        }
      }
      else if (tag == "meta") {
        needCloseTag = false;
        // no need to store <meta content="text/html; charset=utf-8" http-equiv="content-type">, it will be utf8 anyway
        if (e.httpEquiv && e.httpEquiv.toLowerCase() == "content-type")
          return {html: '', css: ''};
        if (!e.name || !(e.name.toLowerCase() == "description" || e.name.toLowerCase() == "keywords")) // store only "name" and "keywords"
          return {html: '', css: ''};
        if (e.name     ) o += " name="       + atr(e.name);
        if (e.httpEquiv) o += " http-equiv=" + atr(e.httpEquiv);
        if (e.content  ) o += " content="    + atr(e.content);
      }
      else if (tag == "textarea") {
        if (e.cols  != 20) o += " cols="         + atr(e.cols);
        if (e.rows   != 2) o += " rows="         + atr(e.rows);
        if (e.placeholder) o += " placeholder="  + atr(e.placeholder); // type=text, html5
        if (e.name       ) o += " name="         + atr(e.name);
        if (e.disabled   ) o += ' disabled="disabled"';
        if (e.readOnly   ) o += ' readonly="readonly"';
      }
      else if (tag == "label") {
        if (e.htmlFor) o += " for=" + atr(e.htmlFor);
      }
      else if (tag == "object" || tag == "applet" || tag == "video" || tag == "embed") {
        o = '<div class=' + atr(tag) + '';
        tag = "div";
        css1 = [['background-color',  'rgba(224, 224, 224, 0.5)'],
                ['display',           'inline-block'],
                ['margin',            '0'],
                ['padding',           '0'],
                ['border-width',      '0'],
                ['width',             addpx(e.width || 300)],
                ['height',            addpx(e.height || 150)]].concat(css1);
      }
      else if (tag == "canvas") {
        needCloseTag = false; // for <img>
        o = '<img src=' + atr(tag=='canvas' ? prompt2('ventiurl '+ e.toDataURL()) : transparent1x1gif);
        css1 = [['border-width', '0'],
                ['width',  addpx(e.width || 300)],
                ['height', addpx(e.height || 150)]].concat(css1);
      }
      else if (tag == "form") {
        if (e.acceptCharset) o += " accept-charset=" + atr(e.acceptCharset);
        if (e.action       ) o += " action="         + atr(linkurl(e.action));
        if (e.method       ) o += " method="         + atr(e.method);
        if (e.target       ) o += " target="         + atr(e.target);
        if (e.encoding != "application/x-www-form-urlencoded")
                             o += " enctype="        + atr(e.encoding);
      }
      else if (tag == "fieldset") {
        if (e.disabled) o += ' disabled="disabled"';
      }
      else if (tag == "del" || tag == "ins") {
        if (e.cite    ) o += " cite="     + atr(linkurl(e.cite));
        if (e.datetime) o += " datetime=" + atr(e.datetime);
      }
      else if (tag == "blockquote" || tag == "q") {
        if (e.cite    ) o += " cite="     + atr(linkurl(e.cite));
      }
      else if (tag == "button") {
        if (e.type != "submit") o += " type="   + atr(e.type);
        if (e.disabled        ) o += ' disabled="disabled"';
        if (e.name            ) o += " name="   + atr(e.name);
        if (e.value           ) o += " value="  + atr(e.value);
      }
      else if (tag == "input") {
        needCloseTag = false;
        if (e.type                   != "text") o += " type="         + atr(e.type);
        if (e.accept                          ) o += " accept="       + atr(e.accept); // type=file
        if (e.placeholder                     ) o += " placeholder="  + atr(e.placeholder); // type=text, html5
        if (e.getAttribute('autosave') != null) o += " autosave="     + atr(e.getAttribute('autosave')); // tc:vaadin.com
        if (e.getAttribute('results')  != null) o += " results="      + atr(e.getAttribute('results'));
        if (e.maxLength               < 500000) o += " maxlength="    + atr(e.maxLength);
        if (e.name                            ) o += " name="         + atr(e.name);
        if (e.size                       != 20) o += " size="         + atr(e.size);
        if (e.src                             ) o += " src="          + atr(prompt2('ventiurl '+ e.src) || transparent1x1gif);
        if (e.checked                         ) o += ' checked="checked"';
        if (e.disabled                        ) o += ' disabled="disabled"';
        if (e.readOnly                        ) o += ' readonly="readonly"';
        o += " value=" + atr(e.value); // <input type="submit" value="">, empty value must be present
      }
      else if (tag == "img") {
        needCloseTag = false;
        if (e.alt     ) o += " alt="      + atr(e.alt);
        if (e.isMap   ) o += ' ismap="ismap"';
        if (e.longDesc) o += " longdesc=" + atr(linkurl(e.longDesc));
        if (e.lowsrc  ) o += " lowsrc="   + atr(prompt2('ventiurl '+ e.lowsrc) || transparent1x1gif);
        if (e.name    ) o += " name="     + atr(e.name);
        if (e.src     ) o += " src="      + atr(prompt2('ventiurl '+ e.src) || transparent1x1gif);
        if (e.useMap  ) o += " usemap="   + atr(e.useMap);
        if (e.getAttribute('vspace')  != null) css1 = [["margin-top", addpx(e.vspace)],
                                                       ["margin-bottom", addpx(e.vspace)]].concat(css1);
        if (e.getAttribute('hspace')  != null) css1 = [["margin-left", addpx(e.hspace)],
                                                       ["margin-right", addpx(e.hspace)]].concat(css1);
        if (e.getAttribute('border')  != null) css1 = [["border-width", addpx(e.border)]].concat(css1);
        // height="0" must go to css, it is not the same as height absense (tc: http://www.render.ru/books/show_book.php?book_id=1622)
        if (e.getAttribute('width')   != null) css1 = [["width", addpx(e.width)]].concat(css1);
        if (e.getAttribute('height')  != null) css1 = [["height", addpx(e.height)]].concat(css1);
      }
      else if (tag == "hr") {
        needCloseTag = false;
        if (e.noshade) o += ' noshade="noshade"';
        if (e.getAttribute('width') != null) o += " width=" + atr(e.width);
        if (e.getAttribute('size')  != null) o += " size=" + atr(e.size);
      }
      else if (tag == "br") {
        needCloseTag = false;
        if (e.clear) o += " clear=" + atr(e.clear);
      }
      else if (tag == "font" || tag == "basefont") {
        if (e.size ) o += " size=" + atr(e.size);
        if (e.color) o += " color=" + atr(e.color);
        if (e.face ) o += " face=" + atr(e.face);
      }
      else if (tag == "marquee") {
        if (e.bgColor) css1 = [["background-color", e.bgColor]].concat(css1);
      }
      else if (tag == "li") {
        if (e.value) o += " value=" + atr(e.value);
        if (e.type  ) css1 = [["list-style-type", e.type]].concat(css1); // 1|a|A|i|I|disc|circle|square
      }
      else if (tag == "ol") {
        if (e.reversed  ) o += ' reversed="reversed"';
        if (e.start != 1) o += " start=" + atr(e.start);
      }
      else if (tag == "ul") {
        if (e.type  ) css1 = [["list-style-type", e.type]].concat(css1); // disc|circle|square
      }
      else if (tag == "select") {
        if (e.disabled ) o += ' disabled="disabled"';
        if (e.multiple ) o += ' multiple="multiple"';
        if (e.size != 0) o += " size=" + atr(e.size);
      }
      else if (tag == "option") {
        if (e.disabled) o += ' disabled="disabled"';
        if (e.selected) o += ' selected="selected"';
        if (e.label   ) o += " label=" + atr(e.label);
        if (e.value   ) o += " value=" + atr(e.value);
      }
      else if (tag == "a") {
        if (e.href    ) o += " href="     + atr(linkurl(e.href));
        if (e.hreflang) o += " hreflang=" + atr(e.hreflang);
        if (e.name    ) o += " name="     + atr(e.name); // html4
        if (e.target  ) o += " target="   + atr(e.target);
        if (e.type    ) o += " type="     + atr(e.type); // media type
        if (doc.body && doc.body.link)  css1 = [['color', cs.getPropertyValue("color")]].concat(css1);
      }
      else if (tag == "area") {
        if (e.alt     ) o += " alt="     + atr(e.alt);
        if (e.coords  ) o += " coords="  + atr(e.coords);
        if (e.href    ) o += " href="    + atr(linkurl(e.href));
        if (e.shape   ) o += " shape="   + atr(e.shape);  // default|rect|circle|poly
        if (e.target  ) o += " target="  + atr(e.target);
      }
      else if (tag == "optgroup") {                 //  todo: HTMLOptGroupElement
        if (e.disabled) o += ' disabled="disabled"';
        if (e.label   ) o += " label=" + atr(e.label);
      }
      else if (tag == "menu") {
        if (e.type ) o += " type="   + atr(e.type);
        if (e.label) o += " label="  + atr(e.label);
      }
      else if (tag == "table") {
        if (e.frame       ) o += " frame="       + atr(e.frame);    // Not supported in HTML5
        if (e.rules       ) o += " rules="       + atr(e.rules);    // Not supported in HTML5
        if (e.caption     ) o += " caption="     + atr(e.caption);  // Not supported in HTML5
        if (e.getAttribute('background') != null) {  // www.xxc.ru
          var url = absolutizeURI(doc.baseURI, e.getAttribute('background'));
          css1 = [["background-image", "url(" + url + ")"]].concat(css1);
        }
        if (e.bgColor     ) css1 = [["background-color", cs.getPropertyValue("background-color")]].concat(css1);
        if (e.getAttribute('width')       != null) css1 = [["width", addpx(e.width)]].concat(css1); // todo: width="100%"
        if (e.getAttribute('height')      != null) css1 = [["height", addpx(e.getAttribute('height'))]].concat(css1); // there is no e.height in WebKit DOM, tc: www.xxx.com
        if (e.getAttribute('cellspacing') != null) css1 = [['border-collapse', 'separate'],
                                                           ["border-spacing", addpx(e.cellSpacing)]].concat(css1);
        if (e.getAttribute('border')      != null) css1 = [["border-style", 'outset'], // mic.org
                                                           ["border-color", '#999'],
                                                           ["border-width", addpx(e.border)]].concat(css1);
        if (e.getAttribute('bordercolor') != null) {  // http://www.stm.info/info/comm-09/co090914.htm
          css1 = [["border-style", 'solid'],
                  ["border-color", e.getAttribute('bordercolor')],
                  ["border-width", addpx(3)]].concat(css1);
        }
      }
      else if (tag == "col" || tag == "colgroup") {
        if (e.ch       ) o += " char="    + atr(e.ch);
        if (e.chOff    ) o += " charoff=" + atr(e.chOff);
        if (e.span != 0) o += " span="    + atr(e.span);
        if (e.bgColor  ) css1 = [["background-color", cs.getPropertyValue("background-color")]].concat(css1);
        if (e.vAlign   ) css1 = [["vertical-align", addpx(e.vAlign)]].concat(css1);
        if (e.getAttribute('width')  != null) css1 = [["width", addpx(e.width)]].concat(css1);
      }
      else if (tag == "tbody" || tag == "tfoot" || tag == "thead" || tag == "tr") {
        if (e.ch          ) o += " char="    + atr(e.ch);
        if (e.chOff       ) o += " charoff=" + atr(e.chOff);
        if (e.bgColor     ) css1 = [["background-color", cs.getPropertyValue("background-color")]].concat(css1);
        if (e.vAlign      ) css1 = [["vertical-align", addpx(e.vAlign)]].concat(css1);
      }
      else if (tag == "td" || tag == "th") {

        for(var table = e; table; table = table.parentNode) {
          if(table.tagName == 'TABLE') {
            if (table.border && table.border!='0') // inset 1px color-from-table-css
              css1 = [["border-top-color",    cs.getPropertyValue("border-top-color")],
                      ["border-left-color",   cs.getPropertyValue("border-left-color")],
                      ["border-right-color",  cs.getPropertyValue("border-right-color")],
                      ["border-bottom-color", cs.getPropertyValue("border-bottom-color")],
                      ["border-style", 'inset'],
                      ["border-width", '1px']].concat(css1);
            if (table.getAttribute('cellpadding') != null)
              css1 = [["padding", addpx(table.cellPadding)]].concat(css1);
            break;
          }
        }

        css1 = [['color', cs.getPropertyValue("color")]].concat(css1); // body=text if BackCompat?
        if (e.abbr        ) o += " abbr="      + atr(e.abbr);
        if (e.axis        ) o += " axis="      + atr(e.axis);
        if (e.ch          ) o += " char="      + atr(e.ch);
        if (e.chOff       ) o += " charoff="   + atr(e.chOff);
        if (e.headers     ) o += " headers="   + atr(e.headers);
        if (e.colSpan != 1) o += " colspan="   + atr(e.colSpan);
        if (e.rowSpan != 1) o += " rowspan="   + atr(e.rowSpan);
        if (e.getAttribute('background') != null) { // www.mic.org
          var url = absolutizeURI(doc.baseURI, e.getAttribute('background'));
          css1 = [["background-image", "url(" + url + ")"]].concat(css1);
        }
        if (e.bgColor     ) css1 = [["background-color", cs.getPropertyValue("background-color")]].concat(css1);
        if (e.noWrap      ) css1 = [["white-space", "nowrap"]].concat(css1);
        if (e.vAlign      ) css1 = [["vertical-align", addpx(e.vAlign)]].concat(css1); // valign=30 (tc:gazpromao.narod.ru)
        if (e.getAttribute('width')  != null) css1 = [["width",  addpx(e.width )]].concat(css1);
        if (e.getAttribute('heigth') != null) css1 = [["height", addpx(e.height)]].concat(css1);
      }
      else if (tag == "p") { // tc:ibm.com, <ul>, <div> cannot be inside <p>
        if(doc.compatMode != 'BackCompat') { // in BackCompat, <p> is so special, better not do the change
          css1 = [["margin-top", cs.getPropertyValue("margin-top")],
                  ["margin-bottom", cs.getPropertyValue("margin-bottom")]].concat(css1);
          o = '<div';
          tag = 'div'
        }
      }
      else if (tag == "xmp") {
        o = "<pre";
        tag = 'pre'
      }
      else {
        // <p>, <span>, <div>, <b>, <pre>, <code>, <h1> and so on
      }

      //if (e.lang != "") o += " lang=" + atr(e.lang);
      //if (e.id       ) o += " id=" + atr(e.id);           // no needed, debug only
      //if (!e.toString().match(/^\[object SVG/) && e.className) o += " class=" + atr(e.className); // no needed, debug only

      if (e.dir  ) css1 = [["direction", e.dir]].concat(css1);
      if (e.align) o += " align=" + atr(e.align);
      if (e.title) o += " title=" + atr(e.title);


      //  todo: filter out ihherited types which the parent has
      if(tag != "head" && tag != "title" && tag != "base" && tag != "meta") { // they have css, but it is senseless
        var cssstr = fixstyle(e, cs, css1);
        if (cssstr)
          o += " style=" + atr(cssstr);
      }

      if(needCloseTag) {
        var ochildren = "";
        for (var i = 0; i < e.childNodes.length; i++) {
          if (e.childNodes[i] != "[object Text]") { // <tag>
            var child = processe(e.childNodes[i]);
            ochildren += child.html;
            eltcss += child.css;
          } else {
            var t = e.childNodes[i].data;
            if (cs.getPropertyValue('white-space').indexOf('pre')!=0) { // not <pre> nor <xmp> nor <textarea>
              t = t.replace(/[\r\n\t ]+/g /* \s+ except &nbsp; */, ' ').replace(/^ | $/, '\n');
            }
            var firstletter;
            if(i==0 && (firstletter = cssfor('first-letter')).length>0) {
              t = t.replace(/^(\s*)(\S)(.*)$/m, function(c, p1, p2, p3) { // replace the 1st non-space letter
                var csfl = doc.defaultView.getComputedStyle(e, 'after')
                return txt(p1) + '<span style=' + atr(fixstyle(null, csfl, firstletter)) + '>' + txt(p2) + '</span>' + txt(p3);
              });
            } else {
              t = txt(t);
            }
            ochildren += t;
          }
        }

        if (tag == "head") // do not emit <head> but emit its children
          o = ochildren;
        else {
          o += '>';
          if (e.parentNode && e.parentNode.toString().match(/^\[object SVG/)) {
            // do not insert <span> inside <svg>...</svg>, tc:http://nickqizhu.github.io/dc.js/
            o += ochildren;
          } else {
            o += mkspan2(doc.defaultView.getComputedStyle(e, 'before'), cssfor('before'));
            o += ochildren;
            o += mkspan2(doc.defaultView.getComputedStyle(e, 'after'), cssfor('after'));
          }
          o += '</' + tag + '>';
        }
      } else {
        o += '/>';
      }

      return {html:o, css:eltcss};
    } // processe

    prompt2("log", "get doctype");
    var doctype = "";
    if(doc.doctype) {
        var dt = doc.doctype;
        if (dt) {
           doctype += "<!DOCTYPE " + dt.nodeName;
           if (dt.publicId) {
             doctype += " PUBLIC " + atr(dt.publicId);
             if (dt.systemId) doctype += " " + atr(dt.systemId);
           } else if (dt.systemId)
             doctype += " SYSTEM " + atr(dt.systemId);
           if (dt.internalSubset)
             doctype += " [" + dt.internalSubset + "]";
           doctype += ">";
        }
    } else {
      doctype = "<!-- no doctype -->\n";
    }


    // keep html5 (new tags, also keyboard icon on google.com), but lift BackCompat to html 4.01
    if (doc.compatMode=='BackCompat' || doctype.indexOf('frameset.dtd')>0) {
      doctype = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'; // still BackCompat
              + '\n<!-- ' + doctype + ' -->'; // old doctype
    }
//    doctype += '\n<!-- compatMode ' + doc.compatMode + ' -->';


    prompt2("log", "get css");
    function ocss(z) {
      if (z == "[object CSSMediaRule]") {
        var o = "";
        for(var k=0; k<z.cssRules.length; k++)
          o += ocss(z.cssRules[k]);
        return o;
      }
      if (z == "[object CSSFontFaceRule]") {
//        prompt2("log", "@font-face {" + z.style.cssText + "}");
//        return "@font-face {" + z.style.cssText + "}\n";
        var srcs = [], o = '@font-face {\n'
        for(var j=0; j<z.style.length; j++) {
          var k = z.style[j], v = z.style.getPropertyValue(k);
          if(k=='src') {
            v = v.replace(/url\(([^)]+)\)(\s*format\([^)]+\)|)/g, function(a, url, rest) {
              var du = prompt2('ventiurl '+ url);
              if(du)
                srcs.push(' url(' + du + ')' + rest);
            });
            o += 'src:';
            for(var i=0; i<srcs.length; i++) {
              if(i>0)o += ',';
              o += srcs[i];
            }
            o += ';\n';
          } else {
            o += k + ':' + v + ';\n';
          }
        }
        return srcs.length==0 /* no url put to venti */ ? '' : o + '}\n';
      }
      if (z == "[object CSSImportRule]" ) {
        var o = "";
        if (z.styleSheet!=null) // inside @media print (tc:www.fbi.gov)
          for(var k=0; k<z.styleSheet.cssRules.length; k++)
            o += ocss(z.styleSheet.cssRules[k]);
        return o;
      }
      return "";
    }

    var head = '';
    var ss = doc.styleSheets;
    for(var i=0; i<ss.length; i++) {
      assert(ss[i].cssRules!=null, "restart with --web-security=no");
      for(var j=0; j<ss[i].cssRules.length; j++)
        head += ocss(ss[i].cssRules[j]) ;
    }


    prompt2("log", "get bg");


    var html = '<div class="html1"';
    html += ' style="width: ' + doc.defaultView.innerWidth + 'px;';
    html += 'text-align: left;'; // do not inherit from outer frame
    html += 'overflow-x: ' + (cshtml.getPropertyValue("overflow-x")=='hidden'?'hidden':'auto') + ';';
    html += 'overflow-y: ' + (cshtml.getPropertyValue("overflow-y")=='hidden'?'hidden':'auto') + ';';

    // choose color for fake 'screen' background, must not be transparent anyway
    var bgcolor = (usedbodycolor ? csbody : cshtml).getPropertyValue("background-color");
    html += ' background-color: ' + bgcolor + ';';

    var bgimage = (usedbodyimage ? csbody : cshtml).getPropertyValue("background-image");
    if (bgimage!='none') {
      bgimage = bgimage.replace(/url\(([^)]+)\)/g, function(m, url) {
        return 'url(' + (prompt2('ventiurl ' + url) || transparent1x1gif) + ')';
      });
      html += ' background-image: '    + bgimage + ';';
      html += ' background-repeat: '   + (usedbodyimage ? csbody : cshtml).getPropertyValue("background-repeat") + ';';
      html += ' background-size: '     + (usedbodyimage ? csbody : cshtml).getPropertyValue("background-size") + ';'; // tc: http://typeclassopedia.bitbucket.org/#slide-4
      html += ' background-position: ' + (usedbodyimage ? csbody : cshtml).getPropertyValue("background-position-x").split(' ')[0] + ' '
                                       + (usedbodyimage ? csbody : cshtml).getPropertyValue("background-position-y").split(' ')[0] + ';';
    }
    // same as position:static, but makes point for further fixeds and absolutes
    // must be before `+=htmlstyle` (tc:maps.google.com, it has `position:absolute`)
    html += 'position: relative;';
    html += htmlstyle;
    // z-index must be after `+=htmlstyle` as it may have "z-index: -1", tc:www.davidrumsey.com, http://archive.is/OMCwI
    html += '; z-index: 0';
    html += '">';

    prompt2("log "+ "get body");
    var ch = processe(doc.documentElement/*<html> <svg>*/);

    html += ch.html;
    html += '</div>';
    return {'url':doc.URL, 'doctype':doctype, 'title':(''+doc.title), 'css':head + ch.css, 'body':html, 'compatmode':doc.compatMode};
  } // processdoc

    return processdoc(document,
      // min-height needed only if there is an absolute/fixed positioned element, todo: check for it
      // document.height is undefined for svg (tc:http://croczilla.com/bits_and_pieces/svg/samples/xbl2/xbl2.xml)
      document.height
        ? 'min-height: ' + addpx(Math.max(document.height, document.defaultView.innerHeight)) + ';'  // td:ya.ru, css-tricks.com, python.org
        : 'height: '     + addpx(document.defaultView.innerHeight) + ';'
      );

}
